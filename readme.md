# Stiles

A lightweight, modern Sass framework that focuses on re-usability over anything else.
Think SMACCS meets bootstrap.
Fully commented and easy to customize.

## URLs

[Docs](https://docs.nerdswithcharisma.com/stiles)
|
[NPM Page](https://www.npmjs.com/package/@nerds-with-charisma/nerds-style-sass)
|
[GitLab Page](https://gitlab.com/nerds-with-charisma/nerds-style-sass)
|
[NWC Website](http://nerdswithcharisma.com/)

## Usage
Install with npm
<br />
`npm i @nerds-with-charisma/nerds-style-sass --save`

Link to the scss files in your main .scss file
<br />
`@import "~@nerds-with-charisma/nerds-style-sass/main.scss";`
<br />

[See the docs](https://nerdswithcharisma.com/stiles/) for how to use the helper classes.

## Compatibility

Right now, just cutting edge browsers. We'll add some features and better testing shortly.

## Release Notes
### 3.1.17
  • Add state dropdown

### 3.1.16
  • Label alignment fix

### 3.1.15
  • Add passwordValidator function

### 3.1.12
  • Adjust label position for input

### 3.1.11
  • Update layout of checkbox/radio to center vertically

### 3.1.10
  • Fix z index issue on tooltip

### 3.1.9
  • Wrap Input in div to help with stacking issue

### 3.1.8
• Fixed minor issue with cover background.
• Updated docs

### 3.1.7

• Added a css file if you aren't using sass

### 3.1.6

• Add key up callback to input

### 3.1.5

• Add event as second returned value in input component

### 3.1.4

• Add mobile header component

### 3.1.3

• Type-o fix in cover component

### 3.1.2

• Add full cover image/video component
• Added justify--content class

### 3.1.1

• Add tooltip component

### 3.1.0

• Add seo widget

### 3.0.5

• Fix css bug wth modal

### 3.0.4

• Export modal for official use

### 3.0.3

• Accidentally published parent folder

### 3.0.2

• Add modal component

### 3.0.1

• Finally fixed everything with the publish, version 3 should be working like a charm. Appologize for any issues.

### 2.5.15-2.6+

• Make the package only the dist directory so it's much smaller (Do not use these, they will not work)

<!--
  Update the version
  `npm version <update_type>`

  Publish the update
  `npm publish`

  Bit
  `bit add .`
  `bit build`

  Test locally -> in dev > stiles-local > package.json change
  "@nerds-with-charisma/nerds-style-sass": "^2.5.3",
  "@nerds-with-charisma/nerds-style-sass": "/Users/briandausman/nerds-with-charisma/gitlab/stiles--nerd-style-sass/nerds-style-sass/dist",

  ## TODOs
  Bit version should ask or auto increment on tag
  Add more components and helpers, but keep slim
    Dropdown menu (maybe)

    Regular header?

  Make an Ecommerce framework (kiwk, kwiky, kwike)
  Badges
-->
