"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var _react = _interopRequireDefault(require("react"));

var _enzyme = _interopRequireWildcard(require("enzyme"));

var _enzymeAdapterReact = _interopRequireDefault(require("enzyme-adapter-react-16"));

var _Radio = _interopRequireDefault(require("./Radio"));

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

_enzyme["default"].configure({
  adapter: new _enzymeAdapterReact["default"]()
}); // get a copy of the dom for our component


var setup = function setup() {
  var props = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var state = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
  return (0, _enzyme.shallow)( /*#__PURE__*/_react["default"].createElement(_Radio["default"], {
    title: props.title,
    id: props.id,
    callback: function callback(v, e) {
      return console.log(v, e);
    },
    value: props.value,
    checked: props.checked,
    theme: props.theme
  }));
};

var testProps = {
  title: 'Check Me',
  id: 'input--someId',
  callback: function callback(v, e) {
    return console.log(v, e);
  },
  value: 'some-value',
  checked: true,
  theme: 'customStyles'
}; // find by test attr

var findByTestAttr = function findByTestAttr(wrapper, val) {
  return wrapper.find("[data-test=\"".concat(val, "\"]"));
}; // make sure the component renders


test('Radio renders without crashing', function () {
  var wrapper = setup(testProps);
  var appComponent = findByTestAttr(wrapper, 'component-radio');
  expect(appComponent.length).toBe(1);
}); // title renders fine

test('Radio title is added', function () {
  var wrapper = setup(testProps);
  var appComponent = findByTestAttr(wrapper, 'component-radio-title');
  expect(appComponent.text()).toBe('Check Me');
}); // id is added to main element

test('Radio Id is added to label', function () {
  var wrapper = setup(testProps);
  var appComponent = wrapper.find('#input--someId');
  expect(appComponent.length).toBe(1);
}); // callback

describe('Radio callback value is returned', function () {
  test('function is called on click', function () {
    var callback = jest.fn(function (v) {
      return v;
    });
    var wrapper = (0, _enzyme.shallow)( /*#__PURE__*/_react["default"].createElement(_Radio["default"], {
      callback: callback,
      title: "test",
      value: "test value"
    }));
    var radio = findByTestAttr(wrapper, 'component-radio-input');
    radio.simulate('click');
    var callbackItem = callback.mock.results[0].value;
    expect(callback).toHaveBeenCalled();
    expect(callbackItem).toBe('test value');
  });
}); // value passed is set

test('Radio Default value is set', function () {
  var wrapper = setup(testProps);
  var appComponent = findByTestAttr(wrapper, 'component-radio-input');
  expect(appComponent.length).toBe(1);
  expect(appComponent.props().value).toBe('some-value');
}); // checked is set

test('Radio Check if passed', function () {
  var wrapper = setup(testProps);
  var appComponent = findByTestAttr(wrapper, 'component-radio-input');
  expect(appComponent.props().defaultChecked).toBe(true);
}); // theme styles are applied

test('Radio Styles are applied', function () {
  var wrapper = setup(testProps);
  var appComponent = findByTestAttr(wrapper, 'component-radio');
  expect(appComponent.find('.customStyles').length).toBe(1);
});