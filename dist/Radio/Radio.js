"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = require("prop-types");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var Radio = function Radio(_ref) {
  var callback = _ref.callback,
      checked = _ref.checked,
      dataAttr = _ref.dataAttr,
      disabled = _ref.disabled,
      id = _ref.id,
      name = _ref.name,
      theme = _ref.theme,
      title = _ref.title,
      value = _ref.value;
  return /*#__PURE__*/_react["default"].createElement("label", {
    "data-test": "component-radio",
    className: "nwc--checkbox ".concat(theme)
  }, /*#__PURE__*/_react["default"].createElement("input", {
    "data-test": "component-radio-input",
    "data-attr": dataAttr,
    defaultChecked: checked,
    disabled: disabled,
    id: id,
    name: name,
    onClick: function onClick(e) {
      return callback(value, e);
    },
    type: "radio",
    value: value
  }), /*#__PURE__*/_react["default"].createElement("strong", {
    "data-test": "component-radio-title",
    className: "nwc--checkbox-title font--14"
  }, title));
};

Radio.defaultProps = {
  checked: false,
  dataAttr: null,
  disabled: false,
  id: '',
  theme: ''
};
Radio.propTypes = {
  callback: _propTypes.PropTypes.func.isRequired,
  checked: _propTypes.PropTypes.bool,
  dataAttr: _propTypes.PropTypes.string,
  disabled: _propTypes.PropTypes.bool,
  id: _propTypes.PropTypes.string,
  theme: _propTypes.PropTypes.string,
  title: _propTypes.PropTypes.string.isRequired,
  value: _propTypes.PropTypes.string.isRequired
};
var _default = Radio;
/**
 * Radio input with callbacks
 *
 *
 * @property {func} callback - A function that will run after the input  has changed, it will return the value of the input for you to use in the parent component.
 * @property {bool} checked - Should the radio show as checked or not.
 * @property {string} dataAttr - A custom data attribute value you can pass to hook into the input if needed.
 * @property {bool} disabled - Should the input be disabled or not. Use with a state var to determine when the input can be edited.
 * @property {string} id - An id to be added to the input itself, not the wrapping label.
 * @property {string} theme - Classes to be added to the wrapping label.
 * @property {string} title - The label text title of the input.
 * @property {string} value - The value of the radio, will be accessible as part of "e" in the callback
 */

exports["default"] = _default;