"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = require("prop-types");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var Fab = function Fab(_ref) {
  var backgroundColor = _ref.backgroundColor,
      children = _ref.children,
      clickCb = _ref.clickCb,
      enterCb = _ref.enterCb,
      fabOpen = _ref.fabOpen,
      leaveCb = _ref.leaveCb,
      left = _ref.left,
      mini = _ref.mini,
      textColor = _ref.textColor,
      theme = _ref.theme,
      title = _ref.title;
  var dockedStyle = null; // determine where it should be docked at the bottom, if at all
  // if docked, which side

  if (left === true) {
    dockedStyle = {
      left: 25
    };
  } else {
    dockedStyle = {
      right: 25
    };
  } // if should be mini or regular size


  var miniStyle = null;
  if (mini === true) miniStyle = {
    height: 40,
    lineHeight: '40px',
    padding: '0 10px',
    minWidth: 40
  }; // if left, we need to adjust the styles

  var leftStyle = null;
  if (left === true) leftStyle = {
    textAlign: 'left',
    left: '10px',
    right: 'inherit'
  };
  return /*#__PURE__*/_react["default"].createElement("span", {
    "data-test": "component-fab",
    onMouseEnter: function onMouseEnter() {
      return enterCb();
    },
    onMouseLeave: function onMouseLeave() {
      return leaveCb();
    },
    onClick: function onClick() {
      return clickCb();
    },
    className: "nwc--fab-wrapper nwc--docked ".concat(theme),
    style: _objectSpread({}, dockedStyle)
  }, /*#__PURE__*/_react["default"].createElement("span", {
    "data-test": "component-fab-open",
    className: fabOpen === true ? 'nwc--fab-mask' : 'nwc--fab-mask opacity--0'
  }), /*#__PURE__*/_react["default"].createElement("div", {
    "data-test": "component-fab-wrapper",
    className: fabOpen === true ? 'nwc--fab-menu' : 'nwc--fab-menu opacity--0',
    style: _objectSpread({}, leftStyle)
  }, /*#__PURE__*/_react["default"].createElement("span", {
    className: left === true ? 'left' : 'right'
  }, children)), /*#__PURE__*/_react["default"].createElement("button", {
    "data-test": "component-fab-button",
    type: "button",
    className: "nwc--fab padding--none border--none",
    style: _objectSpread({}, miniStyle, {
      background: backgroundColor,
      color: textColor
    })
  }, /*#__PURE__*/_react["default"].createElement("div", null, title)));
};

Fab.defaultProps = {
  backgroundColor: '',
  clickCb: function clickCb() {
    return null;
  },
  enterCb: function enterCb() {
    return null;
  },
  leaveCb: function leaveCb() {
    return null;
  },
  left: false,
  mini: false,
  textColor: '',
  theme: '',
  title: '+'
};
Fab.propTypes = {
  backgroundColor: _propTypes.PropTypes.string,
  children: _propTypes.PropTypes.object.isRequired,
  clickCb: _propTypes.PropTypes.func,
  enterCb: _propTypes.PropTypes.func,
  fabOpen: _propTypes.PropTypes.bool.isRequired,
  leaveCb: _propTypes.PropTypes.func,
  left: _propTypes.PropTypes.bool,
  mini: _propTypes.PropTypes.bool,
  textColor: _propTypes.PropTypes.string,
  theme: _propTypes.PropTypes.string,
  title: _propTypes.PropTypes.string
};
var _default = Fab;
/**
 * Floating Action button that takes several arguements to customize it's appearance
 *
 *
 * @property {string} backgroundColor - The hex background color of the button.
 * @property {object} children - A valid JSX set of elements.
 * @property {func} clickCb - The callback when the button is clicked.
 * @property {func} enterCb - The callback function when the mouse enters the button.
 * @property {bool} fabOpen - Toggle to show the content inside the button.
 * @property {func} leaveCb - The callback function when the mouse leaves the button.
 * @property {bool} left - Dock it on the left, otherwise will be docked on the right.
 * @property {bool} mini - Make the fab smaller than normal.
 * @property {string} textColor - Thecolor of the text of the button.
 * @property {string} theme - The classes applied to the button itself.
 * @property {string} title - The text shown next to the main button.
 */

exports["default"] = _default;