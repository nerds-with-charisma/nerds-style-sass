"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var _react = _interopRequireDefault(require("react"));

var _enzyme = _interopRequireWildcard(require("enzyme"));

var _enzymeAdapterReact = _interopRequireDefault(require("enzyme-adapter-react-16"));

var _FabItem = _interopRequireDefault(require("./FabItem"));

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

_enzyme["default"].configure({
  adapter: new _enzymeAdapterReact["default"]()
});

var setup = function setup() {
  var props = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var state = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
  return (0, _enzyme.shallow)( /*#__PURE__*/_react["default"].createElement(_FabItem["default"], {
    backgroundColor: props.backgroundColor,
    caption: props.caption,
    isImage: props.isImage,
    textColor: props.textColor
  }, props.children));
};

var testProps = {
  backgroundColor: '#fff',
  caption: 'test caption',
  children: /*#__PURE__*/_react["default"].createElement("h1", null, 'Test'),
  isImage: true,
  textColor: '#000'
};

var findByTestAttr = function findByTestAttr(wrapper, val) {
  return wrapper.find("[data-test=\"".concat(val, "\"]"));
}; // Tests


test('FabItem renders without crashing', function () {
  var wrapper = setup(testProps);
  var appComponent = findByTestAttr(wrapper, 'component-fabItem');
  expect(appComponent.length).toBe(1);
}); // backgroundColor & text color

test('FabItem color styles gets set', function () {
  var wrapper = setup(testProps);
  var appComponent = findByTestAttr(wrapper, 'component-fabItem');
  expect(JSON.stringify(appComponent.prop('style'))).toBe(JSON.stringify({
    background: '#fff',
    color: '#000'
  }));
}); // caption

test('FabItem caption gets rendered', function () {
  var wrapper = setup(testProps);
  var appComponent = findByTestAttr(wrapper, 'component-fabItem-caption');
  expect(appComponent.length).toBe(1);
  expect(appComponent.text()).toBe('test caption');
}); // children

test('FabItem children render properly', function () {
  var wrapper = setup(testProps);
  var appComponent = findByTestAttr(wrapper, 'component-fabItem');
  expect(appComponent.find('h1').text()).toBe('Test');
}); // isImage,

test('FabItem image renders properly', function () {
  var wrapper = setup(testProps);
  var appComponent = findByTestAttr(wrapper, 'component-fabItem');
  expect(appComponent.find('.nwc--fab-image').length).toBe(1);
});