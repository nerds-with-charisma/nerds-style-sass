"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var _react = _interopRequireDefault(require("react"));

var _enzyme = _interopRequireWildcard(require("enzyme"));

var _enzymeAdapterReact = _interopRequireDefault(require("enzyme-adapter-react-16"));

var _Fab = _interopRequireDefault(require("./Fab"));

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

_enzyme["default"].configure({
  adapter: new _enzymeAdapterReact["default"]()
});

var setup = function setup() {
  var props = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var state = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
  return (0, _enzyme.shallow)( /*#__PURE__*/_react["default"].createElement(_Fab["default"], _defineProperty({
    backgroundColor: props.backgroundColor,
    clickCb: props.clickCb,
    enterCb: props.enterCb,
    fabOpen: props.fabOpen,
    leaveCb: props.leaveCb,
    left: props.left,
    mini: props.mini,
    textColor: props.textColor,
    theme: props.theme,
    title: props.title
  }, "theme", props.theme), props.children));
};

var testProps = {
  backgroundColor: '#fff',
  children: /*#__PURE__*/_react["default"].createElement("h1", null, 'Test'),
  clickCb: function clickCb() {
    return alert('click');
  },
  enterCb: function enterCb() {
    return alert('enter');
  },
  fabOpen: true,
  leaveCb: function leaveCb() {
    return alert('leave');
  },
  left: true,
  mini: false,
  textColor: '#000',
  theme: 'test-class',
  title: 'Test button'
}; // find by test attr

var findByTestAttr = function findByTestAttr(wrapper, val) {
  return wrapper.find("[data-test=\"".concat(val, "\"]"));
}; // make sure it renders aight


test('Fab renders without crashing', function () {
  var wrapper = setup(testProps);
  var appComponent = findByTestAttr(wrapper, 'component-fab');
  expect(appComponent.length).toBe(1);
}); // backgroundColor & text color

test('Fab styles get applied', function () {
  var wrapper = setup(testProps);
  var appComponent = findByTestAttr(wrapper, 'component-fab').find('.nwc--fab');
  expect(JSON.stringify(appComponent.prop('style'))).toBe(JSON.stringify({
    background: '#fff',
    color: '#000'
  }));
}); // fabOpen

test('Fab opens properly', function () {
  var wrapper = setup(testProps);
  var appComponent = findByTestAttr(wrapper, 'component-fab-open');
  expect(appComponent.prop('className')).toBe('nwc--fab-mask');
}); // left

test('Fab docks on the left side', function () {
  var wrapper = setup(testProps);
  var appComponent = findByTestAttr(wrapper, 'component-fab-wrapper');
  expect(JSON.stringify(appComponent.prop('style'))).toBe(JSON.stringify({
    textAlign: 'left',
    left: '10px',
    right: 'inherit'
  }));
}); // mini

test('Fab mini styles render properly', function () {
  var wrapper = setup({
    children: /*#__PURE__*/_react["default"].createElement("h1", null, 'Test'),
    fabOpen: true,
    mini: true
  });
  var appComponent = findByTestAttr(wrapper, 'component-fab-button');
  expect(JSON.stringify(appComponent.prop('style'))).toBe(JSON.stringify({
    height: 40,
    lineHeight: '40px',
    padding: '0 10px',
    minWidth: 40,
    background: '',
    color: ''
  }));
}); // theme

test('Fab theme styles render properly', function () {
  var wrapper = setup(testProps);
  var appComponent = wrapper.find('.nwc--fab-wrapper.nwc--docked.test-class');
  expect(appComponent.length).toBe(1);
}); // title

test('Fab title renders', function () {
  var wrapper = setup(testProps);
  var appComponent = findByTestAttr(wrapper, 'component-fab-button').find('div');
  expect(appComponent.text()).toBe('Test button');
}); // children

test('Fab renders its children properly', function () {
  var wrapper = setup(testProps);
  var appComponent = findByTestAttr(wrapper, 'component-fab-wrapper');
  var childrenWrapper = appComponent.find('span');
  expect(childrenWrapper.find('h1').text()).toBe('Test');
});