"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = require("prop-types");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var FabItem = function FabItem(_ref) {
  var backgroundColor = _ref.backgroundColor,
      caption = _ref.caption,
      children = _ref.children,
      isImage = _ref.isImage,
      textColor = _ref.textColor;
  return /*#__PURE__*/_react["default"].createElement("span", {
    "data-test": "component-fabItem",
    className: isImage !== true ? 'nwc--fab-item' : 'nwc--fab-image',
    style: {
      background: backgroundColor,
      color: textColor
    }
  }, children, caption !== null && /*#__PURE__*/_react["default"].createElement("span", {
    "data-test": "component-fabItem-caption"
  }, caption), isImage === true && /*#__PURE__*/_react["default"].createElement("br", null));
};

FabItem.defaultProps = {
  caption: null,
  isImage: false
};
FabItem.propTypes = {
  backgroundColor: _propTypes.PropTypes.string,
  caption: _propTypes.PropTypes.string,
  children: _propTypes.PropTypes.object.isRequired,
  isImage: _propTypes.PropTypes.bool,
  textColor: _propTypes.PropTypes.string
};
var _default = FabItem;
/**
 * An item inside the fab, highly customizable
 *
 *
 * @property {string} backgroundColor - The hex color of the item's background.
 * @property {string} caption  - The caption title to be shown for the item.
 * @property {object} children - Valid JSX elmement.
 * @property {bool} isImage - If it's just an image, no text.
 * @property {string} textColor - The color of the text of the item.
 */

exports["default"] = _default;