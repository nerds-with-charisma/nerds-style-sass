"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var _react = _interopRequireDefault(require("react"));

var _enzyme = _interopRequireWildcard(require("enzyme"));

var _enzymeAdapterReact = _interopRequireDefault(require("enzyme-adapter-react-16"));

var _Tooltip = _interopRequireDefault(require("./Tooltip"));

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

_enzyme["default"].configure({
  adapter: new _enzymeAdapterReact["default"]()
});

var setup = function setup() {
  var props = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var state = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
  return (0, _enzyme.shallow)( /*#__PURE__*/_react["default"].createElement(_Tooltip["default"], {
    callback: props.callback,
    content: props.content,
    id: props.id,
    position: props.position,
    theme: props.theme,
    themeTooltip: props.themeTooltip,
    title: props.title,
    width: props.width
  }));
};

var testProps = {
  callback: function callback() {
    return console.log('callback');
  },
  content: /*#__PURE__*/_react["default"].createElement("strong", null, "This is all stuff and it's super duper cool"),
  id: 'myId',
  position: 'bottom',
  theme: 'test-theme',
  themeTooltip: 'tooltip-classes',
  title: 'Hello',
  width: 800
}; // find by test attr

var findByTestAttr = function findByTestAttr(wrapper, val) {
  return wrapper.find("[data-test=\"".concat(val, "\"]"));
};

var wrapper = setup(testProps);
var mainComponent = findByTestAttr(wrapper, 'component-tooltip'); // Test

test('Tooltip renders without crashing', function () {
  expect(mainComponent.length).toBe(1);
}); // id: 'myId',

test('Tooltip id gets applied', function () {
  expect(mainComponent.find('#myId').length).toBe(1);
}); // theme

test('Tooltip theme gets applied', function () {
  expect(mainComponent.find('button').prop('className')).toBe('test-theme');
}); // title

test('Tooltip title renders', function () {
  expect(mainComponent.find('button').text()).toBe('Hello');
});