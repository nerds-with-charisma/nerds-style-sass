"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var _react = _interopRequireDefault(require("react"));

var _enzyme = _interopRequireWildcard(require("enzyme"));

var _enzymeAdapterReact = _interopRequireDefault(require("enzyme-adapter-react-16"));

var _CoverBackground = _interopRequireDefault(require("./CoverBackground"));

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

_enzyme["default"].configure({
  adapter: new _enzymeAdapterReact["default"]()
});

var setup = function setup() {
  var props = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var state = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
  return (0, _enzyme.shallow)( /*#__PURE__*/_react["default"].createElement(_CoverBackground["default"], {
    backgroundColor: props.backgroundColor,
    minHeight: props.minHeight,
    mp4: props.mp4,
    src: props.src,
    theme: props.theme,
    webm: props.webm
  }, props.children));
};

var testProps = {
  children: /*#__PURE__*/_react["default"].createElement("div", null, /*#__PURE__*/_react["default"].createElement("h1", null, "Hi")),
  src: 'https://nerdswithcharisma.com/static/6d2103ee47e61e92d68e165eb9de330a/97132/bg--hero.webp',
  backgroundColor: '#ff0000',
  minHeight: 500,
  mp4: 'somevideo.mp4',
  theme: 'test-class',
  webm: 'somevideo.webm'
}; // find by test attr

var findByTestAttr = function findByTestAttr(wrapper, val) {
  return wrapper.find("[data-test=\"".concat(val, "\"]"));
};

var wrapper = setup(testProps);
var mainComponent = findByTestAttr(wrapper, 'component-cover-background');
var webmComponent = findByTestAttr(wrapper, 'component-cover-background-webm');
var mp4Component = findByTestAttr(wrapper, 'component-cover-background-mp4'); // Test

test('CoverBackground renders without crashing', function () {
  expect(mainComponent.length).toBe(1);
}); // children

test('CoverBackground children render', function () {
  expect(mainComponent.find('h1').length).toBe(1);
}); // src=

test('CoverBackground image background is applied', function () {
  expect(mainComponent.prop('style').backgroundImage).toBe('url(https://nerdswithcharisma.com/static/6d2103ee47e61e92d68e165eb9de330a/97132/bg--hero.webp)');
}); // minHeight

test('CoverBackground min height gets set', function () {
  expect(mainComponent.prop('style').minHeight).toBe(500);
}); // mp4

test('CoverBackground mp4 is set if supplied', function () {
  expect(mp4Component.length).toBe(1);
  expect(mp4Component.prop('src')).toBe('somevideo.mp4');
}); // backgroundColor

test('CoverBackground background gets set', function () {
  expect(mainComponent.prop('style').backgroundColor).toBe('#ff0000');
}); // theme

test('CoverBackground theme classes are applied', function () {
  expect(mainComponent.find('.test-class').length).toBe(1);
}); // webm={props.webm}

test('CoverBackground webm is set if supplied', function () {
  expect(webmComponent.length).toBe(1);
  expect(webmComponent.prop('src')).toBe('somevideo.webm');
});