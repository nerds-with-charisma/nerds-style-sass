"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var _react = _interopRequireDefault(require("react"));

var _enzyme = _interopRequireWildcard(require("enzyme"));

var _enzymeAdapterReact = _interopRequireDefault(require("enzyme-adapter-react-16"));

var _Seo = _interopRequireDefault(require("./Seo"));

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

_enzyme["default"].configure({
  adapter: new _enzymeAdapterReact["default"]()
});

var setup = function setup() {
  var props = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var state = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
  return (0, _enzyme.shallow)( /*#__PURE__*/_react["default"].createElement(_Seo["default"], {
    lang: props.lang,
    meta: props.meta,
    openGraph: props.openGraph,
    title: props.title
  }, /*#__PURE__*/_react["default"].createElement("h1", null, 'Test Child')));
};

var testProps = {
  title: 'Test title',
  meta: [{
    name: 'description',
    content: 'My description'
  }],
  openGraph: {
    url: 'https://nerdswithcharisma.com'
  },
  lang: 'FE',
  schema: [{
    '@type': 'Person',
    colleague: [],
    image: 'https://nerdswithcharisma.com/images/portfolio/brian.jpg',
    jobTitle: 'Developer',
    alumniOf: 'Northern Illinois University',
    gender: 'male',
    '@context': 'http://schema.org',
    email: 'briandausman@gmail.com',
    name: 'Brian Dausman',
    url: 'https://nerdswithcharisma.com'
  }]
}; // find by test attr

var findByTestAttr = function findByTestAttr(wrapper, val) {
  return wrapper.find("[data-test=\"".concat(val, "\"]"));
};

var wrapper = setup(testProps);
var mainComponent = findByTestAttr(wrapper, 'component-seo'); // Test

test('Seo widget renders without crashing', function () {
  expect(mainComponent.length).toBe(1);
}); // children

test('Seo child renders if supplied', function () {
  expect(mainComponent.find('h1').text()).toBe('Test Child');
}); // lang

test('Seo language is set', function () {
  expect(mainComponent.prop('lang')).toBe('FE');
}); // meta

test('Seo meta gets populated', function () {
  expect(mainComponent.find('meta').first().prop('content')).toBe('My description');
}); // openGraph

test('Seo OG gets populated', function () {
  expect(mainComponent.find('meta[property="og:url"]').prop('content')).toBe('https://nerdswithcharisma.com');
}); // title

test('Seo title renders', function () {
  expect(mainComponent.find('title').text()).toBe('Test title');
});