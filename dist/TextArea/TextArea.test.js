"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var _react = _interopRequireDefault(require("react"));

var _enzyme = _interopRequireWildcard(require("enzyme"));

var _enzymeAdapterReact = _interopRequireDefault(require("enzyme-adapter-react-16"));

var _TextArea = _interopRequireDefault(require("./TextArea"));

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

_enzyme["default"].configure({
  adapter: new _enzymeAdapterReact["default"]()
});

var setup = function setup() {
  var props = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var state = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
  return (0, _enzyme.shallow)( /*#__PURE__*/_react["default"].createElement(_TextArea["default"], {
    autofill: props.autofill,
    blurCallback: props.blurCallback,
    callback: props.callback,
    error: props.error,
    id: props.id,
    maxLength: props.maxLength,
    minLength: props.minLength,
    optional: props.optional,
    placeHolder: props.placeHolder,
    populatedValue: props.populatedValue,
    required: props.required,
    rows: props.rows,
    theme: props.theme,
    title: props.title
  }));
};

var testProps = {
  autofill: 'nope',
  blurCallback: function blurCallback(v) {
    return console.log(v);
  },
  callback: function callback(v) {
    return console.log(v);
  },
  error: 'Error Test',
  id: 'input--test',
  maxLength: 40,
  minLength: 40,
  optional: true,
  placeHolder: 'Placeholder Test',
  populatedValue: 'Test Value',
  required: true,
  rows: '10',
  theme: 'test',
  title: 'Test'
}; // find by test attr

var findByTestAttr = function findByTestAttr(wrapper, val) {
  return wrapper.find("[data-test=\"".concat(val, "\"]"));
}; // Test


test('TextArea renders without crashing', function () {
  var wrapper = setup(testProps);
  var appComponent = findByTestAttr(wrapper, 'component-textArea');
  expect(appComponent.length).toBe(1);
}); // autofill

test('Input autofill is working', function () {
  var wrapper = setup(testProps);
  var appComponent = findByTestAttr(wrapper, 'component-textArea-input');
  expect(appComponent.prop('autofill')).toBe('nope');
}); // blurCallback

describe('TextArea blur callback value is returned', function () {
  test('function is called on  blur', function () {
    var callback = jest.fn(function (v) {
      return v;
    });
    var wrapper = (0, _enzyme.shallow)( /*#__PURE__*/_react["default"].createElement(_TextArea["default"], {
      id: "test",
      blurCallback: callback,
      callback: function callback() {
        return true;
      },
      rows: "1",
      title: "test"
    }));
    var textArea = findByTestAttr(wrapper, 'component-textArea-input');
    var mockEvent = {
      target: {
        value: 'blur value'
      }
    };
    textArea.simulate('blur', mockEvent);
    var callbackItem = callback.mock.results[0].value;
    expect(callback).toHaveBeenCalled();
    expect(callbackItem).toBe('blur value');
  });
}); // callback

describe('TextArea callback value is returned', function () {
  test('function is called on click', function () {
    var callback = jest.fn(function (v) {
      return v;
    });
    var wrapper = (0, _enzyme.shallow)( /*#__PURE__*/_react["default"].createElement(_TextArea["default"], {
      id: "test",
      callback: callback,
      rows: "1",
      title: "test"
    }));
    var textArea = findByTestAttr(wrapper, 'component-textArea-input');
    var mockEvent = {
      target: {
        value: 'test value'
      }
    };
    textArea.simulate('change', mockEvent);
    var callbackItem = callback.mock.results[0].value;
    expect(callback).toHaveBeenCalled();
    expect(callbackItem).toBe('test value');
  });
}); // error

test('TextArea error works', function () {
  var wrapper = setup(testProps);
  var appComponent = findByTestAttr(wrapper, 'component-textArea-error');
  expect(appComponent.text()).toBe('Error Test');
}); // id

test('TextArea id is set', function () {
  var wrapper = setup(testProps);
  var appComponent = findByTestAttr(wrapper, 'component-textArea-input');
  expect(appComponent.prop('id')).toBe('input--test');
}); // maxLength

test('TextArea max length is set', function () {
  var wrapper = setup(testProps);
  var appComponent = findByTestAttr(wrapper, 'component-textArea-input');
  expect(appComponent.prop('maxLength')).toBe(40);
}); // minLength

test('TextArea min length is set', function () {
  var wrapper = setup(testProps);
  var appComponent = findByTestAttr(wrapper, 'component-textArea-input');
  expect(appComponent.prop('minLength')).toBe(40);
}); // optional

test('TextArea optional text shows', function () {
  var wrapper = setup(testProps);
  var appComponent = findByTestAttr(wrapper, 'component-textArea-optional');
  expect(appComponent.length).toBe(1);
}); // placeHolder

test('TextArea placeholder gets populated', function () {
  var wrapper = setup(testProps);
  var appComponent = findByTestAttr(wrapper, 'component-textArea-input');
  expect(appComponent.prop('placeholder')).toBe('Placeholder Test');
}); // populatedValue

test('TextArea initial popluated value works', function () {
  var wrapper = setup(testProps);
  var appComponent = findByTestAttr(wrapper, 'component-textArea-input');
  var defaultValue = JSON.stringify(appComponent.debug());
  expect(defaultValue.includes('Test')).toBe(true);
}); // required

test('TextArea required text shows', function () {
  var wrapper = setup(testProps);
  var appComponent = findByTestAttr(wrapper, 'component-textArea-required');
  expect(appComponent.length).toBe(1);
}); // theme

test('TextArea theme styles render', function () {
  var wrapper = setup(testProps);
  var appComponent = findByTestAttr(wrapper, 'component-textArea');
  expect(appComponent.find('.nwc--input.test').length).toBe(1);
}); // title

test('TextArea title renders', function () {
  var wrapper = setup(testProps);
  var appComponent = findByTestAttr(wrapper, 'component-textArea-title');
  expect(appComponent.text()).toBe('Test');
});