"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = require("prop-types");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var TextArea = function TextArea(_ref) {
  var autofill = _ref.autofill,
      blurCallback = _ref.blurCallback,
      callback = _ref.callback,
      error = _ref.error,
      id = _ref.id,
      maxLength = _ref.maxLength,
      minLength = _ref.minLength,
      optional = _ref.optional,
      placeHolder = _ref.placeHolder,
      populatedValue = _ref.populatedValue,
      required = _ref.required,
      rows = _ref.rows,
      theme = _ref.theme,
      title = _ref.title,
      type = _ref.type;
  return /*#__PURE__*/_react["default"].createElement("label", {
    "data-test": "component-textArea",
    className: "nwc--input ".concat(theme),
    htmlFor: id
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "nwc--label-wrap"
  }, /*#__PURE__*/_react["default"].createElement("strong", {
    className: "font--14"
  }, /*#__PURE__*/_react["default"].createElement("span", {
    "data-test": "component-textArea-title"
  }, title), required === true && /*#__PURE__*/_react["default"].createElement("sup", {
    "data-test": "component-textArea-required",
    className: "font--error font--10"
  }, ' *'), optional === true && /*#__PURE__*/_react["default"].createElement("sup", {
    "data-test": "component-textArea-optional",
    className: "font--grey font--10"
  }, ' (optional)'))), /*#__PURE__*/_react["default"].createElement("textarea", {
    "data-test": "component-textArea-input",
    autofill: autofill,
    className: "padding--md font--16 border--grey",
    defaultValue: populatedValue,
    id: id,
    maxLength: maxLength,
    minLength: minLength,
    name: id,
    onChange: function onChange(e) {
      return callback(e.target.value);
    },
    onBlur: function onBlur(e) {
      return blurCallback(e.target.value);
    },
    placeholder: placeHolder,
    rows: rows,
    type: type
  }), error !== null && /*#__PURE__*/_react["default"].createElement("aside", {
    "data-test": "component-textArea-error",
    className: "nwc--validation nwc--validation--error font--12"
  }, error));
};

TextArea.defaultProps = {
  autofill: 'yes',
  blurCallback: function blurCallback() {
    return true;
  },
  theme: '',
  error: null,
  maxLength: 5000,
  minLength: 1,
  optional: false,
  placeholder: null,
  populatedValue: null,
  required: false,
  rows: 1,
  type: 'text'
};
TextArea.propTypes = {
  autofill: _propTypes.PropTypes.string,
  blurCallback: _propTypes.PropTypes.func,
  callback: _propTypes.PropTypes.func.isRequired,
  error: _propTypes.PropTypes.string,
  id: _propTypes.PropTypes.string.isRequired,
  maxLength: _propTypes.PropTypes.number,
  minLength: _propTypes.PropTypes.number,
  optional: _propTypes.PropTypes.bool,
  placeHolder: _propTypes.PropTypes.string,
  populatedValue: _propTypes.PropTypes.string,
  required: _propTypes.PropTypes.bool,
  rows: _propTypes.PropTypes.string,
  theme: _propTypes.PropTypes.string,
  title: _propTypes.PropTypes.string.isRequired,
  type: _propTypes.PropTypes.string
};
var _default = TextArea;
/**
 * Super customizable inputs that you can style to ur hearts desire.
 * Callbacks for blur and change let you control logic in your parent component easily.
 *
 * @property {string} autofill - If the input should allow autofilling from the browser. This is not always handled properly in Chrome, so we suggest to use a string like 'nope'.
 * @property {func} blurCallback - The function to run when the input loses focus/blurred.
 * @property {func} callback - A function that will run after the input  has changed, it will return the value of the input for you to use in the parent component.
 * @property {bool} disabled - Should the input be disabled or not. Use with a state var to determine when the input can be edited.
 * @property {string} error - An error message to be shown docked at the bottom of the input. If null, nothing will show.
 * @property {string} id - An id to be added to the input itself, not the wrapping label.
 * @property {number} maxLength - The allowed max length of characters into the input.
 * @property {number} minLength - The allowed min lenght of characters into the input.
 * @property {bool} optional - If true, will show an (optional) tag next to the title. If false or not supplied, nothing will show.
 * @property {string} placeHolder - Placeholder value to show on the input before any value is entered.
 * @property {string} populatedValue - The default value to be populated into the input on load.
 * @property {bool} required - If true, will show a required *'s. If false or not supplied, nothing will show.
 * @property {string} rows - How many rows should show for the text area.
 * @property {string} theme - Classes to be added to the wrapping label.
 * @property {string} title - The label text title of the input.
 * @property {string} type - The type attribute to determine what sort of input is used (text, email, etc).
 */

exports["default"] = _default;