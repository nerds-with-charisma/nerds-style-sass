"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var _react = _interopRequireDefault(require("react"));

var _enzyme = _interopRequireWildcard(require("enzyme"));

var _enzymeAdapterReact = _interopRequireDefault(require("enzyme-adapter-react-16"));

var _Modal = _interopRequireDefault(require("./Modal"));

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

_enzyme["default"].configure({
  adapter: new _enzymeAdapterReact["default"]()
});

var setup = function setup() {
  var props = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var state = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
  return (0, _enzyme.shallow)( /*#__PURE__*/_react["default"].createElement(_Modal["default"], {
    closeCb: function closeCb() {
      return props.closeCb;
    },
    maskColor: props.maskColor,
    show: props.show,
    theme: props.theme,
    themeBody: props.themeBody,
    themeClose: props.themeClose,
    title: props.title,
    width: props.width
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "font--14"
  }, props.children)));
};

var testProps = {
  children: /*#__PURE__*/_react["default"].createElement("h1", null, 'The plans you refer to will soon be back in our hands.'),
  maskColor: 'rgba(50,20,100,0.7)',
  show: true,
  theme: 'test-theme',
  themeBody: 'test-theme-body',
  themeClose: 'test-theme-close',
  title: 'Imma Modal',
  width: 50
};
var defaultProps = {
  children: /*#__PURE__*/_react["default"].createElement("h1", null, 'The plans you refer to will soon be back in our hands.'),
  show: false,
  theme: 'test-theme',
  themeBody: 'test-theme-body',
  themeClose: 'test-theme-close',
  title: 'Imma Modal',
  width: 50
}; // find by test attr

var findByTestAttr = function findByTestAttr(wrapper, val) {
  return wrapper.find("[data-test=\"".concat(val, "\"]"));
};

var wrapper = setup(testProps);
var appComponent = findByTestAttr(wrapper, 'component-modal');
var defaultWrapper = setup(defaultProps);
var defaultComponent = findByTestAttr(defaultWrapper, 'component-modal'); // Test

test('Modal renders without crashing', function () {
  expect(appComponent.length).toBe(1);
}); // children

test('Modal children render properly', function () {
  expect(appComponent.find('h1').length).toBe(1);
}); // maskColor

test('Modal mask color applied, and default kicks in if not', function () {
  expect(appComponent.find('#nwc--modal-mask').prop('style').backgroundColor).toBe('rgba(50,20,100,0.7)'); // change show to true to test
  // expect(
  //   defaultComponent.find('#nwc--modal-mask').prop('style').backgroundColor,
  // ).toBe('rgba(0,0,0,0.7)');
}); // show

test("Modal shows when it should and does not when it shouldn't", function () {
  expect(appComponent.length).toBe(1);
  expect(defaultComponent.length).toBe(0);
}); // theme

test('Modal theme is applied', function () {
  expect(appComponent.find('.test-theme').length).toBe(1);
}); // themeBody

test('Modal body theme is applied', function () {
  expect(appComponent.find('.test-theme-body').length).toBe(1);
}); // themeClose

test('Modal close theme is applied', function () {
  expect(appComponent.find('.test-theme-close').length).toBe(1);
}); // title

test('Modal title populates', function () {
  var title = findByTestAttr(wrapper, 'component-modal-title');
  expect(title.length).toBe(1);
  expect(title.text()).toBe('Imma Modal');
});