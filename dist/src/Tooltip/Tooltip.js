"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = require("prop-types");

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(n); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var Tooltip = function Tooltip(_ref) {
  var _tooltipStyle;

  var callback = _ref.callback,
      content = _ref.content,
      id = _ref.id,
      position = _ref.position,
      theme = _ref.theme,
      themeTooltip = _ref.themeTooltip,
      title = _ref.title,
      width = _ref.width;

  var _useState = (0, _react.useState)(false),
      _useState2 = _slicedToArray(_useState, 2),
      show = _useState2[0],
      showSetter = _useState2[1];

  var _useState3 = (0, _react.useState)(0),
      _useState4 = _slicedToArray(_useState3, 2),
      elementWidth = _useState4[0],
      elementWidthSetter = _useState4[1];

  var buttonStyle = {
    display: 'inline-block',
    position: 'relative',
    textAlign: 'center'
  };
  var tooltipStyle = (_tooltipStyle = {
    position: 'absolute',
    top: position === 'top' ? null : null,
    bottom: position === 'top' ? '100%' : null,
    left: position === 'left' ? "-".concat(width, "px") : "".concat(elementWidth / 2 - width / 2, "px")
  }, _defineProperty(_tooltipStyle, "left", position === 'right' ? '100%' : "".concat(elementWidth / 2 - width / 2, "px")), _defineProperty(_tooltipStyle, "marginTop", position === 'left' || position === 'right' ? '-60%' : 0), _tooltipStyle);
  return /*#__PURE__*/_react["default"].createElement("span", {
    "data-test": "component-tooltip",
    id: id,
    className: "nwc--tooltip",
    style: buttonStyle
  }, /*#__PURE__*/_react["default"].createElement("button", {
    type: "button",
    style: {
      cursor: 'pointer'
    },
    className: theme,
    onClick: function onClick() {
      return callback();
    },
    onMouseEnter: function onMouseEnter(e) {
      elementWidthSetter(e.currentTarget.offsetWidth);
      showSetter(true);
    },
    onMouseLeave: function onMouseLeave() {
      return showSetter(false);
    }
  }, title), show === true && /*#__PURE__*/_react["default"].createElement("div", {
    className: "".concat(themeTooltip, " nwc--tooltip-").concat(position),
    style: _objectSpread({
      width: width
    }, tooltipStyle)
  }, content));
};

Tooltip.defaultProps = {
  callback: null,
  content: 'Hello!',
  id: null,
  theme: null,
  themeTooltip: 'bg--light shadow--md padding--sm text--center font--14',
  title: 'Hover Here',
  width: 300
};
Tooltip.propTypes = {
  callback: _propTypes.PropTypes.func,
  content: _propTypes.PropTypes.object,
  id: _propTypes.PropTypes.string,
  theme: _propTypes.PropTypes.string,
  themeTooltip: _propTypes.PropTypes.string,
  title: _propTypes.PropTypes.string,
  width: _propTypes.PropTypes.number
};
var _default = Tooltip;
/**
 * @property {func} callback - what, if anything should happen when the hover button is clicked
 * @property {object} content - a valid jsx object that you want displayed inside the hover tooltip
 * @property {string} id - id applied to the wrapper
 * @property {string} theme - classes applied to the hover button
 * @property {string} themeTooltip - classes applied to the hover tooltip itself, default uses the nwc sass styles
 * @property {string} title - the title that appears on the button
 * @property {number} width - how wide the tooltip should be, defaults to 300
 */

exports["default"] = _default;

//# sourceMappingURL=Tooltip.js.map