"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = require("prop-types");

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

var Breadcrumbs = function Breadcrumbs(_ref) {
  var crumbs = _ref.crumbs,
      delimiter = _ref.delimiter,
      delimiterTheme = _ref.delimiterTheme,
      id = _ref.id,
      linkClass = _ref.linkClass,
      schema = _ref.schema,
      theme = _ref.theme;
  (0, _react.useEffect)(function () {
    // build our schema markup
    if (schema === true) {
      var itemListElement = [];

      for (var i = 0; i < crumbs.length; i++) {
        itemListElement.push({
          '@type': 'ListItem',
          position: i + 1,
          name: crumbs[i].title,
          item: "".concat(window.location.origin).concat(crumbs[i].url)
        });
      } // insert the schema into the src


      var el = document.createElement('script');
      el.type = 'application/ld+json';
      el.text = JSON.stringify({
        '@context': 'https://schema.org',
        '@type': 'BreadcrumbList',
        itemListElement: itemListElement
      });
      document.querySelector('head').appendChild(el);
    }
  }, []);
  return /*#__PURE__*/_react["default"].createElement("section", {
    "data-test": "component-breadcrumb",
    className: "nwc--breadcrumb ".concat(theme),
    id: id
  }, crumbs.map(function (c, i) {
    return /*#__PURE__*/_react["default"].createElement("span", {
      key: c.title
    }, c.active === true ? c.title : /*#__PURE__*/_react["default"].createElement("a", {
      href: c.url,
      className: linkClass
    }, c.title), i < crumbs.length - 1 && /*#__PURE__*/_react["default"].createElement("span", {
      "data-test": "component-breadcrumb-delimiter",
      className: "nwc--delimiter ".concat(delimiterTheme)
    }, " ".concat(delimiter, " ")));
  }));
};

Breadcrumbs.defaultProps = {
  delimiter: '/',
  delimiterTheme: 'font--grey',
  id: null,
  linkClass: null,
  schema: false,
  theme: null
};
Breadcrumbs.propTypes = {
  crumbs: _propTypes.PropTypes.array.isRequired,
  delimiter: _propTypes.PropTypes.string,
  delimiterTheme: _propTypes.PropTypes.string,
  id: _propTypes.PropTypes.string,
  linkDlass: _propTypes.PropTypes.string,
  schema: _propTypes.PropTypes.bool,
  theme: _propTypes.PropTypes.string
};
var _default = Breadcrumbs;
/**
 * Breadcrumbs to show your progress in the flow
 *
 * @property {array} crumbs - The array of breadcrumbs required { title: string, url: string, active: bool }
 * @property {string} delimiter - The character to go in between each crumb, defaults to /
 * @property {string} delimiterTheme - The class applied to the delimiter, in case you want to change color or something easily
 * @property {string} id - Id to be added to the wrapper
 * @property {string} linkClass - Class to be added to each link
 * @property {object} schema - A JSON object for the JSON schema for search engines
 * @property {string} theme - Classes to be applied to the wrapper
 */

exports["default"] = _default;

//# sourceMappingURL=Breadcrumbs.js.map