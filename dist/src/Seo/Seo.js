"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = require("prop-types");

var _reactHelmet = require("react-helmet");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var Seo = function Seo(_ref) {
  var children = _ref.children,
      lang = _ref.lang,
      meta = _ref.meta,
      openGraph = _ref.openGraph,
      schema = _ref.schema,
      title = _ref.title;
  return /*#__PURE__*/_react["default"].createElement(_reactHelmet.Helmet, {
    lang: lang,
    "data-test": "component-seo"
  }, /*#__PURE__*/_react["default"].createElement("title", null, title), meta && meta.length > 0 && meta.map(function (item) {
    return /*#__PURE__*/_react["default"].createElement("meta", {
      key: item.name,
      name: item.name,
      content: item.content
    });
  }), schema && schema.length > 0 && schema.map(function (item) {
    return /*#__PURE__*/_react["default"].createElement("script", {
      type: "application/ld+json",
      key: item
    }, JSON.stringify(item).replace(/_/g, '@'));
  }), openGraph && Object.keys(openGraph).map(function (key) {
    return /*#__PURE__*/_react["default"].createElement("meta", {
      property: "og:".concat(key),
      content: openGraph[key],
      key: openGraph[key]
    });
  }), children);
};

Seo.defaultProps = {
  children: null,
  lang: 'en',
  meta: [],
  og: {},
  schema: []
};
Seo.propTypes = {
  children: _propTypes.PropTypes.object,
  lang: _propTypes.PropTypes.string,
  meta: _propTypes.PropTypes.array,
  openGraph: _propTypes.PropTypes.object,
  schema: _propTypes.PropTypes.array,
  title: _propTypes.PropTypes.string.isRequired
};
var _default = Seo;
/**
 * @property {object} children - Valid JSX object, that will be injected into the head
 * @property {string} lang - Language to be specified, if not provided, en will be used
 * @property {array} meta - an array of all meta elements to be populated meta: [{ name: 'description', content: 'My description', }],
 * @property {object} openGraph - Facebook's open graph to be populated in the head: { key: value }
 * @property {array} schema - Any valid schema data to be populated, comma separate each object
 * @property {string} title - The page title to be populated
 */

exports["default"] = _default;

//# sourceMappingURL=Seo.js.map