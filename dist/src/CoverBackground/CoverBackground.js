'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true,
});
exports['default'] = void 0;

var _react = _interopRequireDefault(require('react'));

var _propTypes = require('prop-types');

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

function ownKeys(object, enumerableOnly) {
  var keys = Object.keys(object);
  if (Object.getOwnPropertySymbols) {
    var symbols = Object.getOwnPropertySymbols(object);
    if (enumerableOnly)
      symbols = symbols.filter(function(sym) {
        return Object.getOwnPropertyDescriptor(object, sym).enumerable;
      });
    keys.push.apply(keys, symbols);
  }
  return keys;
}

function _objectSpread(target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i] != null ? arguments[i] : {};
    if (i % 2) {
      ownKeys(Object(source), true).forEach(function(key) {
        _defineProperty(target, key, source[key]);
      });
    } else if (Object.getOwnPropertyDescriptors) {
      Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));
    } else {
      ownKeys(Object(source)).forEach(function(key) {
        Object.defineProperty(
          target,
          key,
          Object.getOwnPropertyDescriptor(source, key),
        );
      });
    }
  }
  return target;
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true,
    });
  } else {
    obj[key] = value;
  }
  return obj;
}

var CoverBackground = function CoverBackground(_ref) {
  var backgroundColor = _ref.backgroundColor,
    children = _ref.children,
    minHeight = _ref.minHeight,
    mp4 = _ref.mp4,
    src = _ref.src,
    theme = _ref.theme,
    webm = _ref.webm;
  var backgroundStyle = {
    backgroundImage: 'url('.concat(src, ')'),
    backgroundColor: backgroundColor,
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
    overflow: 'hidden',
    position: 'relative',
  };
  return /*#__PURE__*/ _react['default'].createElement(
    'div',
    {
      'data-test': 'component-cover-background',
      id: 'nwc--cover-background',
      className: theme,
      style: _objectSpread({}, backgroundStyle, {
        minHeight: minHeight,
      }),
    },
    (webm || mp4) &&
      /*#__PURE__*/ _react['default'].createElement(
        'video',
        {
          id: 'nwc--video',
          playsInline: true,
          autoPlay: true,
          muted: true,
          loop: true,
          style: {
            position: 'absolute',
            top: '50%',
            left: '50%',
            minWidth: '100%',
            minHeight: '100%',
            width: 'auto',
            height: 'auto',
            zIndex: 1,
            transform: 'translateX(-50%) translateY(-50%)',
            backgroundSize: 'cover',
            transition: '1s opacity',
          },
        },
        /*#__PURE__*/ _react['default'].createElement('source', {
          'data-test': 'component-cover-background-webm',
          src: webm,
          type: 'video/webm',
        }),
        /*#__PURE__*/ _react['default'].createElement('source', {
          'data-test': 'component-cover-background-mp4',
          src: mp4,
          type: 'video/mp4',
        }),
      ),
    /*#__PURE__*/ _react['default'].createElement(
      'div',
      {
        style: {
          zIndex: 9,
          position: 'relative',
        },
      },
      children,
    ),
  );
};

CoverBackground.defaultProps = {
  backgroundColor: '#733791',
  minHeight: 100,
  mp4: null,
  theme: 'padding--lg align--center justify--center',
  webm: null,
};
CoverBackground.propTypes = {
  backgroundColor: _propTypes.PropTypes.string,
  children: _propTypes.PropTypes.object.isRequired,
  minHeight: _propTypes.PropTypes.number,
  mp4: _propTypes.PropTypes.string,
  src: _propTypes.PropTypes.string.isRequired,
  theme: _propTypes.PropTypes.string,
  webm: _propTypes.PropTypes.string,
};
var _default = CoverBackground;
/**
 * @property {string} backgroundColor - The default background color behind the image or video
 * @property {object} children - Any valid JSX to show inside of the div, you need to style these on your own
 * @property {number} minHeight - A set minimum height of the div, defaults to 100, you can pass 0 to override if you want no min height
 * @property {string} mp4 - If using video, the mp4 source
 * @property {string} src - The image src if using an image
 * @property {string} theme - Classes to be applied, if none, Stiles will center it vertically and horizontally if you're using our scss
 * @property {string} webm - If using video, the webm source
 */

exports['default'] = _default;

//# sourceMappingURL=CoverBackground.js.map
