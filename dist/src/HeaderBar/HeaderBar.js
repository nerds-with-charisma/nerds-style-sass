"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = require("prop-types");

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

var HeaderBar = function HeaderBar(_ref) {
  var backgroundColor = _ref.backgroundColor,
      center = _ref.center,
      container = _ref.container,
      fontColor = _ref.fontColor,
      fixed = _ref.fixed,
      height = _ref.height,
      left = _ref.left,
      mobileOnly = _ref.mobileOnly,
      right = _ref.right,
      theme = _ref.theme;
  (0, _react.useEffect)(function () {
    if (fixed === true) document.getElementsByTagName('body')[0].style.marginTop = "".concat(height + 10, "px");
  }, []);
  if (mobileOnly === true && typeof window !== 'undefined' && window.innerWidth > 991) return false;
  return /*#__PURE__*/_react["default"].createElement("section", {
    "data-test": "component-header-bar",
    id: "nwc--header-bar",
    className: theme,
    style: {
      height: height,
      lineHeight: "".concat(height, "px"),
      backgroundColor: backgroundColor,
      color: fontColor,
      position: fixed === true ? 'fixed' : 'relative',
      width: fixed === true ? '100%' : null,
      top: 0,
      left: 0
    }
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: container,
    style: {
      display: 'flex',
      justifyContent: 'center',
      alignContent: 'center'
    }
  }, /*#__PURE__*/_react["default"].createElement("div", {
    "data-test": "component-header-bar-left",
    className: "nwc--header-left",
    style: leftStyle
  }, left), /*#__PURE__*/_react["default"].createElement("div", {
    "data-test": "component-header-bar-center",
    className: "nwc--header-center",
    style: centerStyle
  }, center), /*#__PURE__*/_react["default"].createElement("div", {
    "data-test": "component-header-bar-right",
    className: "nwc--header-right",
    style: rightStyle
  }, right)));
};

var leftStyle = {
  display: 'inline-block',
  marginRight: 10,
  flex: 1
};
var centerStyle = {
  display: 'inline-block',
  marginRight: 10,
  flex: 2
};
var rightStyle = {
  display: 'inline-block',
  textAlign: 'right',
  flex: 1
};
HeaderBar.defaultProps = {
  backgroundColor: '#fff',
  container: 'container--lg',
  fontColor: '#000',
  fixed: false,
  height: 56,
  mobileOnly: false,
  theme: 'shadow--sm'
};
HeaderBar.propTypes = {
  backgroundColor: _propTypes.PropTypes.string,
  center: _propTypes.PropTypes.object.isRequired,
  container: _propTypes.PropTypes.string,
  fontColor: _propTypes.PropTypes.string,
  fixed: _propTypes.PropTypes.bool,
  height: _propTypes.PropTypes.number,
  left: _propTypes.PropTypes.object.isRequired,
  mobileOnly: _propTypes.PropTypes.bool,
  right: _propTypes.PropTypes.object.isRequired,
  theme: _propTypes.PropTypes.string
};
var _default = HeaderBar;
/**
 * @property {string} backgroundColor - The color of the headerbar, any hex or rgba value should work, white by defaul
 * @property {object} center - The center content, any valid JSX element
 * @property {string} container - The container class around the left, center, right sections. If using Stiles library and valid container will work: container, container--lg, etc...
 * @property {string} fontColor - The font color of the items in the header
 * @property {bool} fixed - If the header bar should stay fixed at the top while scrolling (will add margin to the body if true)
 * @property {number} height - How tall the header bar should be, defaults to 56
 * @property {object} left - The left content, any valid JSX element, most common use is with the hamburger component
 * @property {bool} mobileOnly - If true, the header will not show on desktop, else it will show for all devices
 * @property {object} right - The right content, any valid JSX element
 * @property {string} theme - Classes to be applied to the wrapper
 */

exports["default"] = _default;

//# sourceMappingURL=HeaderBar.js.map