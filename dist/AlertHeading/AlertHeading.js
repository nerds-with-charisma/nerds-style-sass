"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = require("prop-types");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var AlertHeading = function AlertHeading(_ref) {
  var children = _ref.children,
      theme = _ref.theme;
  return /*#__PURE__*/_react["default"].createElement("div", {
    "data-test": "component-alertHeading",
    className: "nwc--alert-heading ".concat(theme)
  }, children);
};

AlertHeading.defaultProps = {
  theme: ''
};
AlertHeading.propTypes = {
  children: _propTypes.PropTypes.object.isRequired,
  theme: _propTypes.PropTypes.string
};
var _default = AlertHeading;
/**
 * An alert div that can be info, success, or error...or not, u decide. Pass "theme" and style it the way you want.
 *
 * @property {object} children - Pass any valid jsx object in-between.
 * @property {string} theme -[info, error, success], also any custom classes you want to pass
 * *
 */

exports["default"] = _default;