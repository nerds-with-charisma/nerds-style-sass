"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var _react = _interopRequireDefault(require("react"));

var _enzyme = _interopRequireWildcard(require("enzyme"));

var _enzymeAdapterReact = _interopRequireDefault(require("enzyme-adapter-react-16"));

var _Input = _interopRequireDefault(require("./Input"));

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

_enzyme["default"].configure({
  adapter: new _enzymeAdapterReact["default"]()
});

var setup = function setup() {
  var props = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var state = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
  return (0, _enzyme.shallow)( /*#__PURE__*/_react["default"].createElement(_Input["default"], {
    autofill: props.autofill,
    blurCallback: props.blurCallback,
    callback: props.callback,
    error: props.error,
    id: props.id,
    maxLength: props.maxLength,
    minLength: props.minLength,
    optional: props.optional,
    pattern: props.pattern,
    placeHolder: props.placeHolder,
    populatedValue: props.populatedValue,
    required: props.required,
    theme: props.theme,
    title: props.title,
    type: props.type
  }));
};

var testProps = {
  autofill: 'nope',
  blurCallback: function blurCallback(v) {
    return console.log(v);
  },
  callback: function callback(v) {
    return console.log(v);
  },
  error: 'Error Test',
  id: 'input--test',
  maxLength: 40,
  minLength: 40,
  optional: true,
  pattern: '/[A-Za-z]{3}/',
  placeHolder: 'Placeholder Test',
  populatedValue: 'Test Value',
  required: true,
  theme: 'test',
  title: 'Test',
  type: 'text'
}; // find by test attr

var findByTestAttr = function findByTestAttr(wrapper, val) {
  return wrapper.find("[data-test=\"".concat(val, "\"]"));
}; // Test


test('Input renders without crashing', function () {
  var wrapper = setup(testProps);
  var appComponent = findByTestAttr(wrapper, 'component-input');
  expect(appComponent.length).toBe(1);
}); // autofill

test('Input autofill is working', function () {
  var wrapper = setup(testProps);
  var appComponent = findByTestAttr(wrapper, 'component-input');
  expect(appComponent.find('input').prop('autofill')).toBe('nope');
}); // blurCallback

describe('Input blur callback value is returned', function () {
  test('function is called on  blur', function () {
    var callback = jest.fn(function (v) {
      return v;
    });
    var wrapper = (0, _enzyme.shallow)( /*#__PURE__*/_react["default"].createElement(_Input["default"], {
      id: "test",
      blurCallback: callback,
      callback: function callback() {
        return true;
      },
      title: "test"
    }));
    var input = findByTestAttr(wrapper, 'component-input-input');
    var mockEvent = {
      target: {
        value: 'blur value'
      }
    };
    input.simulate('blur', mockEvent);
    var callbackItem = callback.mock.results[0].value;
    expect(callback).toHaveBeenCalled();
    expect(callbackItem).toBe('blur value');
  });
}); // callback

describe('Input callback value is returned', function () {
  test('function is called on click', function () {
    var callback = jest.fn(function (v) {
      return v;
    });
    var wrapper = (0, _enzyme.shallow)( /*#__PURE__*/_react["default"].createElement(_Input["default"], {
      id: "test",
      callback: callback,
      title: "test"
    }));
    var input = findByTestAttr(wrapper, 'component-input-input');
    var mockEvent = {
      target: {
        value: 'test value'
      }
    };
    input.simulate('change', mockEvent);
    var callbackItem = callback.mock.results[0].value;
    expect(callback).toHaveBeenCalled();
    expect(callbackItem).toBe('test value');
  });
}); // error

test('Input error works', function () {
  var wrapper = setup(testProps);
  var appComponent = findByTestAttr(wrapper, 'component-input-error');
  expect(appComponent.text()).toBe('Error Test');
}); // id

test('Input id is set', function () {
  var wrapper = setup(testProps);
  var appComponent = findByTestAttr(wrapper, 'component-input-input');
  expect(appComponent.prop('id')).toBe('input--test');
}); // maxLength

test('Input max length is set', function () {
  var wrapper = setup(testProps);
  var appComponent = findByTestAttr(wrapper, 'component-input-input');
  expect(appComponent.prop('maxLength')).toBe(40);
}); // minLength

test('Input min length is set', function () {
  var wrapper = setup(testProps);
  var appComponent = findByTestAttr(wrapper, 'component-input-input');
  expect(appComponent.prop('minLength')).toBe(40);
}); // optional

test('Input optional text shows', function () {
  var wrapper = setup(testProps);
  var appComponent = findByTestAttr(wrapper, 'component-input-optional');
  expect(appComponent.length).toBe(1);
}); // pattern

test('Input pattern renders', function () {
  var wrapper = setup(testProps);
  var appComponent = findByTestAttr(wrapper, 'component-input-input');
  expect(appComponent.prop('pattern')).toBe('/[A-Za-z]{3}/');
}); // placeHolder

test('Input placeholder gets populated', function () {
  var wrapper = setup(testProps);
  var appComponent = findByTestAttr(wrapper, 'component-input-input');
  expect(appComponent.prop('placeholder')).toBe('Placeholder Test');
}); // populatedValue

test('Input initial popluated value works', function () {
  var wrapper = setup(testProps);
  var appComponent = findByTestAttr(wrapper, 'component-input-input');
  var defaultValue = JSON.stringify(appComponent.debug());
  expect(defaultValue.includes('Test')).toBe(true);
}); // required

test('Input required text shows', function () {
  var wrapper = setup(testProps);
  var appComponent = findByTestAttr(wrapper, 'component-input-required');
  expect(appComponent.length).toBe(1);
}); // theme

test('Input theme styles render', function () {
  var wrapper = setup(testProps);
  var appComponent = findByTestAttr(wrapper, 'component-input');
  expect(appComponent.find('.nwc--input.test').length).toBe(1);
}); // title

test('Input title renders', function () {
  var wrapper = setup(testProps);
  var appComponent = findByTestAttr(wrapper, 'component-input-title');
  expect(appComponent.text()).toBe('Test');
}); // type

test('Input id is set', function () {
  var wrapper = setup(testProps);
  var appComponent = findByTestAttr(wrapper, 'component-input-input');
  expect(appComponent.prop('type')).toBe('text');
});