"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = require("prop-types");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var Input = function Input(_ref) {
  var autofill = _ref.autofill,
      blurCallback = _ref.blurCallback,
      callback = _ref.callback,
      disabled = _ref.disabled,
      error = _ref.error,
      id = _ref.id,
      inputTheme = _ref.inputTheme,
      keyUpCallback = _ref.keyUpCallback,
      maxLength = _ref.maxLength,
      minLength = _ref.minLength,
      optional = _ref.optional,
      pattern = _ref.pattern,
      placeHolder = _ref.placeHolder,
      populatedValue = _ref.populatedValue,
      required = _ref.required,
      theme = _ref.theme,
      title = _ref.title,
      type = _ref.type;
  return /*#__PURE__*/_react["default"].createElement("div", {
    className: "margin--bottom"
  }, /*#__PURE__*/_react["default"].createElement("label", {
    "data-test": "component-input",
    className: "nwc--input ".concat(theme),
    htmlFor: id
  }, title && /*#__PURE__*/_react["default"].createElement("div", {
    className: "nwc--label-wrap"
  }, /*#__PURE__*/_react["default"].createElement("strong", {
    className: "font--14"
  }, /*#__PURE__*/_react["default"].createElement("span", {
    "data-test": "component-input-title"
  }, title), required === true && /*#__PURE__*/_react["default"].createElement("sup", {
    "data-test": "component-input-required",
    className: "font--error font--10"
  }, ' *'), optional === true && /*#__PURE__*/_react["default"].createElement("sup", {
    "data-test": "component-input-optional",
    className: "font--grey font--10"
  }, ' (optional)'))), /*#__PURE__*/_react["default"].createElement("input", {
    "data-test": "component-input-input",
    autofill: autofill,
    className: "border paddingMD font16 ".concat(inputTheme),
    defaultValue: populatedValue,
    disabled: disabled,
    id: id,
    maxLength: maxLength,
    minLength: minLength,
    name: id,
    onBlur: function onBlur(e) {
      return blurCallback(e.target.value, e);
    },
    onChange: function onChange(e) {
      return callback(e.target.value, e);
    },
    onKeyUp: function onKeyUp(e) {
      return keyUpCallback(e);
    },
    pattern: pattern,
    placeholder: placeHolder,
    type: type
  }), error !== null && /*#__PURE__*/_react["default"].createElement("aside", {
    "data-test": "component-input-error",
    className: "nwc--validation nwc--validation--error font--12"
  }, error)));
};

Input.defaultProps = {
  autofill: 'yes',
  blurCallback: function blurCallback() {
    return true;
  },
  disabled: false,
  error: null,
  inputTheme: '',
  keyUpCallback: function keyUpCallback() {
    return true;
  },
  maxLength: 50,
  minLength: 1,
  optional: false,
  pattern: null,
  placeholder: null,
  populatedValue: null,
  required: false,
  theme: '',
  type: 'text'
};
Input.propTypes = {
  autofill: _propTypes.PropTypes.string,
  blurCallback: _propTypes.PropTypes.func,
  callback: _propTypes.PropTypes.func.isRequired,
  disabled: _propTypes.PropTypes.bool,
  error: _propTypes.PropTypes.string,
  id: _propTypes.PropTypes.string.isRequired,
  inputTheme: _propTypes.PropTypes.string,
  keyUpCallback: _propTypes.PropTypes.func,
  maxLength: _propTypes.PropTypes.number,
  minLength: _propTypes.PropTypes.number,
  optional: _propTypes.PropTypes.bool,
  pattern: _propTypes.PropTypes.string,
  populatedValue: _propTypes.PropTypes.string,
  placeHolder: _propTypes.PropTypes.string,
  required: _propTypes.PropTypes.bool,
  theme: _propTypes.PropTypes.string,
  title: _propTypes.PropTypes.string.isRequired,
  type: _propTypes.PropTypes.string
};
var _default = Input;
/**
 * Super customizable inputs that you can style to ur hearts desire.
 * Callbacks for blur and change let you control logic in your parent component easily.
 *
 * @property {string} autofill - If the input should allow autofilling from the browser. This is not always handled properly in Chrome, so we suggest to use a string like 'nope'.
 * @property {func} blurCallback - The function to run when the input loses focus/blurred.
 * @property {func} callback - A function that will run after the input  has changed, it will return the value of the input for you to use in the parent component.
 * @property {bool} disabled - Should the input be disabled or not. Use with a state var to determine when the input can be edited.
 * @property {string} error - An error message to be shown docked at the bottom of the input. If null, nothing will show.
 * @property {string} id - An id to be added to the input itself, not the wrapping label.
 * @property {string} inputTheme - Classes to be added to the input itself.
 * @property {func} keyUpCallback - Callback when a key is released
 * @property {number} maxLength - The allowed max length of characters into the input.
 * @property {number} minLength - The allowed min lenght of characters into the input.
 * @property {bool} optional - If true, will show an (optional) tag next to the title. If false or not supplied, nothing will show.
 * @property {string} pattern - Regex to match on the input.
 * @property {string} placeHolder - Placeholder value to show on the input before any value is entered.
 * @property {string} populatedValue - The default value to be populated into the input on load.
 * @property {bool} required - If true, will show a required *'s. If false or not supplied, nothing will show.
 * @property {string} theme - Classes to be added to the wrapping label.
 * @property {string} title - The label text title of the input.
 * @property {string} type - The type attribute to determine what sort of input is used (text, email, etc).
 *
 */

exports["default"] = _default;