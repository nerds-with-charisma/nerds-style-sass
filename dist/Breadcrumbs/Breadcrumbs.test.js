"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var _react = _interopRequireDefault(require("react"));

var _enzyme = _interopRequireWildcard(require("enzyme"));

var _enzymeAdapterReact = _interopRequireDefault(require("enzyme-adapter-react-16"));

var _Breadcrumbs = _interopRequireDefault(require("./Breadcrumbs"));

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

_enzyme["default"].configure({
  adapter: new _enzymeAdapterReact["default"]()
});

var setup = function setup() {
  var props = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var state = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
  return (0, _enzyme.shallow)( /*#__PURE__*/_react["default"].createElement(_Breadcrumbs["default"], {
    crumbs: props.crumbs,
    delimiter: props.delimiter,
    delimiterTheme: props.delimiterTheme,
    id: props.id,
    linkClass: props.linkClass,
    schema: true,
    theme: props.theme
  }));
};

var testProps = {
  crumbs: [{
    title: 'Home',
    url: '/',
    active: false
  }, {
    title: 'About',
    url: '/about',
    active: true
  }],
  delimiter: '/',
  delimiterTheme: 'test-delimiter',
  id: 'test',
  linkClass: 'link-class',
  schema: true,
  theme: 'test-class'
}; // find by test attr

var findByTestAttr = function findByTestAttr(wrapper, val) {
  return wrapper.find("[data-test=\"".concat(val, "\"]"));
}; // Tests


test('Breadcrumb renders without crashing', function () {
  var wrapper = setup(testProps);
  var appComponent = findByTestAttr(wrapper, 'component-breadcrumb');
  expect(appComponent.length).toBe(1);
}); // crumbs

test('Breadcrumb links render', function () {
  var wrapper = setup(testProps);
  var appComponent = findByTestAttr(wrapper, 'component-breadcrumb');
  var crumbs = appComponent.find('span');
  expect(crumbs.length).toBe(3);
}); // delimiter: '/'

test('Delimiter renders right', function () {
  var wrapper = setup(testProps);
  var appComponent = findByTestAttr(wrapper, 'component-breadcrumb-delimiter');
  var delimiterCount = testProps.crumbs.length - appComponent.length;
  expect(delimiterCount).toBe(1);
}); // delimiterTheme

test('Delimiter theme styles are applied', function () {
  var wrapper = setup(testProps);
  var appComponent = findByTestAttr(wrapper, 'component-breadcrumb-delimiter');
  expect(appComponent.prop('className')).toBe('nwc--delimiter test-delimiter');
}); // id

test('Delimiter id is appended', function () {
  var wrapper = setup(testProps);
  expect(wrapper.find('#test').length).toBe(1);
}); // linkClass

test('Breadcrumb link classes are appliled', function () {
  var wrapper = setup(testProps);
  expect(wrapper.find('a').prop('className')).toBe('link-class');
}); // theme

test('Breadcrumb schema renders', function () {
  var wrapper = setup(testProps);
  expect(wrapper.prop('className')).toBe('nwc--breadcrumb test-class');
});