"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = require("prop-types");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var Checkbox = function Checkbox(_ref) {
  var callback = _ref.callback,
      checked = _ref.checked,
      dataAttr = _ref.dataAttr,
      disabled = _ref.disabled,
      id = _ref.id,
      theme = _ref.theme,
      title = _ref.title,
      value = _ref.value;
  return /*#__PURE__*/_react["default"].createElement("label", {
    className: "nwc--checkbox ".concat(theme),
    "data-test": "component-checkbox",
    id: id
  }, /*#__PURE__*/_react["default"].createElement("input", {
    "data-attr": dataAttr,
    "data-test": "component-checkbox-input",
    defaultChecked: checked,
    disabled: disabled,
    onClick: function onClick(e) {
      return callback(value, e);
    },
    type: "checkbox",
    value: value
  }), /*#__PURE__*/_react["default"].createElement("div", null, /*#__PURE__*/_react["default"].createElement("strong", {
    "data-test": "component-checkbox-title",
    className: "nwc--checkbox-title font--14"
  }, title)));
};

Checkbox.defaultProps = {
  checked: false,
  dataAttr: null,
  disabled: false,
  id: null,
  theme: ''
};
Checkbox.propTypes = {
  callback: _propTypes.PropTypes.func.isRequired,
  checked: _propTypes.PropTypes.bool,
  dataAttr: _propTypes.PropTypes.string,
  disabled: _propTypes.PropTypes.bool,
  id: _propTypes.PropTypes.string,
  theme: _propTypes.PropTypes.string,
  title: _propTypes.PropTypes.string.isRequired,
  value: _propTypes.PropTypes.string.isRequired
};
var _default = Checkbox;
/**
 * Checkbox input with callbacks
 *
 *
 * @property {func} callback - A function that will run after the input  has changed, it will return the value of the input for you to use in the parent component.
 * @property {bool} checked - Should the checkbox show as checked or not.
 * @property {string} dataAttr - A custom data attribute value you can pass to hook into the input if needed.
 * @property {bool} disabled - Should the input be disabled or not. Use with a state var to determine when the input can be edited.
 * @property {string} id - An id to be added to the input itself, not the wrapping label.
 * @property {string} theme - Classes to be added to the wrapping label.
 * @property {string} title - The label text title of the input.
 * @property {string} value - The value of the checkbox, will be accessible as part of "e" in the callback
 */

exports["default"] = _default;