"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var _react = _interopRequireDefault(require("react"));

var _enzyme = _interopRequireWildcard(require("enzyme"));

var _enzymeAdapterReact = _interopRequireDefault(require("enzyme-adapter-react-16"));

var _HeaderBar = _interopRequireDefault(require("./HeaderBar"));

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

_enzyme["default"].configure({
  adapter: new _enzymeAdapterReact["default"]()
});

var setup = function setup() {
  var props = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var state = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
  return (0, _enzyme.shallow)( /*#__PURE__*/_react["default"].createElement(_HeaderBar["default"], {
    fixed: props.fixed,
    center: props.center,
    left: props.left,
    right: props.right,
    backgroundColor: props.backgroundColor,
    container: props.container,
    fontColor: props.fontColor,
    height: props.height,
    mobileOnly: props.mobileOnly,
    theme: props.theme
  }));
};

var testProps = {
  fixed: true,
  center: /*#__PURE__*/_react["default"].createElement("strong", null, 'Center'),
  left: /*#__PURE__*/_react["default"].createElement("span", null, 'Left'),
  right: /*#__PURE__*/_react["default"].createElement("span", null, 'Right'),
  backgroundColor: 'red',
  container: 'container',
  fontColor: 'blue',
  height: 100,
  mobileOnly: false,
  theme: 'test-class'
};
var defaultProps = {
  mobileOnly: true,
  center: /*#__PURE__*/_react["default"].createElement("strong", null, 'Center'),
  left: /*#__PURE__*/_react["default"].createElement("span", null, 'Left'),
  right: /*#__PURE__*/_react["default"].createElement("span", null, 'Right')
};
var heightProps = {
  fixed: true,
  center: /*#__PURE__*/_react["default"].createElement("strong", null, 'Center'),
  left: /*#__PURE__*/_react["default"].createElement("span", null, 'Left'),
  right: /*#__PURE__*/_react["default"].createElement("span", null, 'Right'),
  backgroundColor: 'red',
  container: 'container',
  fontColor: 'blue',
  mobileOnly: false,
  theme: 'test-class'
}; // find by test attr

var findByTestAttr = function findByTestAttr(wrapper, val) {
  return wrapper.find("[data-test=\"".concat(val, "\"]"));
};

var wrapper = setup(testProps);
var defaultWrapper = setup(defaultProps);
var heightWrapper = setup(heightProps);
var mainComponent = findByTestAttr(wrapper, 'component-header-bar');
var defaultComponent = findByTestAttr(defaultWrapper, 'component-header-bar');
var heightComponent = findByTestAttr(heightWrapper, 'component-header-bar');
var centerComponent = findByTestAttr(wrapper, 'component-header-bar-center');
var letfComponent = findByTestAttr(wrapper, 'component-header-bar-left');
var rightComponent = findByTestAttr(wrapper, 'component-header-bar-right'); // Test

test('HeaderBar renders without crashing', function () {
  expect(mainComponent.length).toBe(1);
}); // fixed

test('HeaderBar gets fixed styles when prop is true', function () {
  expect(mainComponent.prop('style').position).toBe('fixed');
}); // center

test('HeaderBar center prop children render', function () {
  expect(centerComponent.find('strong').text()).toBe('Center');
}); // left

test('HeaderBar left prop children render', function () {
  expect(letfComponent.find('span').text()).toBe('Left');
}); // right

test('HeaderBar right prop children render', function () {
  expect(rightComponent.find('span').text()).toBe('Right');
}); // backgroundColor

test('HeaderBar background color gets set', function () {
  expect(mainComponent.prop('style').backgroundColor).toBe('red');
}); // container

test('HeaderBar container gets applied', function () {
  expect(mainComponent.find('.container').length).toBe(1);
}); // fontColor

test('HeaderBar text color gets set', function () {
  expect(mainComponent.prop('style').color).toBe('blue');
}); // height

test('HeaderBar height prop gets set', function () {
  expect(mainComponent.prop('style').height).toBe(100);
  expect(heightComponent.prop('style').height).toBe(56);
}); // mobileOnly

test("HeaderBar doesn't show up on desktop", function () {
  expect(defaultComponent.length).toBe(0);
}); // theme

test('Hamburger renders without crashing', function () {
  expect(mainComponent.find('.test-class').length).toBe(1);
});