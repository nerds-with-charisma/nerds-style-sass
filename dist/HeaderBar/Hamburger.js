"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = require("prop-types");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var Hamburger = function Hamburger(_ref) {
  var callback = _ref.callback,
      color = _ref.color,
      height = _ref.height,
      showClose = _ref.showClose;
  var hamburgerStyle = {
    width: 27,
    height: 2,
    backgroundColor: color,
    margin: '0 0 6px 0',
    transition: '0.4s'
  };
  var isClosed1 = {
    transform: 'rotate(-45deg) translate(-6px, 8px)'
  };
  var isClosed2 = {
    opacity: 0
  };
  var isClosed3 = {
    transform: 'rotate(45deg) translate(-3px, -6px)'
  };
  return /*#__PURE__*/_react["default"].createElement("div", {
    "data-test": "component-hamburger",
    className: "hamburger",
    style: {
      paddingTop: "".concat(height / 3, "px")
    },
    onClick: function onClick() {
      return callback();
    }
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "bar1",
    style: showClose === true ? _objectSpread({}, isClosed1, {}, hamburgerStyle) : hamburgerStyle
  }), /*#__PURE__*/_react["default"].createElement("div", {
    className: "bar2",
    style: showClose === true ? _objectSpread({}, isClosed2, {}, hamburgerStyle) : hamburgerStyle
  }), /*#__PURE__*/_react["default"].createElement("div", {
    className: "bar3",
    style: showClose === true ? _objectSpread({}, isClosed3, {}, hamburgerStyle) : hamburgerStyle
  }));
};

Hamburger.defaultProps = {
  height: 56,
  color: '#000',
  showClose: false
};
Hamburger.propTypes = {
  callback: _propTypes.PropTypes.func.isRequired,
  color: _propTypes.PropTypes.string,
  height: _propTypes.PropTypes.number,
  showClose: _propTypes.PropTypes.bool
};
var _default = Hamburger;
/**
 * @property {func} callback - The callback function when the icon is clicked
 * @property {string} color - The color of the icon
 * @property {number} height - How tall the icon should be
 * @property {bool} showClose - Toggle to show the close button or the hamburger, track with your own state
 */

exports["default"] = _default;