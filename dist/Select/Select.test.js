"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var _react = _interopRequireDefault(require("react"));

var _enzyme = _interopRequireWildcard(require("enzyme"));

var _enzymeAdapterReact = _interopRequireDefault(require("enzyme-adapter-react-16"));

var _Select = _interopRequireDefault(require("./Select"));

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

_enzyme["default"].configure({
  adapter: new _enzymeAdapterReact["default"]()
}); // get a copy of the dom for our component


var setup = function setup() {
  var props = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var state = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
  return (0, _enzyme.shallow)( /*#__PURE__*/_react["default"].createElement(_Select["default"], {
    callback: props.callback,
    error: props.error,
    fullWidth: props.fullWidth,
    id: props.id,
    options: props.options,
    theme: props.theme,
    title: props.title,
    required: props.required,
    optional: props.optional
  }));
};

var testProps = {
  callback: function callback(v) {
    return console.log(v);
  },
  error: 'Test error',
  fullWidth: true,
  id: 'testId',
  options: [],
  theme: 'test-class',
  title: 'Test',
  required: true,
  optional: false
}; // find by test attr

var findByTestAttr = function findByTestAttr(wrapper, val) {
  return wrapper.find("[data-test=\"".concat(val, "\"]"));
}; // make sure the component renders


test('Select renders without crashing', function () {
  var wrapper = setup(testProps);
  var appComponent = findByTestAttr(wrapper, 'component-select');
  expect(appComponent.length).toBe(1);
}); // callback

describe('Select callback value is returned', function () {
  test('function is called on change', function () {
    var options = [{
      title: 'Option 1',
      value: 'option-1',
      active: true
    }];
    var callback = jest.fn(function (v) {
      return v;
    });
    var wrapper = (0, _enzyme.shallow)( /*#__PURE__*/_react["default"].createElement(_Select["default"], {
      id: "test",
      callback: callback,
      title: "test",
      options: options
    }));
    var input = findByTestAttr(wrapper, 'component-select-input');
    var mockEvent = {
      target: {
        value: 'Option 1'
      }
    };
    input.simulate('change', mockEvent);
    var callbackItem = callback.mock.results[0].value;
    expect(callback).toHaveBeenCalled();
    expect(callbackItem).toBe('Option 1');
  });
}); // error

test('Select error shows', function () {
  var wrapper = setup(testProps);
  var appComponent = findByTestAttr(wrapper, 'component-select-error');
  expect(appComponent.length).toBe(1);
}); // fullWidth

test('Select full width style shows', function () {
  var wrapper = setup(testProps);
  var appComponent = findByTestAttr(wrapper, 'component-select');
  expect(appComponent.find('.full-width').length).toBe(1);
}); // id,

test('Select full width style shows', function () {
  var wrapper = setup(testProps);
  var appComponent = findByTestAttr(wrapper, 'component-select');
  expect(appComponent.find('#testId').length).toBe(1);
}); // theme: 'test-class',

test('Select theme classes are applied', function () {
  var wrapper = setup(testProps);
  expect(wrapper.find('.nwc--select.test-class').length).toBe(1);
}); //   title

test('Select title renders', function () {
  var wrapper = setup(testProps);
  var appComponent = findByTestAttr(wrapper, 'component-select-title');
  expect(appComponent.text()).toBe('Test');
}); // required

test('Select required renders', function () {
  var wrapper = setup(testProps);
  var appComponent = findByTestAttr(wrapper, 'component-select-required');
  expect(appComponent.length).toBe(1);
}); // optional

test('Select optional renders', function () {
  var wrapper = setup({
    callback: function callback(v) {
      return console.log(v);
    },
    error: 'Test error',
    fullWidth: true,
    id: 'testId',
    options: [],
    theme: 'test-class',
    title: 'Test',
    required: false,
    optional: true
  });
  var appComponent = findByTestAttr(wrapper, 'component-select-optional');
  expect(appComponent.length).toBe(1);
});