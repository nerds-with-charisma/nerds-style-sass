"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = require("prop-types");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var Select = function Select(_ref) {
  var callback = _ref.callback,
      error = _ref.error,
      fullWidth = _ref.fullWidth,
      id = _ref.id,
      options = _ref.options,
      theme = _ref.theme,
      title = _ref.title,
      required = _ref.required,
      optional = _ref.optional;
  return /*#__PURE__*/_react["default"].createElement("label", {
    "data-test": "component-select",
    className: "nwc--select ".concat(theme),
    htmlFor: id
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "nwc--label-wrap"
  }, /*#__PURE__*/_react["default"].createElement("strong", {
    className: "font--14"
  }, /*#__PURE__*/_react["default"].createElement("span", {
    "data-test": "component-select-title"
  }, title), required === true && /*#__PURE__*/_react["default"].createElement("sup", {
    "data-test": "component-select-required",
    className: "font--error font--10"
  }, ' *'), optional === true && /*#__PURE__*/_react["default"].createElement("sup", {
    "data-test": "component-select-optional",
    className: "font--grey font--10"
  }, ' (optional)'))), /*#__PURE__*/_react["default"].createElement("select", {
    "data-test": "component-select-input",
    id: id,
    className: fullWidth === true ? 'full-width' : '',
    onChange: function onChange(e) {
      return callback(e.target.value);
    }
  }, options.map(function (item) {
    return /*#__PURE__*/_react["default"].createElement("option", {
      key: item.value,
      value: item.value
    }, item.title);
  })), error !== null && /*#__PURE__*/_react["default"].createElement("aside", {
    "data-test": "component-select-error",
    className: "nwc--validation nwc--validation--error font--12"
  }, error));
};

Select.defaultProps = {
  fullWidth: false,
  error: null,
  theme: '',
  title: null,
  required: false,
  optional: false
};
Select.propTypes = {
  callback: _propTypes.PropTypes.func.isRequired,
  error: _propTypes.PropTypes.string,
  fullWidth: _propTypes.PropTypes.bool,
  id: _propTypes.PropTypes.string.isRequired,
  options: _propTypes.PropTypes.array.isRequired,
  theme: _propTypes.PropTypes.string,
  title: _propTypes.PropTypes.string,
  required: _propTypes.PropTypes.bool,
  optional: _propTypes.PropTypes.bool
};
var _default = Select;
/**
 * Select input with callbacks
 *
 * @property {func} callback - A function that will run after the input  has changed, it will return the value of the input for you to use in the parent component.
 * @property {string} error - An error message to be shown if there's an issue with the selection.
 * @property {bool} fullWidth - A bool to determin if inline or block.
 * @property {string} id - An id to be added to the input itself, not the wrapping label.
 * @property {array} optoins - An array of object shown when the select is open { title: '', value: '' }
 * @property {string} theme - Classes to be added to the wrapping label.
 * @property {string} title - The label text title of the input.
 * @property {bool} required - Will show required * if true, else nothing.
 * @property {bool} optional - Will show (optional) if true, else nothing.
 */

exports["default"] = _default;