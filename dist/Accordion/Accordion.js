"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = require("prop-types");

var _AccordionItem = _interopRequireDefault(require("./AccordionItem"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(n); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var Accordion = function Accordion(_ref) {
  var delimiter = _ref.delimiter,
      delimiterClose = _ref.delimiterClose,
      id = _ref.id,
      multiOpen = _ref.multiOpen,
      options = _ref.options;

  var _useState = (0, _react.useState)(options),
      _useState2 = _slicedToArray(_useState, 2),
      o = _useState2[0],
      oSetter = _useState2[1];

  var togglePanels = function togglePanels(i) {
    var optionsMute = options;
    if (multiOpen !== true) optionsMute.map(function (a) {
      return a.isOpen = false;
    }); // set them all closed

    optionsMute[i].isOpen = !optionsMute[i].isOpen; // toggle the clicked one

    oSetter(_toConsumableArray(optionsMute));
  }; // todo change isToggle so it leave them open on click or collapses them and opens the one u clicked;


  return /*#__PURE__*/_react["default"].createElement("section", {
    "data-test": "component-accordion",
    id: id,
    className: "nwc--accordion"
  }, o.map(function (item, i) {
    return /*#__PURE__*/_react["default"].createElement(_AccordionItem["default"], {
      key: item.title,
      delimiter: delimiter,
      delimiterClose: delimiterClose,
      item: item,
      togglePanels: togglePanels,
      i: i
    });
  }));
};

Accordion.defaultProps = {
  delimiter: '+',
  delimiterClose: '-',
  id: null,
  multiOpen: true
};
Accordion.propTypes = {
  delimiter: _propTypes.PropTypes.string,
  delimiterClose: _propTypes.PropTypes.string,
  id: _propTypes.PropTypes.string,
  multiOpen: _propTypes.PropTypes.bool,
  options: _propTypes.PropTypes.array.isRequired
};
var _default = Accordion;
/**
* Accordion component that can be single open or multip open based off the mutliOpe prop.
* If false, only 1 panel will be open at a time.
* If true, panels will stay open until their heading is clicked again.
*
* @property {string} delimiter - The indicator on the right of the accordion when closed
* @property {string} delimiterClosed - The indicator on the right of the accordion when opened
* @property {string} id - Id to be applied to the wrapper
* @property {bool} multiOpen - If true, then panels will stay open until you click the heading. If false, all panels will close except the one you click, leaving only 1 open at a time
* @property {array} options - The options for each accordion panel { title: 'test', content: 'hello', isOpen: true }
*/

exports["default"] = _default;