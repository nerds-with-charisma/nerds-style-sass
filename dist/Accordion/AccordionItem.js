"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = require("prop-types");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var AccordionItem = function AccordionItem(_ref) {
  var delimiter = _ref.delimiter,
      delimiterClose = _ref.delimiterClose,
      i = _ref.i,
      item = _ref.item,
      togglePanels = _ref.togglePanels;
  return /*#__PURE__*/_react["default"].createElement("div", {
    "data-test": "component-accordion-item",
    className: "nwc--accordion-item"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "nwc--accordion-title padding--sm cursor--pointer",
    onClick: function onClick() {
      return togglePanels(i);
    }
  }, /*#__PURE__*/_react["default"].createElement("span", {
    "data-test": "component-accordion-delimiter",
    className: "nwc--accordion-delimiter float--right font--20"
  }, item.isOpen === true ? delimiterClose : delimiter), item.title), item.isOpen === true && /*#__PURE__*/_react["default"].createElement("div", {
    className: "nwc--accordion-content font--16 padding--sm"
  }, item.content));
};

AccordionItem.defaultProps = {
  delimiter: '+',
  delimiterClose: '-'
};
AccordionItem.propTypes = {
  delimiter: _propTypes.PropTypes.string,
  delimiterClose: _propTypes.PropTypes.string,
  i: _propTypes.PropTypes.number.isRequired,
  item: _propTypes.PropTypes.object.isRequired,
  togglePanels: _propTypes.PropTypes.func.isRequired
};
var _default = AccordionItem;
/**
 * The accordion item that's mapped inside the parent accodrion component
 *
 * @property
 */

exports["default"] = _default;