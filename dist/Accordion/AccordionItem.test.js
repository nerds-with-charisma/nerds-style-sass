"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var _react = _interopRequireDefault(require("react"));

var _enzyme = _interopRequireWildcard(require("enzyme"));

var _enzymeAdapterReact = _interopRequireDefault(require("enzyme-adapter-react-16"));

var _AccordionItem = _interopRequireDefault(require("./AccordionItem"));

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

_enzyme["default"].configure({
  adapter: new _enzymeAdapterReact["default"]()
});

var setup = function setup() {
  var props = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var state = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
  return (0, _enzyme.shallow)( /*#__PURE__*/_react["default"].createElement(_AccordionItem["default"], {
    delimiter: props.delimiter,
    delimiterClose: props.delimiterClose,
    item: props.item,
    togglePanels: props.togglePanels,
    i: props.i
  }));
};

var testProps = {
  delimiter: '>',
  delimiterClose: '-',
  item: {
    title: 'test',
    content: 'hello',
    isOpen: false
  },
  togglePanels: function togglePanels() {
    return console.log();
  },
  i: 0
}; // find by test attr

var findByTestAttr = function findByTestAttr(wrapper, val) {
  return wrapper.find("[data-test=\"".concat(val, "\"]"));
}; // Renders without exploding


test('AccordionItem renders without crashing', function () {
  var wrapper = setup(testProps);
  var appComponent = findByTestAttr(wrapper, 'component-accordion-item');
  expect(appComponent.length).toBe(1);
}); // delimiter

test('AccordionItem delimiter works if provided', function () {
  var wrapper = setup(testProps);
  var appComponent = findByTestAttr(wrapper, 'component-accordion-delimiter');
  expect(appComponent.length).toBe(1);
  expect(appComponent.text()).toBe('>');
});
test('AccordionItem close delimiter shows default +', function () {
  var wrapper = setup({
    item: {
      title: 'test',
      content: 'hello',
      isOpen: false
    },
    togglePanels: function togglePanels() {
      return console.log();
    },
    i: 0
  });
  var appComponent = findByTestAttr(wrapper, 'component-accordion-delimiter');
  expect(appComponent.length).toBe(1);
  expect(appComponent.text()).toBe('+');
}); // delimiterClose

test('AccordionItem close delimiter shows', function () {
  var wrapper = setup({
    delimiter: '>',
    delimiterClose: '-',
    item: {
      title: 'test',
      content: 'hello',
      isOpen: true
    },
    togglePanels: function togglePanels() {
      return console.log();
    },
    i: 0
  });
  var appComponent = findByTestAttr(wrapper, 'component-accordion-delimiter');
  expect(appComponent.length).toBe(1);
  expect(appComponent.text()).toBe('-');
});