"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = require("prop-types");

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(n); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var ScrollToTop = function ScrollToTop(_ref) {
  var delimiter = _ref.delimiter,
      delimiterTheme = _ref.delimiterTheme,
      size = _ref.size,
      target = _ref.target,
      theme = _ref.theme,
      threshold = _ref.threshold;

  var _useState = (0, _react.useState)(threshold ? false : true),
      _useState2 = _slicedToArray(_useState, 2),
      show = _useState2[0],
      showSetter = _useState2[1];

  (0, _react.useEffect)(function () {
    if (window) window.addEventListener('scroll', handleScroll);
  }, []);

  var handleScroll = function handleScroll() {
    if (threshold && window.scrollY > threshold) {
      showSetter(true);
    } else {
      showSetter(false);
    }
  };

  var scrollToTop = function scrollToTop() {
    if (!target) {
      window.scrollTo({
        top: 0,
        behavior: 'smooth'
      });
    } else {
      var elmnt = document.getElementById(target);
      elmnt.scrollIntoView({
        behavior: 'smooth'
      });
    }
  };

  return /*#__PURE__*/_react["default"].createElement("span", {
    "data-test": "component-scroll-to-top",
    id: "nwc--back-to-top",
    style: sttStyle
  }, show === true && /*#__PURE__*/_react["default"].createElement("button", {
    type: "button",
    className: theme,
    onClick: function onClick() {
      return scrollToTop();
    },
    style: {
      height: size,
      width: size,
      lineHeight: "".concat(size, "px"),
      padding: 0
    }
  }, /*#__PURE__*/_react["default"].createElement("span", {
    "data-test": "component-scroll-to-top-delimiter",
    className: delimiterTheme
  }, delimiter)));
};

var sttStyle = {
  position: 'fixed',
  bottom: 25,
  right: 25
};
ScrollToTop.defaultProps = {
  delimiter: /*#__PURE__*/_react["default"].createElement("span", null, "\u25B2"),
  delimiterTheme: 'font--dark',
  size: 60,
  target: null,
  theme: 'radius--lg shadow--sm',
  threshold: 100
};
ScrollToTop.propTypes = {
  delimiter: _propTypes.PropTypes.object,
  delimiterTheme: _propTypes.PropTypes.string,
  size: _propTypes.PropTypes.number,
  target: _propTypes.PropTypes.string,
  theme: _propTypes.PropTypes.string,
  threshold: _propTypes.PropTypes.number
};
var _default = ScrollToTop;
/**
 * @property {object} delimiter - The icon or words that will show on the button, must be wrapped in valid JSX like a span
 * @property {string} delimiterTheme - Classes to be applied to the delimiter itself (wrapped in a span)
 * @property {number} size - The height and width of the button
 * @property {number} target - An ID of where to scroll to, if excluded, will be the top of the page
 * @property {string} theme - Classes applied to the button wrapper
 * @property {number} threshold -  How far the user has to scroll before the button shows, if excluded, it will always show
 */

exports["default"] = _default;