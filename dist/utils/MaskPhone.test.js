"use strict";

var _MaskPhone = _interopRequireDefault(require("./MaskPhone"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

// tests
test('Mask phone returns a value', function () {
  expect((0, _MaskPhone["default"])(5555555555)).toBe('(555) 555-5555');
});