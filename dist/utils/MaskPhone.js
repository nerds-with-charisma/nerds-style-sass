"use strict";

/**
 *
 *
 * @param {*} phone
 * @returns The formatted phone number in (XXX) XXX-XXXX format
 */
var maskPhone = function maskPhone(phone) {
  try {
    if (typeof window !== "undefined" && window.event && window.event.keyCode === 8) return false;
  } catch (e) {
    if (typeof window !== "undefined") console.error(e);
  }

  var x = phone.toString().replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,4})/);
  phone = !x[2] ? x[1] : '(' + x[1] + ') ' + x[2] + (x[3] ? '-' + x[3] : '');
  return phone;
};

module.exports = maskPhone;