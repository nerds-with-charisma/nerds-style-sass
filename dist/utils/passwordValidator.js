"use strict";

/**
 * @param {string} password - string value to test
 * @param {number} length - how long the password should be, defaults to 6
 * @param {boolean} shouldHaveNumber - should the password require a number, default true
 * @param {boolean} shouldHaveCapital - should a capital letter be required, in any character, default true,
 * @param {boolean} shouldHaveNoSpecialChars - should we exclude any characters, defaults to true
 * @param {string} invalidChars - if "shouldHaveNoSpecialChars" is true, which chars should we exclude
 */
var passwordValidator = function passwordValidator(pw) {
  var length = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 6;
  var shouldHaveNumber = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;
  var shouldHaveCapital = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : true;
  var shouldHaveNoSpecialChars = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : true;
  var invalidChars = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : '?!';
  if (!pw) return false; // test length

  if (pw.length >= length) {
    for (var loop = 0; loop < pw.length; loop += 1) {
      // cannot have spaces
      if (pw.charCodeAt(loop) === 32) {
        // does not, throw an error
        return {
          error: true,
          message: "Spaces are not allowed"
        };
      }
    }
  } else {
    // does not, throw an error
    return {
      error: true,
      message: "Your password must be at least ".concat(length, " characters long")
    };
  } // test if it should have a number


  if (shouldHaveNumber === true && !/\d/.test(pw)) {
    // does not, throw an error
    return {
      error: true,
      message: "Passwords must have a number"
    };
  } //test if there are  any special characters


  if (shouldHaveNoSpecialChars === true) {
    for (var _loop = 0; _loop < pw.length; _loop += 1) {
      if (invalidChars.indexOf(pw.charAt(_loop)) !== -1) {
        // has invalid char, throw error
        return {
          error: true,
          message: "Password cannot contain ".concat(invalidChars)
        };
      }
    }
  } // test if it has a capital letter


  if (shouldHaveCapital === true && !/[A-Z]/.test(pw)) {
    // does not, throw an error
    return {
      error: true,
      message: "Password must have a capital letter"
    };
  }

  return {
    error: false,
    message: "Password is valid"
  };
};

module.exports = passwordValidator;