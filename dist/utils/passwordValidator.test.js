"use strict";

var _passwordValidator = require("./passwordValidator");

test('Returns true for valid password with default settings', function () {
  expect((0, _passwordValidator.passwordValidator)('Testing123').error).toBe(false);
});
test('Returns false for invalid password with default settings', function () {
  expect((0, _passwordValidator.passwordValidator)('testing').error).toBe(true);
}); // test each param

test('Length validation works correctly', function () {
  // length tests
  expect((0, _passwordValidator.passwordValidator)('Hi1', 4, true, true, true, '?!').error).toBe(true);
  expect((0, _passwordValidator.passwordValidator)('Hello1', 4, true, true, '?!').error).toBe(false); // has number tests

  expect((0, _passwordValidator.passwordValidator)('Hello', 4, true).error).toBe(true);
  expect((0, _passwordValidator.passwordValidator)('Hello1', 4, true).error).toBe(false); // capitol number tests

  expect((0, _passwordValidator.passwordValidator)('hello', 4, true).error).toBe(true);
  expect((0, _passwordValidator.passwordValidator)('Hello1', 4, true).error).toBe(false); // has special chars

  expect((0, _passwordValidator.passwordValidator)('H@llo1', 4, true, true, true, '@').error).toBe(true);
  expect((0, _passwordValidator.passwordValidator)('Hello1', 4, true, true, '@').error).toBe(false);
});