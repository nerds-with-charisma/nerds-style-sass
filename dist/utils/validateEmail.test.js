"use strict";

var _validateEmail = _interopRequireDefault(require("./validateEmail"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

test('Returns true for valid email', function () {
  expect((0, _validateEmail["default"])('briandausman@gmail.com').error).toBe(false);
});
test('Returns false for invalid email', function () {
  expect((0, _validateEmail["default"])('briandausmangmail.com').error).toBe(true);
  expect((0, _validateEmail["default"])('briandausmangmail.com').message).toBe('The e-mail address you have entered is not valid. Email should be of the form abc@de.com');
});