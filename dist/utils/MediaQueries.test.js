"use strict";

var _MediaQueries = _interopRequireDefault(require("./MediaQueries"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

test('Mq is returned properly', function () {
  expect((0, _MediaQueries["default"])()).toBe('lg');
});
test('Mq returns right xs', function () {
  Object.defineProperty(window, 'innerWidth', {
    writable: true,
    configurable: true,
    value: 105
  });
  expect((0, _MediaQueries["default"])()).toBe('xs');
});
test('Mq returns right sm', function () {
  Object.defineProperty(window, 'innerWidth', {
    writable: true,
    configurable: true,
    value: 600
  });
  expect((0, _MediaQueries["default"])()).toBe('sm');
});
test('Mq returns right md', function () {
  Object.defineProperty(window, 'innerWidth', {
    writable: true,
    configurable: true,
    value: 800
  });
  expect((0, _MediaQueries["default"])()).toBe('md');
});