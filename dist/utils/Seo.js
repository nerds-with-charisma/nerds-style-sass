"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = require("prop-types");

var _reactHelmet = _interopRequireDefault(require("react-helmet"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var Seo = function Seo(_ref) {
  var children = _ref.children,
      lang = _ref.lang,
      meta = _ref.meta,
      openGraph = _ref.openGraph,
      schema = _ref.schema,
      title = _ref.title;
  return /*#__PURE__*/_react["default"].createElement(_reactHelmet["default"], {
    lang: lang
  }, /*#__PURE__*/_react["default"].createElement("title", null, title), meta && meta.length > 0 && meta.map(function (item) {
    return /*#__PURE__*/_react["default"].createElement("meta", {
      key: item.name,
      name: item.name,
      content: item.content
    });
  }), schema && schema.length > 0 && schema.map(function (item) {
    return /*#__PURE__*/_react["default"].createElement("script", {
      type: "application/ld+json",
      key: item
    }, JSON.stringify(item).replace(/_/g, '@'));
  }), openGraph && openGraph.length > 0 && Object.keys(openGraph).map(function (key) {
    return /*#__PURE__*/_react["default"].createElement("meta", {
      property: "og:".concat(key),
      content: openGraph[key],
      key: openGraph[key]
    });
  }), children);
};

Seo.defaultProps = {
  children: null,
  gaId: null,
  gtmId: null,
  lang: 'en',
  meta: [],
  og: {},
  schema: []
};
Seo.propTypes = {
  children: _propTypes.PropTypes.object,
  gaId: _propTypes.PropTypes.string,
  gtmId: _propTypes.PropTypes.string,
  lang: _propTypes.PropTypes.string.isRequired,
  meta: _propTypes.PropTypes.array,
  openGraph: _propTypes.PropTypes.object,
  schema: _propTypes.PropTypes.array,
  title: _propTypes.PropTypes.string.isRequired
};
var _default = Seo;
exports["default"] = _default;