"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = require("prop-types");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var ButtonGroup = function ButtonGroup(_ref) {
  var callback = _ref.callback,
      optional = _ref.optional,
      options = _ref.options,
      required = _ref.required,
      theme = _ref.theme,
      title = _ref.title;
  return /*#__PURE__*/_react["default"].createElement(_react["default"].Fragment, null, /*#__PURE__*/_react["default"].createElement("strong", {
    "data-test": "component-buttonGroup",
    className: "nwc--button-group-title font--14"
  }, /*#__PURE__*/_react["default"].createElement("span", {
    className: "nwc--button-group-title-text"
  }, title), required === true && /*#__PURE__*/_react["default"].createElement("sup", {
    "data-test": "component-buttonGroup-required",
    className: "font--error font--10"
  }, ' *'), optional === true && /*#__PURE__*/_react["default"].createElement("sup", {
    "data-test": "component-buttonGroup-optional",
    className: "font--grey font--10"
  }, ' (optional)')), /*#__PURE__*/_react["default"].createElement("div", {
    className: "nwc--button-group ".concat(theme)
  }, options.map(function (option, index) {
    return /*#__PURE__*/_react["default"].createElement("button", {
      key: option.title,
      type: "button",
      className: option.active ? 'active' : null,
      onClick: function onClick() {
        return callback(index);
      }
    }, option.title);
  })));
};

ButtonGroup.defaultProps = {
  optional: false,
  required: false,
  theme: ''
};
ButtonGroup.propTypes = {
  callback: _propTypes.PropTypes.func.isRequired,
  optional: _propTypes.PropTypes.bool,
  options: _propTypes.PropTypes.array.isRequired,
  required: _propTypes.PropTypes.bool,
  theme: _propTypes.PropTypes.string,
  title: _propTypes.PropTypes.string.isRequired
};
var _default = ButtonGroup;
/**
 * Buttons grouped together, almost like a select in button form!
 *
 *
 * @property {func} callback - A function that will run after the input  has changed, it will return the value of the input for you to use in the parent component.
 * @property {bool} optional - If true, will show an (optional) tag next to the title. If false or not supplied, nothing will show.
 * @property {array} options - An array of items to display on each button => [title, value, active]
 * @property {bool} required - If true, will show an * next to the title. If false or not supplied, nothing will show.
 * @property {string} theme - Classes to be added to the wrapping label.
 * @property {string} title - The label text title of the input.
 */

exports["default"] = _default;