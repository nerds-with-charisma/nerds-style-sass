"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var _react = _interopRequireDefault(require("react"));

var _enzyme = _interopRequireWildcard(require("enzyme"));

var _enzymeAdapterReact = _interopRequireDefault(require("enzyme-adapter-react-16"));

var _ButtonGroup = _interopRequireDefault(require("./ButtonGroup"));

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

_enzyme["default"].configure({
  adapter: new _enzymeAdapterReact["default"]()
}); // get a copy of the dom for our component


var setup = function setup() {
  var props = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var state = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
  return (0, _enzyme.shallow)( /*#__PURE__*/_react["default"].createElement(_ButtonGroup["default"], {
    callback: props.callback,
    optional: props.optional,
    options: props.options,
    required: props.required,
    theme: props.theme,
    title: props.title
  }));
};

var testProps = {
  callback: function callback(v) {
    return v;
  },
  options: [{
    title: 'Option 1',
    value: 'option-1',
    active: true
  }, {
    title: 'Option 2',
    value: 'option-2',
    active: false
  }],
  theme: 'test-class',
  title: 'Test',
  required: true,
  optional: true
}; // find by test attr

var findByTestAttr = function findByTestAttr(wrapper, val) {
  return wrapper.find("[data-test=\"".concat(val, "\"]"));
}; // make sure the component renders


test('ButtonGroup renders without crashing', function () {
  var wrapper = setup(testProps);
  var appComponent = findByTestAttr(wrapper, 'component-buttonGroup');
  expect(appComponent.length).toBe(1);
}); // callback is returned correctly

describe('Callback value is returned', function () {
  test('function is called on click', function () {
    var options = [{
      title: 'Option 1',
      value: 'option-1',
      active: true
    }, {
      title: 'Option 2',
      value: 'option-2',
      active: false
    }];
    var callback = jest.fn(function (v) {
      return v;
    });
    var wrapper = (0, _enzyme.shallow)( /*#__PURE__*/_react["default"].createElement(_ButtonGroup["default"], {
      callback: callback,
      title: "test",
      options: options
    }));
    var button = wrapper.find('button').last();
    button.simulate('click');
    var callbackItem = options[callback.mock.results[0].value];
    expect(callback).toHaveBeenCalled();
    expect(callbackItem.title).toBe('Option 2');
  });
}); // options render properly

test('ButtonGroup options render correctly', function () {
  var wrapper = setup(testProps);
  var appComponent = wrapper.find('button');
  expect(appComponent.first().text()).toBe('Option 1');
  expect(appComponent.last().text()).toBe('Option 2');
}); // theme class is applied

test('ButtonGroup theme is applied', function () {
  var wrapper = setup(testProps);
  var appComponent = wrapper.find('.nwc--button-group.test-class');
  expect(appComponent.length).toBe(1);
}); // title renders properly

test('ButtongGroup title renders', function () {
  var wrapper = setup(testProps);
  var appComponent = wrapper.find('.nwc--button-group-title-text').text();
  expect(appComponent).toBe('Test');
}); // expect required to show

test('Required * shows up', function () {
  var wrapper = setup(testProps);
  var appComponent = findByTestAttr(wrapper, 'component-buttonGroup-required').text();
  expect(appComponent).toBe(' *');
}); // expect optional to show

test('Optional shows up', function () {
  var wrapper = setup(testProps);
  var appComponent = findByTestAttr(wrapper, 'component-buttonGroup-optional').text();
  expect(appComponent).toBe(' (optional)');
});