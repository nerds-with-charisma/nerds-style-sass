"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = require("prop-types");

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var Drawer = function Drawer(_ref) {
  var backgroundColor = _ref.backgroundColor,
      children = _ref.children,
      closeCb = _ref.closeCb,
      drawerOpen = _ref.drawerOpen,
      theme = _ref.theme,
      onRight = _ref.onRight,
      preventScroll = _ref.preventScroll,
      showClose = _ref.showClose,
      swipeToClose = _ref.swipeToClose;
  // change docked position
  var positionStyle = {};
  if (onRight === true) positionStyle = {
    left: 'inherit',
    right: 0
  }; // stop the body from being scrollable

  setTimeout(function () {
    if (window.document && preventScroll === true && drawerOpen === true) {
      window.document.getElementsByTagName('html')[0].className += ' nwc--noscroll';
    } else {
      window.document.getElementsByTagName('html')[0].className -= ' nwc--noscroll';
    }
  }, 3000); // swipe detection :: https://stackoverflow.com/questions/2264072/detect-a-finger-swipe-through-javascript-on-the-iphone-and-android

  if (swipeToClose === true && drawerOpen) {
    document.addEventListener('touchstart', handleTouchStart, false);
    document.addEventListener('touchmove', handleTouchMove, false);
  }

  var xDown = null;
  var yDown = null;

  function getTouches(e) {
    return e.touches;
  }

  function handleTouchStart(e) {
    var firstTouch = getTouches(e)[0];
    xDown = firstTouch.clientX;
    yDown = firstTouch.clientY;
    console.log('handleTouchStart');
  }

  function handleTouchMove(evt) {
    if (!xDown || !yDown || drawerOpen !== true) return;
    var xUp = evt.touches[0].clientX;
    var yUp = evt.touches[0].clientY;
    var xDiff = xDown - xUp;
    var yDiff = yDown - yUp;

    if (Math.abs(xDiff) > Math.abs(yDiff)) {
      if (xDiff > 0) {
        // left swipe
        // console.log('left');
        if (onRight !== true) closeCb();
      } else {
        // right swipe
        // console.log('right');
        if (onRight === true) closeCb();
      }
    } else {
      if (yDiff > 0) {
        console.log('up'); // up swipe
      } else {
        console.log('down'); // down swipe
      }
    } // reset values


    xDown = null;
    yDown = null;
  }

  return /*#__PURE__*/_react["default"].createElement("nav", {
    "data-test": "component-drawer",
    className: drawerOpen === true ? "nwc--drawer ".concat(theme) : "nwc--drawer closed ".concat(theme),
    style: _objectSpread({}, positionStyle)
  }, /*#__PURE__*/_react["default"].createElement("section", {
    "data-test": "component-drawer-wrapper",
    style: {
      backgroundColor: backgroundColor
    }
  }, showClose === true && /*#__PURE__*/_react["default"].createElement("div", {
    "data-test": "component-drawer-close",
    role: "button",
    className: "nwc--drawer-close padding--sm text--right font--28 border--bottom",
    onClick: function onClick() {
      return closeCb();
    }
  }, "\xD7\xA0\xA0"), children), /*#__PURE__*/_react["default"].createElement("aside", {
    className: "nwc--drawer-mask",
    onClick: function onClick() {
      return closeCb();
    }
  }));
};

Drawer.defaultProps = {
  backgroundColor: '',
  closeCb: function closeCb() {
    return null;
  },
  drawerOpen: false,
  preventScroll: false,
  onRight: false,
  theme: '',
  showClose: false,
  swipeToClose: false
};
Drawer.propTypes = {
  backgroundColor: _propTypes.PropTypes.string,
  children: _propTypes.PropTypes.object.isRequired,
  closeCb: _propTypes.PropTypes.func,
  drawerOpen: _propTypes.PropTypes.bool,
  onRight: _propTypes.PropTypes.bool,
  preventScroll: _propTypes.PropTypes.bool,
  theme: _propTypes.PropTypes.string,
  showClose: _propTypes.PropTypes.bool,
  swipeToClose: _propTypes.PropTypes.bool
};
var _default = Drawer;
/**
 * A docked drawer navigation that can take anything you throw at it...html wise
 *
 *
 * @property {string} backgroundColor - The hex color of the drawer panel.
 * @property {object} children - JSX element inside of the drwaer.
 * @property {func} closeCb - Function to be called after close event has happened.
 * @property {bool} drawerOpen - Trigger to show the menu or not.
 * @property {bool} onRight - If true, the drawer will be on the right side, else the left
 * @property {bool} preventScroll - Stop the background from scrolling when drawer is open.
 * @property {string} theme - Classes to be added to the wrapping label.
 * @property {bool} showClose - Show the close button or not (you can make your own).
 * @property {bool} swipeToClose - Allow swiping the set drawerOpen to false.
 */

exports["default"] = _default;