"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var _react = _interopRequireWildcard(require("react"));

var _enzyme = _interopRequireWildcard(require("enzyme"));

var _enzymeAdapterReact = _interopRequireDefault(require("enzyme-adapter-react-16"));

var _Drawer = _interopRequireDefault(require("./Drawer"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

_enzyme["default"].configure({
  adapter: new _enzymeAdapterReact["default"]()
}); // get a copy of the dom for our component


var setup = function setup() {
  var props = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var state = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
  return (0, _enzyme.shallow)( /*#__PURE__*/_react["default"].createElement(_Drawer["default"], {
    backgroundColor: props.backgroundColor,
    closeCb: props.closeCb,
    drawerOpen: props.drawerOpen,
    onRight: props.onRight,
    preventScroll: props.preventScroll,
    showClose: props.showClose,
    swipeToClose: props.swipeToClose,
    theme: props.theme
  }, props.children));
};

var testProps = {
  backgroundColor: '#fff',
  children: /*#__PURE__*/_react["default"].createElement("h1", null, "Test"),
  closeCb: function closeCb(v) {
    return console.log(v);
  },
  drawerOpen: true,
  onRight: true,
  preventScroll: true,
  showClose: true,
  swipeToClose: true,
  theme: 'test-class'
}; // find by test attr

var findByTestAttr = function findByTestAttr(wrapper, val) {
  return wrapper.find("[data-test=\"".concat(val, "\"]"));
}; // renders


test('Drawer renders without crashing', function () {
  var wrapper = setup(testProps);
  var appComponent = findByTestAttr(wrapper, 'component-drawer');
  expect(appComponent.length).toBe(1);
}); // backgroundColor

test('Drawer renders without crashing', function () {
  var wrapper = setup(testProps);
  var appComponent = findByTestAttr(wrapper, 'component-drawer-wrapper');
  expect(JSON.stringify(appComponent.prop('style'))).toBe(JSON.stringify({
    backgroundColor: '#fff'
  }));
}); // drawerOpen

test('Drawer closes properly', function () {
  var wrapper = setup({
    backgroundColor: '#fff',
    children: /*#__PURE__*/_react["default"].createElement("h1", null, "Test"),
    closeCb: function closeCb(v) {
      return console.log(v);
    },
    drawerOpen: false,
    onRight: false,
    preventScroll: true,
    showClose: true,
    swipeToClose: true,
    theme: 'test-class'
  });
  var appComponent = findByTestAttr(wrapper, 'component-drawer');
  expect(appComponent.find('.nwc--drawer.closed').length).toBe(1);
});
test('Drawer opens properly', function () {
  var wrapper = setup(testProps);
  var appComponent = findByTestAttr(wrapper, 'component-drawer');
  expect(appComponent.find('.nwc--drawer').length).toBe(1);
}); // onRight

test('Drawer docks on right properly', function () {
  var wrapper = setup(testProps);
  var appComponent = findByTestAttr(wrapper, 'component-drawer');
  expect(JSON.stringify(appComponent.prop('style'))).toBe(JSON.stringify({
    left: 'inherit',
    right: 0
  }));
}); // showClose

test('Drawer close button renders', function () {
  var wrapper = setup(testProps);
  var appComponent = findByTestAttr(wrapper, 'component-drawer-close');
  expect(appComponent.length).toBe(1);
}); // theme

test('Drawer theme styles are applied', function () {
  var wrapper = setup(testProps);
  var appComponent = wrapper.find('.nwc--drawer.test-class');
  expect(appComponent.length).toBe(1);
});