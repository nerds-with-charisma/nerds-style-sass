"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = require("prop-types");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var DrawerItem = function DrawerItem(_ref) {
  var backgroundColor = _ref.backgroundColor,
      children = _ref.children,
      textColor = _ref.textColor;
  return /*#__PURE__*/_react["default"].createElement("span", {
    "data-test": "component-drawerItem",
    className: "nwc--drawer-item font--16",
    style: {
      background: backgroundColor,
      color: textColor
    }
  }, children);
};

DrawerItem.propTypes = {
  backgroundColor: _propTypes.PropTypes.string,
  children: _propTypes.PropTypes.object.isRequired,
  textColor: _propTypes.PropTypes.string
};
var _default = DrawerItem;
/**
 * A standard drawer item link
 *
 *
 * @property {string} backgroundColor - The hex color of the background of the specific item.
 * @property {object} children - A valid JSX set of elements.
 * @property {string} textColor - The hex color of the text of this specific item.
 *
 */

exports["default"] = _default;