import React, { useEffect, useState } from 'react';
import { PropTypes } from 'prop-types';

const ScrollToTop = ({
  delimiter,
  delimiterTheme,
  size,
  target,
  theme,
  threshold,
}) => {
  const [show, showSetter] = useState(threshold ? false : true);

  useEffect(() => {
    if (window) window.addEventListener('scroll', handleScroll);
  }, []);

  const handleScroll = () => {
    if (threshold && window.scrollY > threshold) {
      showSetter(true);
    } else {
      showSetter(false);
    }
  };

  const scrollToTop = () => {
    if (!target) {
      window.scrollTo({ top: 0, behavior: 'smooth' });
    } else {
      var elmnt = document.getElementById(target);
      elmnt.scrollIntoView({ behavior: 'smooth' });
    }
  };

  return (
    <span
      data-test="component-scroll-to-top"
      id="nwc--back-to-top"
      style={sttStyle}
    >
      {show === true && (
        <button
          type="button"
          className={theme}
          onClick={() => scrollToTop()}
          style={{
            height: size,
            width: size,
            lineHeight: `${size}px`,
            padding: 0,
          }}
        >
          <span
            data-test="component-scroll-to-top-delimiter"
            className={delimiterTheme}
          >
            {delimiter}
          </span>
        </button>
      )}
    </span>
  );
};

const sttStyle = {
  position: 'fixed',
  bottom: 25,
  right: 25,
};

ScrollToTop.defaultProps = {
  delimiter: <span>&#9650;</span>,
  delimiterTheme: 'font--dark',
  size: 60,
  target: null,
  theme: 'radius--lg shadow--sm',
  threshold: 100,
};

ScrollToTop.propTypes = {
  delimiter: PropTypes.object,
  delimiterTheme: PropTypes.string,
  size: PropTypes.number,
  target: PropTypes.string,
  theme: PropTypes.string,
  threshold: PropTypes.number,
};

export default ScrollToTop;

/**
 * @property {object} delimiter - The icon or words that will show on the button, must be wrapped in valid JSX like a span
 * @property {string} delimiterTheme - Classes to be applied to the delimiter itself (wrapped in a span)
 * @property {number} size - The height and width of the button
 * @property {number} target - An ID of where to scroll to, if excluded, will be the top of the page
 * @property {string} theme - Classes applied to the button wrapper
 * @property {number} threshold -  How far the user has to scroll before the button shows, if excluded, it will always show
 */
