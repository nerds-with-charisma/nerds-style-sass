import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';

import ScrollToTop from './ScrollToTop';

Enzyme.configure({ adapter: new EnzymeAdapter() });

const setup = (props = {}, state = null) => {
  return shallow(
    <ScrollToTop
      delimiter={props.delimiter}
      delimiterTheme={props.delimiterTheme}
      size={props.size}
      target={props.target}
      theme={props.theme}
      threshold={props.threshold}
    />,
  );
};

const testProps = {
  delimiter: <span>'Up'</span>,
  delimiterTheme: 'test-class',
  size: 50,
  target: 'myId',
  theme: 'test-theme',
  threshold: null,
};

// find by test attr
const findByTestAttr = (wrapper, val) => {
  return wrapper.find(`[data-test="${val}"]`);
};

const wrapper = setup(testProps);
const mainComponent = findByTestAttr(wrapper, 'component-scroll-to-top');
const delimiterComponent = findByTestAttr(
  wrapper,
  'component-scroll-to-top-delimiter',
);

// Test
test('Scroll Top renders without crashing', () => {
  expect(mainComponent.length).toBe(1);
});

// delimiter
test('STT delimiter renders right', () => {
  expect(delimiterComponent.length).toBe(1);
  expect(delimiterComponent.find('span span').text()).toBe("'Up'");
});

//   delimiterTheme
test('STT theme is applied', () => {
  expect(mainComponent.find('.test-class').length).toBe(1);
});

//   size
test('STT size to be applied to button', () => {
  expect(JSON.stringify(mainComponent.find('button').prop('style'))).toBe(
    '{"height":50,"width":50,"lineHeight":"50px","padding":0}',
  );
});

//   theme
test('STT button theme is applied', () => {
  expect(mainComponent.find('.test-theme').length).toBe(1);
});
