import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';

import Modal from './Modal';

Enzyme.configure({ adapter: new EnzymeAdapter() });

const setup = (props = {}, state = null) => {
  return shallow(
    <Modal
      closeCb={() => props.closeCb}
      maskColor={props.maskColor}
      show={props.show}
      theme={props.theme}
      themeBody={props.themeBody}
      themeClose={props.themeClose}
      title={props.title}
      width={props.width}
    >
      <div className="font--14">{props.children}</div>
    </Modal>,
  );
};

const testProps = {
  children: <h1>{'The plans you refer to will soon be back in our hands.'}</h1>,
  maskColor: 'rgba(50,20,100,0.7)',
  show: true,
  theme: 'test-theme',
  themeBody: 'test-theme-body',
  themeClose: 'test-theme-close',
  title: 'Imma Modal',
  width: 50,
};

const defaultProps = {
  children: <h1>{'The plans you refer to will soon be back in our hands.'}</h1>,
  show: false,
  theme: 'test-theme',
  themeBody: 'test-theme-body',
  themeClose: 'test-theme-close',
  title: 'Imma Modal',
  width: 50,
};

// find by test attr
const findByTestAttr = (wrapper, val) => {
  return wrapper.find(`[data-test="${val}"]`);
};

const wrapper = setup(testProps);
const appComponent = findByTestAttr(wrapper, 'component-modal');

const defaultWrapper = setup(defaultProps);
const defaultComponent = findByTestAttr(defaultWrapper, 'component-modal');

// Test
test('Modal renders without crashing', () => {
  expect(appComponent.length).toBe(1);
});

// children
test('Modal children render properly', () => {
  expect(appComponent.find('h1').length).toBe(1);
});

// maskColor
test('Modal mask color applied, and default kicks in if not', () => {
  expect(
    appComponent.find('#nwc--modal-mask').prop('style').backgroundColor,
  ).toBe('rgba(50,20,100,0.7)');

  // change show to true to test
  // expect(
  //   defaultComponent.find('#nwc--modal-mask').prop('style').backgroundColor,
  // ).toBe('rgba(0,0,0,0.7)');
});

// show
test("Modal shows when it should and does not when it shouldn't", () => {
  expect(appComponent.length).toBe(1);
  expect(defaultComponent.length).toBe(0);
});

// theme
test('Modal theme is applied', () => {
  expect(appComponent.find('.test-theme').length).toBe(1);
});

// themeBody
test('Modal body theme is applied', () => {
  expect(appComponent.find('.test-theme-body').length).toBe(1);
});

// themeClose
test('Modal close theme is applied', () => {
  expect(appComponent.find('.test-theme-close').length).toBe(1);
});

// title
test('Modal title populates', () => {
  const title = findByTestAttr(wrapper, 'component-modal-title');
  expect(title.length).toBe(1);
  expect(title.text()).toBe('Imma Modal');
});
