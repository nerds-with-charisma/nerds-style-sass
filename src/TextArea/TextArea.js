import React from 'react';
import { PropTypes } from 'prop-types';

const TextArea = ({
  autofill,
  blurCallback,
  callback,
  error,
  id,
  maxLength,
  minLength,
  optional,
  placeHolder,
  populatedValue,
  required,
  rows,
  theme,
  title,
  type,
}) => (
  <label
    data-test="component-textArea"
    className={`nwc--input ${theme}`}
    htmlFor={id}
  >
    <div className="nwc--label-wrap">
      <strong className="font--14">
        <span data-test="component-textArea-title">{title}</span>
        {required === true && (
          <sup
            data-test="component-textArea-required"
            className="font--error font--10"
          >
            {' *'}
          </sup>
        )}
        {optional === true && (
          <sup
            data-test="component-textArea-optional"
            className="font--grey font--10"
          >
            {' (optional)'}
          </sup>
        )}
      </strong>
    </div>
    <textarea
      data-test="component-textArea-input"
      autofill={autofill}
      className="padding--md font--16 border--grey"
      defaultValue={populatedValue}
      id={id}
      maxLength={maxLength}
      minLength={minLength}
      name={id}
      onChange={(e) => callback(e.target.value)}
      onBlur={(e) => blurCallback(e.target.value)}
      placeholder={placeHolder}
      rows={rows}
      type={type}
    />
    {error !== null && (
      <aside
        data-test="component-textArea-error"
        className="nwc--validation nwc--validation--error font--12"
      >
        {error}
      </aside>
    )}
  </label>
);

TextArea.defaultProps = {
  autofill: 'yes',
  blurCallback: () => true,
  theme: '',
  error: null,
  maxLength: 5000,
  minLength: 1,
  optional: false,
  placeholder: null,
  populatedValue: null,
  required: false,
  rows: 1,
  type: 'text',
};

TextArea.propTypes = {
  autofill: PropTypes.string,
  blurCallback: PropTypes.func,
  callback: PropTypes.func.isRequired,
  error: PropTypes.string,
  id: PropTypes.string.isRequired,
  maxLength: PropTypes.number,
  minLength: PropTypes.number,
  optional: PropTypes.bool,
  placeHolder: PropTypes.string,
  populatedValue: PropTypes.string,
  required: PropTypes.bool,
  rows: PropTypes.string,
  theme: PropTypes.string,
  title: PropTypes.string.isRequired,
  type: PropTypes.string,
};

export default TextArea;

/**
 * Super customizable inputs that you can style to ur hearts desire.
 * Callbacks for blur and change let you control logic in your parent component easily.
 *
 * @property {string} autofill - If the input should allow autofilling from the browser. This is not always handled properly in Chrome, so we suggest to use a string like 'nope'.
 * @property {func} blurCallback - The function to run when the input loses focus/blurred.
 * @property {func} callback - A function that will run after the input  has changed, it will return the value of the input for you to use in the parent component.
 * @property {bool} disabled - Should the input be disabled or not. Use with a state var to determine when the input can be edited.
 * @property {string} error - An error message to be shown docked at the bottom of the input. If null, nothing will show.
 * @property {string} id - An id to be added to the input itself, not the wrapping label.
 * @property {number} maxLength - The allowed max length of characters into the input.
 * @property {number} minLength - The allowed min lenght of characters into the input.
 * @property {bool} optional - If true, will show an (optional) tag next to the title. If false or not supplied, nothing will show.
 * @property {string} placeHolder - Placeholder value to show on the input before any value is entered.
 * @property {string} populatedValue - The default value to be populated into the input on load.
 * @property {bool} required - If true, will show a required *'s. If false or not supplied, nothing will show.
 * @property {string} rows - How many rows should show for the text area.
 * @property {string} theme - Classes to be added to the wrapping label.
 * @property {string} title - The label text title of the input.
 * @property {string} type - The type attribute to determine what sort of input is used (text, email, etc).
 */
