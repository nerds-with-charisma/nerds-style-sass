import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';

import TextArea from './TextArea';

Enzyme.configure({ adapter: new EnzymeAdapter() });

const setup = (props = {}, state = null) => {
  return shallow(
    <TextArea
      autofill={props.autofill}
      blurCallback={props.blurCallback}
      callback={props.callback}
      error={props.error}
      id={props.id}
      maxLength={props.maxLength}
      minLength={props.minLength}
      optional={props.optional}
      placeHolder={props.placeHolder}
      populatedValue={props.populatedValue}
      required={props.required}
      rows={props.rows}
      theme={props.theme}
      title={props.title}
    />,
  );
};

const testProps = {
  autofill: 'nope',
  blurCallback: (v) => console.log(v),
  callback: (v) => console.log(v),
  error: 'Error Test',
  id: 'input--test',
  maxLength: 40,
  minLength: 40,
  optional: true,
  placeHolder: 'Placeholder Test',
  populatedValue: 'Test Value',
  required: true,
  rows: '10',
  theme: 'test',
  title: 'Test',
};

// find by test attr
const findByTestAttr = (wrapper, val) => {
  return wrapper.find(`[data-test="${val}"]`);
};

// Test
test('TextArea renders without crashing', () => {
  const wrapper = setup(testProps);
  const appComponent = findByTestAttr(wrapper, 'component-textArea');
  expect(appComponent.length).toBe(1);
});

// autofill
test('Input autofill is working', () => {
  const wrapper = setup(testProps);
  const appComponent = findByTestAttr(wrapper, 'component-textArea-input');
  expect(appComponent.prop('autofill')).toBe('nope');
});

// blurCallback
describe('TextArea blur callback value is returned', () => {
  test('function is called on  blur', () => {
    const callback = jest.fn((v) => v);

    const wrapper = shallow(
      <TextArea
        id="test"
        blurCallback={callback}
        callback={() => true}
        rows="1"
        title="test"
      />,
    );

    const textArea = findByTestAttr(wrapper, 'component-textArea-input');
    const mockEvent = { target: { value: 'blur value' } };

    textArea.simulate('blur', mockEvent);

    const callbackItem = callback.mock.results[0].value;
    expect(callback).toHaveBeenCalled();
    expect(callbackItem).toBe('blur value');
  });
});

// callback
describe('TextArea callback value is returned', () => {
  test('function is called on click', () => {
    const callback = jest.fn((v) => v);

    const wrapper = shallow(
      <TextArea id="test" callback={callback} rows="1" title="test" />,
    );

    const textArea = findByTestAttr(wrapper, 'component-textArea-input');
    const mockEvent = { target: { value: 'test value' } };

    textArea.simulate('change', mockEvent);

    const callbackItem = callback.mock.results[0].value;
    expect(callback).toHaveBeenCalled();
    expect(callbackItem).toBe('test value');
  });
});

// error
test('TextArea error works', () => {
  const wrapper = setup(testProps);
  const appComponent = findByTestAttr(wrapper, 'component-textArea-error');
  expect(appComponent.text()).toBe('Error Test');
});

// id
test('TextArea id is set', () => {
  const wrapper = setup(testProps);
  const appComponent = findByTestAttr(wrapper, 'component-textArea-input');
  expect(appComponent.prop('id')).toBe('input--test');
});

// maxLength
test('TextArea max length is set', () => {
  const wrapper = setup(testProps);
  const appComponent = findByTestAttr(wrapper, 'component-textArea-input');
  expect(appComponent.prop('maxLength')).toBe(40);
});

// minLength
test('TextArea min length is set', () => {
  const wrapper = setup(testProps);
  const appComponent = findByTestAttr(wrapper, 'component-textArea-input');
  expect(appComponent.prop('minLength')).toBe(40);
});

// optional
test('TextArea optional text shows', () => {
  const wrapper = setup(testProps);
  const appComponent = findByTestAttr(wrapper, 'component-textArea-optional');
  expect(appComponent.length).toBe(1);
});

// placeHolder
test('TextArea placeholder gets populated', () => {
  const wrapper = setup(testProps);
  const appComponent = findByTestAttr(wrapper, 'component-textArea-input');
  expect(appComponent.prop('placeholder')).toBe('Placeholder Test');
});

// populatedValue
test('TextArea initial popluated value works', () => {
  const wrapper = setup(testProps);
  const appComponent = findByTestAttr(wrapper, 'component-textArea-input');
  const defaultValue = JSON.stringify(appComponent.debug());
  expect(defaultValue.includes('Test')).toBe(true);
});

// required
test('TextArea required text shows', () => {
  const wrapper = setup(testProps);
  const appComponent = findByTestAttr(wrapper, 'component-textArea-required');
  expect(appComponent.length).toBe(1);
});

// theme
test('TextArea theme styles render', () => {
  const wrapper = setup(testProps);
  const appComponent = findByTestAttr(wrapper, 'component-textArea');
  expect(appComponent.find('.nwc--input.test').length).toBe(1);
});

// title
test('TextArea title renders', () => {
  const wrapper = setup(testProps);
  const appComponent = findByTestAttr(wrapper, 'component-textArea-title');
  expect(appComponent.text()).toBe('Test');
});
