import React from 'react';
import PropTypes from 'prop-types';

const StateDropdown = ({
  callback,
  error,
  fullWidth,
  id,
  optional,
  placeholder,
  populatedValue,
  required,
  showFullName,
  theme,
  title,
}) => {
  const states = [
    {
      name: 'Armed Forces Americas',
      abbreviation: 'AA',
    },
    {
      name: 'Armed Forces Europe, the Middle East, and Canada',
      abbreviation: 'AE',
    },
    {
      name: 'Alabama',
      abbreviation: 'AL',
    },
    {
      name: 'Alaska',
      abbreviation: 'AK',
    },
    {
      name: 'American Samoa',
      abbreviation: 'AS',
    },
    {
      name: 'Arizona',
      abbreviation: 'AZ',
    },
    {
      name: 'Arkansas',
      abbreviation: 'AR',
    },
    {
      name: 'California',
      abbreviation: 'CA',
    },
    {
      name: 'Colorado',
      abbreviation: 'CO',
    },
    {
      name: 'Connecticut',
      abbreviation: 'CT',
    },
    {
      name: 'Delaware',
      abbreviation: 'DE',
    },
    {
      name: 'District Of Columbia',
      abbreviation: 'DC',
    },
    {
      name: 'Federated States Of Micronesia',
      abbreviation: 'FM',
    },
    {
      name: 'Florida',
      abbreviation: 'FL',
    },
    {
      name: 'Georgia',
      abbreviation: 'GA',
    },
    {
      name: 'Guam',
      abbreviation: 'GU',
    },
    {
      name: 'Hawaii',
      abbreviation: 'HI',
    },
    {
      name: 'Idaho',
      abbreviation: 'ID',
    },
    {
      name: 'Illinois',
      abbreviation: 'IL',
    },
    {
      name: 'Indiana',
      abbreviation: 'IN',
    },
    {
      name: 'Iowa',
      abbreviation: 'IA',
    },
    {
      name: 'Kansas',
      abbreviation: 'KS',
    },
    {
      name: 'Kentucky',
      abbreviation: 'KY',
    },
    {
      name: 'Louisiana',
      abbreviation: 'LA',
    },
    {
      name: 'Maine',
      abbreviation: 'ME',
    },
    {
      name: 'Marshall Islands',
      abbreviation: 'MH',
    },
    {
      name: 'Maryland',
      abbreviation: 'MD',
    },
    {
      name: 'Massachusetts',
      abbreviation: 'MA',
    },
    {
      name: 'Michigan',
      abbreviation: 'MI',
    },
    {
      name: 'Minnesota',
      abbreviation: 'MN',
    },
    {
      name: 'Mississippi',
      abbreviation: 'MS',
    },
    {
      name: 'Missouri',
      abbreviation: 'MO',
    },
    {
      name: 'Montana',
      abbreviation: 'MT',
    },
    {
      name: 'Nebraska',
      abbreviation: 'NE',
    },
    {
      name: 'Nevada',
      abbreviation: 'NV',
    },
    {
      name: 'New Hampshire',
      abbreviation: 'NH',
    },
    {
      name: 'New Jersey',
      abbreviation: 'NJ',
    },
    {
      name: 'New Mexico',
      abbreviation: 'NM',
    },
    {
      name: 'New York',
      abbreviation: 'NY',
    },
    {
      name: 'North Carolina',
      abbreviation: 'NC',
    },
    {
      name: 'North Dakota',
      abbreviation: 'ND',
    },
    {
      name: 'Northern Mariana Islands',
      abbreviation: 'MP',
    },
    {
      name: 'Ohio',
      abbreviation: 'OH',
    },
    {
      name: 'Oklahoma',
      abbreviation: 'OK',
    },
    {
      name: 'Oregon',
      abbreviation: 'OR',
    },
    {
      name: 'Palau',
      abbreviation: 'PW',
    },
    {
      name: 'Pennsylvania',
      abbreviation: 'PA',
    },
    {
      name: 'Puerto Rico',
      abbreviation: 'PR',
    },
    {
      name: 'Rhode Island',
      abbreviation: 'RI',
    },
    {
      name: 'South Carolina',
      abbreviation: 'SC',
    },
    {
      name: 'South Dakota',
      abbreviation: 'SD',
    },
    {
      name: 'Tennessee',
      abbreviation: 'TN',
    },
    {
      name: 'Texas',
      abbreviation: 'TX',
    },
    {
      name: 'Utah',
      abbreviation: 'UT',
    },
    {
      name: 'Vermont',
      abbreviation: 'VT',
    },
    {
      name: 'Virgin Islands',
      abbreviation: 'VI',
    },
    {
      name: 'Virginia',
      abbreviation: 'VA',
    },
    {
      name: 'Washington',
      abbreviation: 'WA',
    },
    {
      name: 'West Virginia',
      abbreviation: 'WV',
    },
    {
      name: 'Wisconsin',
      abbreviation: 'WI',
    },
    {
      name: 'Wyoming',
      abbreviation: 'WY',
    },
  ];

  return (
    <label
      data-test="component-Statedropdown"
      className={`nwc--select ${theme}`}
      htmlFor={id}
    >
      <div className="nwc--label-wrap">
        <strong className="font--14">
          <span data-test="component-StateDropdown-title">{title}</span>
          {required === true && (
            <sup
              data-test="component-StateDropdown-required"
              className="font--error font--10"
            >
              {' *'}
            </sup>
          )}
          {optional === true && (
            <sup
              data-test="component-StateDropdown-optional"
              className="font--grey font--10"
            >
              {' (optional)'}
            </sup>
          )}
        </strong>
      </div>
      <br />
      <select
        data-test="component-StateDropdown-input"
        id={id}
        className={fullWidth === true ? 'full-width' : ''}
        onChange={(e) => callback(e.target.value)}
        value={populatedValue}
      >
        <option value={null}>&nbsp;{placeholder}</option>

        {states.map((state) => (
          <option key={state.abbreviation} value={state.abbreviation}>
            &nbsp;{showFullName === true ? state.name : state.abbreviation}
          </option>
        ))}
      </select>
      {error !== null && (
        <aside
          data-test="component-StateDropdown-error"
          className="nwc--validation nwc--validation--error font--12"
        >
          {error}
        </aside>
      )}
    </label>
  );
};

StateDropdown.defaultProps = {
  error: null,
  fullWidth: true,
  populatedValue: '',
  id: 'state',
  optinal: false,
  placeholder: '-- Make a selection --',
  required: false,
  showFullName: false,
  theme: '',
  title: 'State',
};

StateDropdown.propTypes = {
  callback: PropTypes.func.isRequired,
  error: PropTypes.string,
  fullWidth: PropTypes.bool,
  id: PropTypes.string,
  optional: PropTypes.bool,
  populatedValue: PropTypes.string,
  placeholder: PropTypes.string,
  required: PropTypes.bool,
  showFullName: PropTypes.bool,
  theme: PropTypes.string,
  title: PropTypes.string,
};

export default StateDropdown;

/**
 * Select input with callbacks
 *
 * @property {func} callback - A function that will run after the input  has changed, it will return the value of the input for you to use in the parent component.
 * @property {string} error - An error message to be shown if there's an issue with the selection.
 * @property {bool} fullWidth - A bool to determin if inline or block.
 * @property {string} id - An id to be added to the input itself, not the wrapping label.
 * @property {bool} optional - Will show (optional) if true, else nothing.
 * @property {string} populatedValue - Value that is selected on the input
 * @property {bool} required - Will show required * if true, else nothing.
 * @property {bool} showFullName - Should we use the abbreviation or the full state name
 * @property {string} theme - Classes to be added to the wrapping label.
 * @property {string} title - The label text title of the input.


 */
