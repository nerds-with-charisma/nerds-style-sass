import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';

import StateDropdown from './StateDropdown';

Enzyme.configure({ adapter: new EnzymeAdapter() });

// get a copy of the dom for our component
const setup = (props = {}, state = null) => {
  return shallow(
    <StateDropdown
      callback={props.callback}
      error={props.error}
      fullWidth={props.fullWidth}
      id={props.id}
      optional={props.optional}
      placeholder={props.placeholder}
      populatedValue={props.populatedValue}
      required={props.required}
      showFullName={props.showFullName}
      theme={props.theme}
      title={props.title}
    />,
  );
};

const testProps = {
  callback: (v) => console.log(v),
  error: 'Test error',
  fullWidth: true,
  id: 'testId',
  optional: true,
  placeholder: 'Make a selection',
  populatedValue: 'IL',
  required: true,
  showFullName: true,
  theme: 'test-class',
  title: 'Test',
};

// find by test attr
const findByTestAttr = (wrapper, val) => {
  return wrapper.find(`[data-test="${val}"]`);
};

// make sure the component renders
test('StateDropdown renders without crashing', () => {
  const wrapper = setup(testProps);
  const appComponent = findByTestAttr(wrapper, 'component-Statedropdown');
  expect(appComponent.length).toBe(1);
});

// callback
describe('StateDropdown callback value is returned', () => {
  test('function is called on change', () => {
    const callback = jest.fn((v) => v);
    const wrapper = shallow(
      <StateDropdown id="test" callback={callback} title="test" />,
    );

    const input = findByTestAttr(wrapper, 'component-StateDropdown-input');
    const mockEvent = { target: { value: 'IL' } };
    input.simulate('change', mockEvent);
    const callbackItem = callback.mock.results[0].value;

    expect(callback).toHaveBeenCalled();
    expect(callbackItem).toBe('IL');
  });
});

// error
test('StateDropdown error shows', () => {
  const wrapper = setup(testProps);
  const appComponent = findByTestAttr(wrapper, 'component-StateDropdown-error');
  expect(appComponent.length).toBe(1);
  expect(appComponent.text()).toBe(testProps.error);
});

// fullWidth
test('StateDropdown full width style shows', () => {
  const wrapper = setup(testProps);
  const appComponent = findByTestAttr(wrapper, 'component-StateDropdown-input');
  expect(appComponent.find('.full-width').length).toBe(1);
});

// id,
test('StateDropdown full width style shows', () => {
  const wrapper = setup(testProps);
  const appComponent = findByTestAttr(wrapper, 'component-StateDropdown-input');
  expect(appComponent.find('#testId').length).toBe(1);
});

// optional
test('StateDropdown optional renders', () => {
  const wrapper = setup({
    callback: (v) => console.log(v),
    error: 'Test error',
    fullWidth: true,
    id: 'testId',
    options: [],
    theme: 'test-class',
    title: 'Test',
    required: false,
    optional: true,
  });
  const appComponent = findByTestAttr(
    wrapper,
    'component-StateDropdown-optional',
  );
  expect(appComponent.length).toBe(1);
});

// placeholder
test('StateDropdown placeholder is populated if no default value supplied', () => {
  const wrapper = setup({
    callback: (v) => console.log(v),
    placeholder: testProps.placeholder,
  });
  const appComponent = findByTestAttr(wrapper, 'component-StateDropdown-input');
  expect(
    appComponent
      .find('option')
      .first()
      .text()
      .trim(),
  ).toBe(testProps.placeholder);
});

// populated value
test('StateDropdown prepopulated value is set', () => {
  const wrapper = setup(testProps);
  const appComponent = findByTestAttr(wrapper, 'component-StateDropdown-input');
  expect(appComponent.prop('value')).toBe(testProps.populatedValue);
});

// required
test('StateDropdown required renders', () => {
  const wrapper = setup(testProps);
  const appComponent = findByTestAttr(
    wrapper,
    'component-StateDropdown-required',
  );
  expect(appComponent.length).toBe(1);
});

// Full state name shows
test('StateDropdown full state name is shown', () => {
  const wrapper = setup(testProps);
  const appComponent = findByTestAttr(wrapper, 'component-StateDropdown-input');
  expect(
    appComponent
      .find('option')
      .last()
      .text()
      .trim(),
  ).toBe('Wyoming');
});

// theme: 'test-class',
test('StateDropdown theme classes are applied', () => {
  const wrapper = setup(testProps);
  expect(wrapper.find('.nwc--select.test-class').length).toBe(1);
});

//   title
test('StateDropdown title renders', () => {
  const wrapper = setup(testProps);
  const appComponent = findByTestAttr(wrapper, 'component-StateDropdown-title');
  expect(appComponent.text()).toBe(testProps.title);
});
