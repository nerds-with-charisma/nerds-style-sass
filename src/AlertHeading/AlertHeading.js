import React from 'react';
import { PropTypes } from 'prop-types';

const AlertHeading = ({ children, theme }) => (
  <div
    data-test="component-alertHeading"
    className={`nwc--alert-heading ${theme}`}
  >
    {children}
  </div>
);

AlertHeading.defaultProps = {
  theme: '',
};

AlertHeading.propTypes = {
  children: PropTypes.object.isRequired,
  theme: PropTypes.string,
};

export default AlertHeading;

/**
 * An alert div that can be info, success, or error...or not, u decide. Pass "theme" and style it the way you want.
 *
 * @property {object} children - Pass any valid jsx object in-between.
 * @property {string} theme -[info, error, success], also any custom classes you want to pass
 * *
 */
