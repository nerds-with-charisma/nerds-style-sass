import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';

import AlertHeading from '../AlertHeading/AlertHeading';

Enzyme.configure({ adapter: new EnzymeAdapter() });

// get a copy of the dom for our component
const setup = (props = {}, state = null) => {
  return shallow(<AlertHeading {...props}>{props.children}</AlertHeading>);
};

// find by test attr
const findByTestAttr = (wrapper, val) => {
  return wrapper.find(`[data-test="${val}"]`);
};

// make sure the test attribute renders
test('Alert renders without crashing', () => {
  const wrapper = setup({ children: <h1>{'Test'}</h1> }, {});
  // console.log(wrapper.debug());   // log out the dom element we actually got

  const appComponent = findByTestAttr(wrapper, 'component-alertHeading');
  expect(appComponent.length).toBe(1);
});

// contains the right classes
test('Alert contains the right theme classes', () => {
  const wrapper = setup(
    { children: <h1>{'Test'}</h1>, theme: 'test-class' },
    {},
  );
  const appComponent = wrapper.find('.nwc--alert-heading.test-class');
  expect(appComponent.length).toBe(1);
});

// contains the proper title
test('Alert rendered the child properly', () => {
  const wrapper = setup({ children: <h1>{'Test'}</h1> }, {});
  const appComponent = wrapper.find('h1').text();
  expect(appComponent).toBe('Test');
});
