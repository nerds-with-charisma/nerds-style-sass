import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';

import DrawerItem from './DrawerItem';

Enzyme.configure({ adapter: new EnzymeAdapter() });

const setup = (props = {}, state = null) => {
  return shallow(
    <DrawerItem
      backgroundColor={props.backgroundColor}
      textColor={props.textColor}
    >
      {props.children}
    </DrawerItem>,
  );
};

const testProps = {
  backgroundColor: '#ff0000',
  children: (
    <a href="/stiles" className="font--dark block">
      <strong>{'Some Link'}</strong>
    </a>
  ),
  textColor: '#000',
};

// find by test attr
const findByTestAttr = (wrapper, val) => {
  return wrapper.find(`[data-test="${val}"]`);
};

// make sure the component renders
test('DrawerItem renders without crashing', () => {
  const wrapper = setup(testProps);
  const appComponent = findByTestAttr(wrapper, 'component-drawerItem');
  expect(appComponent.length).toBe(1);
});

// make sure background color renders
test('DrawerItem theme styles got applied', () => {
  const wrapper = setup(testProps);
  const appComponent = findByTestAttr(wrapper, 'component-drawerItem');

  expect(JSON.stringify(appComponent.prop('style'))).toBe(
    JSON.stringify({
      background: '#ff0000',
      color: '#000',
    }),
  );
});
