import React from 'react';
import { PropTypes } from 'prop-types';

const DrawerItem = ({ backgroundColor, children, textColor }) => {
  return (
    <span
      data-test="component-drawerItem"
      className="nwc--drawer-item font--16"
      style={{
        background: backgroundColor,
        color: textColor,
      }}
    >
      {children}
    </span>
  );
};

DrawerItem.propTypes = {
  backgroundColor: PropTypes.string,
  children: PropTypes.object.isRequired,
  textColor: PropTypes.string,
};

export default DrawerItem;

/**
 * A standard drawer item link
 *
 *
 * @property {string} backgroundColor - The hex color of the background of the specific item.
 * @property {object} children - A valid JSX set of elements.
 * @property {string} textColor - The hex color of the text of this specific item.
 *
 */
