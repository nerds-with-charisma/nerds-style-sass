import React, { PureComponent } from 'react';
import Enzyme, { shallow } from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';

import Drawer from './Drawer';

Enzyme.configure({ adapter: new EnzymeAdapter() });

// get a copy of the dom for our component
const setup = (props = {}, state = null) => {
  return shallow(
    <Drawer
      backgroundColor={props.backgroundColor}
      closeCb={props.closeCb}
      drawerOpen={props.drawerOpen}
      onRight={props.onRight}
      preventScroll={props.preventScroll}
      showClose={props.showClose}
      swipeToClose={props.swipeToClose}
      theme={props.theme}
    >
      {props.children}
    </Drawer>,
  );
};

const testProps = {
  backgroundColor: '#fff',
  children: <h1>Test</h1>,
  closeCb: (v) => console.log(v),
  drawerOpen: true,
  onRight: true,
  preventScroll: true,
  showClose: true,
  swipeToClose: true,
  theme: 'test-class',
};

// find by test attr
const findByTestAttr = (wrapper, val) => {
  return wrapper.find(`[data-test="${val}"]`);
};

// renders
test('Drawer renders without crashing', () => {
  const wrapper = setup(testProps);
  const appComponent = findByTestAttr(wrapper, 'component-drawer');
  expect(appComponent.length).toBe(1);
});

// backgroundColor
test('Drawer renders without crashing', () => {
  const wrapper = setup(testProps);
  const appComponent = findByTestAttr(wrapper, 'component-drawer-wrapper');
  expect(JSON.stringify(appComponent.prop('style'))).toBe(
    JSON.stringify({ backgroundColor: '#fff' }),
  );
});

// drawerOpen
test('Drawer closes properly', () => {
  const wrapper = setup({
    backgroundColor: '#fff',
    children: <h1>Test</h1>,
    closeCb: (v) => console.log(v),
    drawerOpen: false,
    onRight: false,
    preventScroll: true,
    showClose: true,
    swipeToClose: true,
    theme: 'test-class',
  });
  const appComponent = findByTestAttr(wrapper, 'component-drawer');
  expect(appComponent.find('.nwc--drawer.closed').length).toBe(1);
});

test('Drawer opens properly', () => {
  const wrapper = setup(testProps);
  const appComponent = findByTestAttr(wrapper, 'component-drawer');
  expect(appComponent.find('.nwc--drawer').length).toBe(1);
});

// onRight
test('Drawer docks on right properly', () => {
  const wrapper = setup(testProps);
  const appComponent = findByTestAttr(wrapper, 'component-drawer');
  expect(JSON.stringify(appComponent.prop('style'))).toBe(
    JSON.stringify({ left: 'inherit', right: 0 }),
  );
});

// showClose
test('Drawer close button renders', () => {
  const wrapper = setup(testProps);
  const appComponent = findByTestAttr(wrapper, 'component-drawer-close');
  expect(appComponent.length).toBe(1);
});

// theme
test('Drawer theme styles are applied', () => {
  const wrapper = setup(testProps);
  const appComponent = wrapper.find('.nwc--drawer.test-class');
  expect(appComponent.length).toBe(1);
});
