import React, { useEffect } from 'react';
import { PropTypes } from 'prop-types';

const Drawer = ({
  backgroundColor,
  children,
  closeCb,
  drawerOpen,
  theme,
  onRight,
  preventScroll,
  showClose,
  swipeToClose,
}) => {
  // change docked position
  let positionStyle = {};
  if (onRight === true) positionStyle = { left: 'inherit', right: 0 };

  // stop the body from being scrollable
  setTimeout(() => {
    if (window.document && preventScroll === true && drawerOpen === true) {
      window.document.getElementsByTagName('html')[0].className +=
        ' nwc--noscroll';
    } else {
      window.document.getElementsByTagName('html')[0].className -=
        ' nwc--noscroll';
    }
  }, 3000);

  // swipe detection :: https://stackoverflow.com/questions/2264072/detect-a-finger-swipe-through-javascript-on-the-iphone-and-android
  if (swipeToClose === true && drawerOpen) {
    document.addEventListener('touchstart', handleTouchStart, false);
    document.addEventListener('touchmove', handleTouchMove, false);
  }

  let xDown = null;
  let yDown = null;

  function getTouches(e) {
    return e.touches;
  }

  function handleTouchStart(e) {
    const firstTouch = getTouches(e)[0];
    xDown = firstTouch.clientX;
    yDown = firstTouch.clientY;
    console.log('handleTouchStart');
  }

  function handleTouchMove(evt) {
    if (!xDown || !yDown || drawerOpen !== true) return;

    let xUp = evt.touches[0].clientX;
    let yUp = evt.touches[0].clientY;

    var xDiff = xDown - xUp;
    var yDiff = yDown - yUp;

    if (Math.abs(xDiff) > Math.abs(yDiff)) {
      if (xDiff > 0) {
        // left swipe
        // console.log('left');
        if (onRight !== true) closeCb();
      } else {
        // right swipe
        // console.log('right');
        if (onRight === true) closeCb();
      }
    } else {
      if (yDiff > 0) {
        console.log('up'); // up swipe
      } else {
        console.log('down'); // down swipe
      }
    }

    // reset values
    xDown = null;
    yDown = null;
  }

  return (
    <nav
      data-test="component-drawer"
      className={
        drawerOpen === true
          ? `nwc--drawer ${theme}`
          : `nwc--drawer closed ${theme}`
      }
      style={{
        ...positionStyle,
      }}
    >
      <section
        data-test="component-drawer-wrapper"
        style={{
          backgroundColor: backgroundColor,
        }}
      >
        {showClose === true && (
          <div
            data-test="component-drawer-close"
            role="button"
            className="nwc--drawer-close padding--sm text--right font--28 border--bottom"
            onClick={() => closeCb()}
          >
            &times;&nbsp;&nbsp;
          </div>
        )}
        {children}
      </section>
      <aside className="nwc--drawer-mask" onClick={() => closeCb()} />
    </nav>
  );
};

Drawer.defaultProps = {
  backgroundColor: '',
  closeCb: () => null,
  drawerOpen: false,
  preventScroll: false,
  onRight: false,
  theme: '',
  showClose: false,
  swipeToClose: false,
};

Drawer.propTypes = {
  backgroundColor: PropTypes.string,
  children: PropTypes.object.isRequired,
  closeCb: PropTypes.func,
  drawerOpen: PropTypes.bool,
  onRight: PropTypes.bool,
  preventScroll: PropTypes.bool,
  theme: PropTypes.string,
  showClose: PropTypes.bool,
  swipeToClose: PropTypes.bool,
};

export default Drawer;

/**
 * A docked drawer navigation that can take anything you throw at it...html wise
 *
 *
 * @property {string} backgroundColor - The hex color of the drawer panel.
 * @property {object} children - JSX element inside of the drwaer.
 * @property {func} closeCb - Function to be called after close event has happened.
 * @property {bool} drawerOpen - Trigger to show the menu or not.
 * @property {bool} onRight - If true, the drawer will be on the right side, else the left
 * @property {bool} preventScroll - Stop the background from scrolling when drawer is open.
 * @property {string} theme - Classes to be added to the wrapping label.
 * @property {bool} showClose - Show the close button or not (you can make your own).
 * @property {bool} swipeToClose - Allow swiping the set drawerOpen to false.
 */
