import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';

import FabItem from './FabItem';

Enzyme.configure({ adapter: new EnzymeAdapter() });

const setup = (props = {}, state = null) => {
  return shallow(
    <FabItem
      backgroundColor={props.backgroundColor}
      caption={props.caption}
      isImage={props.isImage}
      textColor={props.textColor}
    >
      {props.children}
    </FabItem>,
  );
};

const testProps = {
  backgroundColor: '#fff',
  caption: 'test caption',
  children: <h1>{'Test'}</h1>,
  isImage: true,
  textColor: '#000',
};

const findByTestAttr = (wrapper, val) => {
  return wrapper.find(`[data-test="${val}"]`);
};

// Tests
test('FabItem renders without crashing', () => {
  const wrapper = setup(testProps);
  const appComponent = findByTestAttr(wrapper, 'component-fabItem');
  expect(appComponent.length).toBe(1);
});

// backgroundColor & text color
test('FabItem color styles gets set', () => {
  const wrapper = setup(testProps);
  const appComponent = findByTestAttr(wrapper, 'component-fabItem');

  expect(JSON.stringify(appComponent.prop('style'))).toBe(
    JSON.stringify({ background: '#fff', color: '#000' }),
  );
});

// caption
test('FabItem caption gets rendered', () => {
  const wrapper = setup(testProps);
  const appComponent = findByTestAttr(wrapper, 'component-fabItem-caption');
  expect(appComponent.length).toBe(1);
  expect(appComponent.text()).toBe('test caption');
});

// children
test('FabItem children render properly', () => {
  const wrapper = setup(testProps);
  const appComponent = findByTestAttr(wrapper, 'component-fabItem');
  expect(appComponent.find('h1').text()).toBe('Test');
});

// isImage,
test('FabItem image renders properly', () => {
  const wrapper = setup(testProps);
  const appComponent = findByTestAttr(wrapper, 'component-fabItem');
  expect(appComponent.find('.nwc--fab-image').length).toBe(1);
});
