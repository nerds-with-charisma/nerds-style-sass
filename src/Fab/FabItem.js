import React from 'react';
import { PropTypes } from 'prop-types';

const FabItem = ({
  backgroundColor,
  caption,
  children,
  isImage,
  textColor,
}) => {
  return (
    <span
      data-test="component-fabItem"
      className={isImage !== true ? 'nwc--fab-item' : 'nwc--fab-image'}
      style={{
        background: backgroundColor,
        color: textColor,
      }}
    >
      {children}
      {caption !== null && (
        <span data-test="component-fabItem-caption">{caption}</span>
      )}
      {isImage === true && <br />}
    </span>
  );
};

FabItem.defaultProps = {
  caption: null,
  isImage: false,
};

FabItem.propTypes = {
  backgroundColor: PropTypes.string,
  caption: PropTypes.string,
  children: PropTypes.object.isRequired,
  isImage: PropTypes.bool,
  textColor: PropTypes.string,
};

export default FabItem;

/**
 * An item inside the fab, highly customizable
 *
 *
 * @property {string} backgroundColor - The hex color of the item's background.
 * @property {string} caption  - The caption title to be shown for the item.
 * @property {object} children - Valid JSX elmement.
 * @property {bool} isImage - If it's just an image, no text.
 * @property {string} textColor - The color of the text of the item.
 */
