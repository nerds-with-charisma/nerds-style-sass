import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';

import Fab from './Fab';

Enzyme.configure({ adapter: new EnzymeAdapter() });

const setup = (props = {}, state = null) => {
  return shallow(
    <Fab
      backgroundColor={props.backgroundColor}
      clickCb={props.clickCb}
      enterCb={props.enterCb}
      fabOpen={props.fabOpen}
      leaveCb={props.leaveCb}
      left={props.left}
      mini={props.mini}
      textColor={props.textColor}
      theme={props.theme}
      title={props.title}
      theme={props.theme}
    >
      {props.children}
    </Fab>,
  );
};

const testProps = {
  backgroundColor: '#fff',
  children: <h1>{'Test'}</h1>,
  clickCb: () => alert('click'),
  enterCb: () => alert('enter'),
  fabOpen: true,
  leaveCb: () => alert('leave'),
  left: true,
  mini: false,
  textColor: '#000',
  theme: 'test-class',
  title: 'Test button',
};

// find by test attr
const findByTestAttr = (wrapper, val) => {
  return wrapper.find(`[data-test="${val}"]`);
};

// make sure it renders aight
test('Fab renders without crashing', () => {
  const wrapper = setup(testProps);
  const appComponent = findByTestAttr(wrapper, 'component-fab');
  expect(appComponent.length).toBe(1);
});

// backgroundColor & text color
test('Fab styles get applied', () => {
  const wrapper = setup(testProps);
  const appComponent = findByTestAttr(wrapper, 'component-fab').find(
    '.nwc--fab',
  );

  expect(JSON.stringify(appComponent.prop('style'))).toBe(
    JSON.stringify({
      background: '#fff',
      color: '#000',
    }),
  );
});

// fabOpen
test('Fab opens properly', () => {
  const wrapper = setup(testProps);
  const appComponent = findByTestAttr(wrapper, 'component-fab-open');
  expect(appComponent.prop('className')).toBe('nwc--fab-mask');
});

// left
test('Fab docks on the left side', () => {
  const wrapper = setup(testProps);
  const appComponent = findByTestAttr(wrapper, 'component-fab-wrapper');

  expect(JSON.stringify(appComponent.prop('style'))).toBe(
    JSON.stringify({ textAlign: 'left', left: '10px', right: 'inherit' }),
  );
});

// mini
test('Fab mini styles render properly', () => {
  const wrapper = setup({
    children: <h1>{'Test'}</h1>,
    fabOpen: true,
    mini: true,
  });
  const appComponent = findByTestAttr(wrapper, 'component-fab-button');
  expect(JSON.stringify(appComponent.prop('style'))).toBe(
    JSON.stringify({
      height: 40,
      lineHeight: '40px',
      padding: '0 10px',
      minWidth: 40,
      background: '',
      color: '',
    }),
  );
});

// theme
test('Fab theme styles render properly', () => {
  const wrapper = setup(testProps);
  const appComponent = wrapper.find('.nwc--fab-wrapper.nwc--docked.test-class');

  expect(appComponent.length).toBe(1);
});

// title
test('Fab title renders', () => {
  const wrapper = setup(testProps);
  const appComponent = findByTestAttr(wrapper, 'component-fab-button').find(
    'div',
  );
  expect(appComponent.text()).toBe('Test button');
});

// children
test('Fab renders its children properly', () => {
  const wrapper = setup(testProps);
  const appComponent = findByTestAttr(wrapper, 'component-fab-wrapper');
  const childrenWrapper = appComponent.find('span');
  expect(childrenWrapper.find('h1').text()).toBe('Test');
});
