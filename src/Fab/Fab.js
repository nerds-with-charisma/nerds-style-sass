import React from 'react';
import { PropTypes } from 'prop-types';

const Fab = ({
  backgroundColor,
  children,
  clickCb,
  enterCb,
  fabOpen,
  leaveCb,
  left,
  mini,
  textColor,
  theme,
  title,
}) => {
  let dockedStyle = null; // determine where it should be docked at the bottom, if at all

  // if docked, which side
  if (left === true) {
    dockedStyle = { left: 25 };
  } else {
    dockedStyle = { right: 25 };
  }

  // if should be mini or regular size
  let miniStyle = null;
  if (mini === true)
    miniStyle = {
      height: 40,
      lineHeight: '40px',
      padding: '0 10px',
      minWidth: 40,
    };

  // if left, we need to adjust the styles
  let leftStyle = null;
  if (left === true)
    leftStyle = { textAlign: 'left', left: '10px', right: 'inherit' };

  return (
    <span
      data-test="component-fab"
      onMouseEnter={() => enterCb()}
      onMouseLeave={() => leaveCb()}
      onClick={() => clickCb()}
      className={`nwc--fab-wrapper nwc--docked ${theme}`}
      style={{
        ...dockedStyle,
      }}
    >
      <span
        data-test="component-fab-open"
        className={
          fabOpen === true ? 'nwc--fab-mask' : 'nwc--fab-mask opacity--0'
        }
      />

      <div
        data-test="component-fab-wrapper"
        className={
          fabOpen === true ? 'nwc--fab-menu' : 'nwc--fab-menu opacity--0'
        }
        style={{ ...leftStyle }}
      >
        <span className={left === true ? 'left' : 'right'}>{children}</span>
      </div>

      <button
        data-test="component-fab-button"
        type="button"
        className="nwc--fab padding--none border--none"
        style={{
          ...miniStyle,
          background: backgroundColor,
          color: textColor,
        }}
      >
        <div>{title}</div>
      </button>
    </span>
  );
};

Fab.defaultProps = {
  backgroundColor: '',
  clickCb: () => null,
  enterCb: () => null,
  leaveCb: () => null,
  left: false,
  mini: false,
  textColor: '',
  theme: '',
  title: '+',
};

Fab.propTypes = {
  backgroundColor: PropTypes.string,
  children: PropTypes.object.isRequired,
  clickCb: PropTypes.func,
  enterCb: PropTypes.func,
  fabOpen: PropTypes.bool.isRequired,
  leaveCb: PropTypes.func,
  left: PropTypes.bool,
  mini: PropTypes.bool,
  textColor: PropTypes.string,
  theme: PropTypes.string,
  title: PropTypes.string,
};

export default Fab;

/**
 * Floating Action button that takes several arguements to customize it's appearance
 *
 *
 * @property {string} backgroundColor - The hex background color of the button.
 * @property {object} children - A valid JSX set of elements.
 * @property {func} clickCb - The callback when the button is clicked.
 * @property {func} enterCb - The callback function when the mouse enters the button.
 * @property {bool} fabOpen - Toggle to show the content inside the button.
 * @property {func} leaveCb - The callback function when the mouse leaves the button.
 * @property {bool} left - Dock it on the left, otherwise will be docked on the right.
 * @property {bool} mini - Make the fab smaller than normal.
 * @property {string} textColor - Thecolor of the text of the button.
 * @property {string} theme - The classes applied to the button itself.
 * @property {string} title - The text shown next to the main button.
 */
