import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';

import Input from './Input';

Enzyme.configure({ adapter: new EnzymeAdapter() });

const setup = (props = {}, state = null) => {
  return shallow(
    <Input
      autofill={props.autofill}
      blurCallback={props.blurCallback}
      callback={props.callback}
      error={props.error}
      id={props.id}
      maxLength={props.maxLength}
      minLength={props.minLength}
      optional={props.optional}
      pattern={props.pattern}
      placeHolder={props.placeHolder}
      populatedValue={props.populatedValue}
      required={props.required}
      theme={props.theme}
      title={props.title}
      type={props.type}
    />,
  );
};

const testProps = {
  autofill: 'nope',
  blurCallback: (v) => console.log(v),
  callback: (v) => console.log(v),
  error: 'Error Test',
  id: 'input--test',
  maxLength: 40,
  minLength: 40,
  optional: true,
  pattern: '/[A-Za-z]{3}/',
  placeHolder: 'Placeholder Test',
  populatedValue: 'Test Value',
  required: true,
  theme: 'test',
  title: 'Test',
  type: 'text',
};

// find by test attr
const findByTestAttr = (wrapper, val) => {
  return wrapper.find(`[data-test="${val}"]`);
};

// Test
test('Input renders without crashing', () => {
  const wrapper = setup(testProps);
  const appComponent = findByTestAttr(wrapper, 'component-input');
  expect(appComponent.length).toBe(1);
});

// autofill
test('Input autofill is working', () => {
  const wrapper = setup(testProps);
  const appComponent = findByTestAttr(wrapper, 'component-input');
  expect(appComponent.find('input').prop('autofill')).toBe('nope');
});

// blurCallback
describe('Input blur callback value is returned', () => {
  test('function is called on  blur', () => {
    const callback = jest.fn((v) => v);

    const wrapper = shallow(
      <Input
        id="test"
        blurCallback={callback}
        callback={() => true}
        title="test"
      />,
    );

    const input = findByTestAttr(wrapper, 'component-input-input');
    const mockEvent = { target: { value: 'blur value' } };

    input.simulate('blur', mockEvent);

    const callbackItem = callback.mock.results[0].value;
    expect(callback).toHaveBeenCalled();
    expect(callbackItem).toBe('blur value');
  });
});

// callback
describe('Input callback value is returned', () => {
  test('function is called on click', () => {
    const callback = jest.fn((v) => v);

    const wrapper = shallow(
      <Input id="test" callback={callback} title="test" />,
    );

    const input = findByTestAttr(wrapper, 'component-input-input');
    const mockEvent = { target: { value: 'test value' } };

    input.simulate('change', mockEvent);

    const callbackItem = callback.mock.results[0].value;
    expect(callback).toHaveBeenCalled();
    expect(callbackItem).toBe('test value');
  });
});

// error
test('Input error works', () => {
  const wrapper = setup(testProps);
  const appComponent = findByTestAttr(wrapper, 'component-input-error');
  expect(appComponent.text()).toBe('Error Test');
});

// id
test('Input id is set', () => {
  const wrapper = setup(testProps);
  const appComponent = findByTestAttr(wrapper, 'component-input-input');
  expect(appComponent.prop('id')).toBe('input--test');
});

// maxLength
test('Input max length is set', () => {
  const wrapper = setup(testProps);
  const appComponent = findByTestAttr(wrapper, 'component-input-input');
  expect(appComponent.prop('maxLength')).toBe(40);
});

// minLength
test('Input min length is set', () => {
  const wrapper = setup(testProps);
  const appComponent = findByTestAttr(wrapper, 'component-input-input');
  expect(appComponent.prop('minLength')).toBe(40);
});

// optional
test('Input optional text shows', () => {
  const wrapper = setup(testProps);
  const appComponent = findByTestAttr(wrapper, 'component-input-optional');
  expect(appComponent.length).toBe(1);
});

// pattern
test('Input pattern renders', () => {
  const wrapper = setup(testProps);
  const appComponent = findByTestAttr(wrapper, 'component-input-input');
  expect(appComponent.prop('pattern')).toBe('/[A-Za-z]{3}/');
});
// placeHolder
test('Input placeholder gets populated', () => {
  const wrapper = setup(testProps);
  const appComponent = findByTestAttr(wrapper, 'component-input-input');
  expect(appComponent.prop('placeholder')).toBe('Placeholder Test');
});

// populatedValue
test('Input initial popluated value works', () => {
  const wrapper = setup(testProps);
  const appComponent = findByTestAttr(wrapper, 'component-input-input');
  const defaultValue = JSON.stringify(appComponent.debug());
  expect(defaultValue.includes('Test')).toBe(true);
});

// required
test('Input required text shows', () => {
  const wrapper = setup(testProps);
  const appComponent = findByTestAttr(wrapper, 'component-input-required');
  expect(appComponent.length).toBe(1);
});

// theme
test('Input theme styles render', () => {
  const wrapper = setup(testProps);
  const appComponent = findByTestAttr(wrapper, 'component-input');
  expect(appComponent.find('.nwc--input.test').length).toBe(1);
});

// title
test('Input title renders', () => {
  const wrapper = setup(testProps);
  const appComponent = findByTestAttr(wrapper, 'component-input-title');
  expect(appComponent.text()).toBe('Test');
});

// type
test('Input id is set', () => {
  const wrapper = setup(testProps);
  const appComponent = findByTestAttr(wrapper, 'component-input-input');
  expect(appComponent.prop('type')).toBe('text');
});
