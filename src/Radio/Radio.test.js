import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';

import Radio from './Radio';

Enzyme.configure({ adapter: new EnzymeAdapter() });

// get a copy of the dom for our component
const setup = (props = {}, state = null) => {
  return shallow(
    <Radio
      title={props.title}
      id={props.id}
      callback={(v, e) => console.log(v, e)}
      value={props.value}
      checked={props.checked}
      theme={props.theme}
    />,
  );
};

const testProps = {
  title: 'Check Me',
  id: 'input--someId',
  callback: (v, e) => console.log(v, e),
  value: 'some-value',
  checked: true,
  theme: 'customStyles',
};

// find by test attr
const findByTestAttr = (wrapper, val) => {
  return wrapper.find(`[data-test="${val}"]`);
};

// make sure the component renders
test('Radio renders without crashing', () => {
  const wrapper = setup(testProps);
  const appComponent = findByTestAttr(wrapper, 'component-radio');
  expect(appComponent.length).toBe(1);
});

// title renders fine
test('Radio title is added', () => {
  const wrapper = setup(testProps);
  const appComponent = findByTestAttr(wrapper, 'component-radio-title');
  expect(appComponent.text()).toBe('Check Me');
});

// id is added to main element
test('Radio Id is added to label', () => {
  const wrapper = setup(testProps);
  const appComponent = wrapper.find('#input--someId');
  expect(appComponent.length).toBe(1);
});

// callback
describe('Radio callback value is returned', () => {
  test('function is called on click', () => {
    const callback = jest.fn((v) => v);

    const wrapper = shallow(
      <Radio callback={callback} title="test" value="test value" />,
    );

    const radio = findByTestAttr(wrapper, 'component-radio-input');

    radio.simulate('click');
    const callbackItem = callback.mock.results[0].value;
    expect(callback).toHaveBeenCalled();
    expect(callbackItem).toBe('test value');
  });
});

// value passed is set
test('Radio Default value is set', () => {
  const wrapper = setup(testProps);
  const appComponent = findByTestAttr(wrapper, 'component-radio-input');
  expect(appComponent.length).toBe(1);
  expect(appComponent.props().value).toBe('some-value');
});

// checked is set
test('Radio Check if passed', () => {
  const wrapper = setup(testProps);
  const appComponent = findByTestAttr(wrapper, 'component-radio-input');
  expect(appComponent.props().defaultChecked).toBe(true);
});

// theme styles are applied
test('Radio Styles are applied', () => {
  const wrapper = setup(testProps);
  const appComponent = findByTestAttr(wrapper, 'component-radio');
  expect(appComponent.find('.customStyles').length).toBe(1);
});
