import React from 'react';
import { PropTypes } from 'prop-types';

const Radio = ({
  callback,
  checked,
  dataAttr,
  disabled,
  id,
  name,
  theme,
  title,
  value,
}) => {
  return (
    <label data-test="component-radio" className={`nwc--checkbox ${theme}`}>
      <input
        data-test="component-radio-input"
        data-attr={dataAttr}
        defaultChecked={checked}
        disabled={disabled}
        id={id}
        name={name}
        onClick={(e) => callback(value, e)}
        type="radio"
        value={value}
      />
      <strong
        data-test="component-radio-title"
        className="nwc--checkbox-title font--14"
      >
        {title}
      </strong>
    </label>
  );
};

Radio.defaultProps = {
  checked: false,
  dataAttr: null,
  disabled: false,
  id: '',
  theme: '',
};

Radio.propTypes = {
  callback: PropTypes.func.isRequired,
  checked: PropTypes.bool,
  dataAttr: PropTypes.string,
  disabled: PropTypes.bool,
  id: PropTypes.string,
  theme: PropTypes.string,
  title: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
};

export default Radio;

/**
 * Radio input with callbacks
 *
 *
 * @property {func} callback - A function that will run after the input  has changed, it will return the value of the input for you to use in the parent component.
 * @property {bool} checked - Should the radio show as checked or not.
 * @property {string} dataAttr - A custom data attribute value you can pass to hook into the input if needed.
 * @property {bool} disabled - Should the input be disabled or not. Use with a state var to determine when the input can be edited.
 * @property {string} id - An id to be added to the input itself, not the wrapping label.
 * @property {string} theme - Classes to be added to the wrapping label.
 * @property {string} title - The label text title of the input.
 * @property {string} value - The value of the radio, will be accessible as part of "e" in the callback
 */
