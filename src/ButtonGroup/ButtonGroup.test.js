import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';

import ButtonGroup from './ButtonGroup';

Enzyme.configure({ adapter: new EnzymeAdapter() });

// get a copy of the dom for our component
const setup = (props = {}, state = null) => {
  return shallow(
    <ButtonGroup
      callback={props.callback}
      optional={props.optional}
      options={props.options}
      required={props.required}
      theme={props.theme}
      title={props.title}
    />,
  );
};

const testProps = {
  callback: (v) => v,
  options: [
    {
      title: 'Option 1',
      value: 'option-1',
      active: true,
    },
    {
      title: 'Option 2',
      value: 'option-2',
      active: false,
    },
  ],
  theme: 'test-class',
  title: 'Test',
  required: true,
  optional: true,
};

// find by test attr
const findByTestAttr = (wrapper, val) => {
  return wrapper.find(`[data-test="${val}"]`);
};

// make sure the component renders
test('ButtonGroup renders without crashing', () => {
  const wrapper = setup(testProps);
  const appComponent = findByTestAttr(wrapper, 'component-buttonGroup');
  expect(appComponent.length).toBe(1);
});

// callback is returned correctly
describe('Callback value is returned', () => {
  test('function is called on click', () => {
    const options = [
      {
        title: 'Option 1',
        value: 'option-1',
        active: true,
      },
      {
        title: 'Option 2',
        value: 'option-2',
        active: false,
      },
    ];

    const callback = jest.fn((v) => v);
    const wrapper = shallow(
      <ButtonGroup callback={callback} title="test" options={options} />,
    );
    const button = wrapper.find('button').last();
    button.simulate('click');

    const callbackItem = options[callback.mock.results[0].value];
    expect(callback).toHaveBeenCalled();
    expect(callbackItem.title).toBe('Option 2');
  });
});

// options render properly
test('ButtonGroup options render correctly', () => {
  const wrapper = setup(testProps);
  const appComponent = wrapper.find('button');
  expect(appComponent.first().text()).toBe('Option 1');
  expect(appComponent.last().text()).toBe('Option 2');
});

// theme class is applied
test('ButtonGroup theme is applied', () => {
  const wrapper = setup(testProps);
  const appComponent = wrapper.find('.nwc--button-group.test-class');
  expect(appComponent.length).toBe(1);
});

// title renders properly
test('ButtongGroup title renders', () => {
  const wrapper = setup(testProps);
  const appComponent = wrapper.find('.nwc--button-group-title-text').text();
  expect(appComponent).toBe('Test');
});

// expect required to show
test('Required * shows up', () => {
  const wrapper = setup(testProps);
  const appComponent = findByTestAttr(
    wrapper,
    'component-buttonGroup-required',
  ).text();
  expect(appComponent).toBe(' *');
});

// expect optional to show
test('Optional shows up', () => {
  const wrapper = setup(testProps);
  const appComponent = findByTestAttr(
    wrapper,
    'component-buttonGroup-optional',
  ).text();
  expect(appComponent).toBe(' (optional)');
});
