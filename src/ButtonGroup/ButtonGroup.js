import React from 'react';
import { PropTypes } from 'prop-types';

const ButtonGroup = ({
  callback,
  optional,
  options,
  required,
  theme,
  title,
}) => {
  return (
    <React.Fragment>
      <strong
        data-test="component-buttonGroup"
        className="nwc--button-group-title font--14"
      >
        <span className="nwc--button-group-title-text">{title}</span>
        {required === true && (
          <sup
            data-test="component-buttonGroup-required"
            className="font--error font--10"
          >
            {' *'}
          </sup>
        )}
        {optional === true && (
          <sup
            data-test="component-buttonGroup-optional"
            className="font--grey font--10"
          >
            {' (optional)'}
          </sup>
        )}
      </strong>

      <div className={`nwc--button-group ${theme}`}>
        {options.map((option, index) => (
          <button
            key={option.title}
            type="button"
            className={option.active ? 'active' : null}
            onClick={() => callback(index)}
          >
            {option.title}
          </button>
        ))}
      </div>
    </React.Fragment>
  );
};

ButtonGroup.defaultProps = {
  optional: false,
  required: false,
  theme: '',
};

ButtonGroup.propTypes = {
  callback: PropTypes.func.isRequired,
  optional: PropTypes.bool,
  options: PropTypes.array.isRequired,
  required: PropTypes.bool,
  theme: PropTypes.string,
  title: PropTypes.string.isRequired,
};

export default ButtonGroup;

/**
 * Buttons grouped together, almost like a select in button form!
 *
 *
 * @property {func} callback - A function that will run after the input  has changed, it will return the value of the input for you to use in the parent component.
 * @property {bool} optional - If true, will show an (optional) tag next to the title. If false or not supplied, nothing will show.
 * @property {array} options - An array of items to display on each button => [title, value, active]
 * @property {bool} required - If true, will show an * next to the title. If false or not supplied, nothing will show.
 * @property {string} theme - Classes to be added to the wrapping label.
 * @property {string} title - The label text title of the input.
 */
