import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';

import CoverBackground from './CoverBackground';

Enzyme.configure({ adapter: new EnzymeAdapter() });

const setup = (props = {}, state = null) => {
  return shallow(
    <CoverBackground
      backgroundColor={props.backgroundColor}
      minHeight={props.minHeight}
      mp4={props.mp4}
      src={props.src}
      theme={props.theme}
      webm={props.webm}
    >
      {props.children}
    </CoverBackground>,
  );
};

const testProps = {
  children: (
    <div>
      <h1>Hi</h1>
    </div>
  ),
  src:
    'https://nerdswithcharisma.com/static/6d2103ee47e61e92d68e165eb9de330a/97132/bg--hero.webp',
  backgroundColor: '#ff0000',
  minHeight: 500,
  mp4: 'somevideo.mp4',
  theme: 'test-class',
  webm: 'somevideo.webm',
};

// find by test attr
const findByTestAttr = (wrapper, val) => {
  return wrapper.find(`[data-test="${val}"]`);
};

const wrapper = setup(testProps);
const mainComponent = findByTestAttr(wrapper, 'component-cover-background');
const webmComponent = findByTestAttr(
  wrapper,
  'component-cover-background-webm',
);
const mp4Component = findByTestAttr(wrapper, 'component-cover-background-mp4');

// Test
test('CoverBackground renders without crashing', () => {
  expect(mainComponent.length).toBe(1);
});

// children
test('CoverBackground children render', () => {
  expect(mainComponent.find('h1').length).toBe(1);
});

// src=
test('CoverBackground image background is applied', () => {
  expect(mainComponent.prop('style').backgroundImage).toBe(
    'url(https://nerdswithcharisma.com/static/6d2103ee47e61e92d68e165eb9de330a/97132/bg--hero.webp)',
  );
});

// minHeight
test('CoverBackground min height gets set', () => {
  expect(mainComponent.prop('style').minHeight).toBe(500);
});

// mp4
test('CoverBackground mp4 is set if supplied', () => {
  expect(mp4Component.length).toBe(1);
  expect(mp4Component.prop('src')).toBe('somevideo.mp4');
});

// backgroundColor
test('CoverBackground background gets set', () => {
  expect(mainComponent.prop('style').backgroundColor).toBe('#ff0000');
});

// theme
test('CoverBackground theme classes are applied', () => {
  expect(mainComponent.find('.test-class').length).toBe(1);
});

// webm={props.webm}
test('CoverBackground webm is set if supplied', () => {
  expect(webmComponent.length).toBe(1);
  expect(webmComponent.prop('src')).toBe('somevideo.webm');
});
