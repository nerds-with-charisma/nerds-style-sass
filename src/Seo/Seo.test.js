import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';

import Seo from './Seo';

Enzyme.configure({ adapter: new EnzymeAdapter() });

const setup = (props = {}, state = null) => {
  return shallow(
    <Seo
      lang={props.lang}
      meta={props.meta}
      openGraph={props.openGraph}
      title={props.title}
    >
      <h1>{'Test Child'}</h1>
    </Seo>,
  );
};

const testProps = {
  title: 'Test title',
  meta: [
    {
      name: 'description',
      content: 'My description',
    },
  ],
  openGraph: {
    url: 'https://nerdswithcharisma.com',
  },
  lang: 'FE',
  schema: [
    {
      '@type': 'Person',
      colleague: [],
      image: 'https://nerdswithcharisma.com/images/portfolio/brian.jpg',
      jobTitle: 'Developer',
      alumniOf: 'Northern Illinois University',
      gender: 'male',
      '@context': 'http://schema.org',
      email: 'briandausman@gmail.com',
      name: 'Brian Dausman',
      url: 'https://nerdswithcharisma.com',
    },
  ],
};

// find by test attr
const findByTestAttr = (wrapper, val) => {
  return wrapper.find(`[data-test="${val}"]`);
};

const wrapper = setup(testProps);
const mainComponent = findByTestAttr(wrapper, 'component-seo');

// Test
test('Seo widget renders without crashing', () => {
  expect(mainComponent.length).toBe(1);
});

// children
test('Seo child renders if supplied', () => {
  expect(mainComponent.find('h1').text()).toBe('Test Child');
});

// lang
test('Seo language is set', () => {
  expect(mainComponent.prop('lang')).toBe('FE');
});

// meta
test('Seo meta gets populated', () => {
  expect(
    mainComponent
      .find('meta')
      .first()
      .prop('content'),
  ).toBe('My description');
});

// openGraph
test('Seo OG gets populated', () => {
  expect(mainComponent.find('meta[property="og:url"]').prop('content')).toBe(
    'https://nerdswithcharisma.com',
  );
});

// title
test('Seo title renders', () => {
  expect(mainComponent.find('title').text()).toBe('Test title');
});
