import React from 'react';
import { PropTypes } from 'prop-types';
import { Helmet } from 'react-helmet';

const Seo = ({ children, lang, meta, openGraph, schema, title }) => (
  <Helmet lang={lang} data-test="component-seo">
    <title>{title}</title>

    {/* meta data */}
    {meta &&
      meta.length > 0 &&
      meta.map((item) => (
        <meta key={item.name} name={item.name} content={item.content} />
      ))}

    {/* schema markup */}
    {schema &&
      schema.length > 0 &&
      schema.map((item) => (
        <script type="application/ld+json" key={item}>
          {JSON.stringify(item).replace(/_/g, '@')}
        </script>
      ))}

    {/* open graph */}
    {openGraph &&
      Object.keys(openGraph).map((key) => (
        <meta
          property={`og:${key}`}
          content={openGraph[key]}
          key={openGraph[key]}
        />
      ))}

    {children}
  </Helmet>
);

Seo.defaultProps = {
  children: null,
  lang: 'en',
  meta: [],
  og: {},
  schema: [],
};

Seo.propTypes = {
  children: PropTypes.object,
  lang: PropTypes.string,
  meta: PropTypes.array,
  openGraph: PropTypes.object,
  schema: PropTypes.array,
  title: PropTypes.string.isRequired,
};

export default Seo;

/**
 * @property {object} children - Valid JSX object, that will be injected into the head
 * @property {string} lang - Language to be specified, if not provided, en will be used
 * @property {array} meta - an array of all meta elements to be populated meta: [{ name: 'description', content: 'My description', }],
 * @property {object} openGraph - Facebook's open graph to be populated in the head: { key: value }
 * @property {array} schema - Any valid schema data to be populated, comma separate each object
 * @property {string} title - The page title to be populated
 */
