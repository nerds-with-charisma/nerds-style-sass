import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';

import Breadcrumbs from './Breadcrumbs';

Enzyme.configure({ adapter: new EnzymeAdapter() });

const setup = (props = {}, state = null) => {
  return shallow(
    <Breadcrumbs
      crumbs={props.crumbs}
      delimiter={props.delimiter}
      delimiterTheme={props.delimiterTheme}
      id={props.id}
      linkClass={props.linkClass}
      schema
      theme={props.theme}
    />,
  );
};

const testProps = {
  crumbs: [
    {
      title: 'Home',
      url: '/',
      active: false,
    },
    {
      title: 'About',
      url: '/about',
      active: true,
    },
  ],
  delimiter: '/',
  delimiterTheme: 'test-delimiter',
  id: 'test',
  linkClass: 'link-class',
  schema: true,
  theme: 'test-class',
};

// find by test attr
const findByTestAttr = (wrapper, val) => {
  return wrapper.find(`[data-test="${val}"]`);
};

// Tests
test('Breadcrumb renders without crashing', () => {
  const wrapper = setup(testProps);
  const appComponent = findByTestAttr(wrapper, 'component-breadcrumb');

  expect(appComponent.length).toBe(1);
});

// crumbs
test('Breadcrumb links render', () => {
  const wrapper = setup(testProps);
  const appComponent = findByTestAttr(wrapper, 'component-breadcrumb');
  const crumbs = appComponent.find('span');
  expect(crumbs.length).toBe(3);
});

// delimiter: '/'
test('Delimiter renders right', () => {
  const wrapper = setup(testProps);
  const appComponent = findByTestAttr(
    wrapper,
    'component-breadcrumb-delimiter',
  );
  const delimiterCount = testProps.crumbs.length - appComponent.length;

  expect(delimiterCount).toBe(1);
});

// delimiterTheme
test('Delimiter theme styles are applied', () => {
  const wrapper = setup(testProps);
  const appComponent = findByTestAttr(
    wrapper,
    'component-breadcrumb-delimiter',
  );

  expect(appComponent.prop('className')).toBe('nwc--delimiter test-delimiter');
});

// id
test('Delimiter id is appended', () => {
  const wrapper = setup(testProps);
  expect(wrapper.find('#test').length).toBe(1);
});

// linkClass
test('Breadcrumb link classes are appliled', () => {
  const wrapper = setup(testProps);
  expect(wrapper.find('a').prop('className')).toBe('link-class');
});

// theme
test('Breadcrumb schema renders', () => {
  const wrapper = setup(testProps);
  expect(wrapper.prop('className')).toBe('nwc--breadcrumb test-class');
});
