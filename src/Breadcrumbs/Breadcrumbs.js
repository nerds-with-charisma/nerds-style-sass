import React, { useEffect } from 'react';
import { PropTypes } from 'prop-types';

const Breadcrumbs = ({
  crumbs,
  delimiter,
  delimiterTheme,
  id,
  linkClass,
  schema,
  theme,
}) => {
  useEffect(() => {
    // build our schema markup
    if (schema === true) {
      const itemListElement = [];
      for (var i = 0; i < crumbs.length; i++) {
        itemListElement.push({
          '@type': 'ListItem',
          position: i + 1,
          name: crumbs[i].title,
          item: `${window.location.origin}${crumbs[i].url}`,
        });
      }

      // insert the schema into the src
      const el = document.createElement('script');
      el.type = 'application/ld+json';

      el.text = JSON.stringify({
        '@context': 'https://schema.org',
        '@type': 'BreadcrumbList',
        itemListElement,
      });

      document.querySelector('head').appendChild(el);
    }
  }, []);

  return (
    <section
      data-test="component-breadcrumb"
      className={`nwc--breadcrumb ${theme}`}
      id={id}
    >
      {crumbs.map((c, i) => (
        <span key={c.title}>
          {c.active === true ? (
            c.title
          ) : (
            <a href={c.url} className={linkClass}>
              {c.title}
            </a>
          )}
          {i < crumbs.length - 1 && (
            <span
              data-test="component-breadcrumb-delimiter"
              className={`nwc--delimiter ${delimiterTheme}`}
            >{` ${delimiter} `}</span>
          )}
        </span>
      ))}
    </section>
  );
};

Breadcrumbs.defaultProps = {
  delimiter: '/',
  delimiterTheme: 'font--grey',
  id: null,
  linkClass: null,
  schema: false,
  theme: null,
};

Breadcrumbs.propTypes = {
  crumbs: PropTypes.array.isRequired,
  delimiter: PropTypes.string,
  delimiterTheme: PropTypes.string,
  id: PropTypes.string,
  linkDlass: PropTypes.string,
  schema: PropTypes.bool,
  theme: PropTypes.string,
};

export default Breadcrumbs;

/**
 * Breadcrumbs to show your progress in the flow
 *
 * @property {array} crumbs - The array of breadcrumbs required { title: string, url: string, active: bool }
 * @property {string} delimiter - The character to go in between each crumb, defaults to /
 * @property {string} delimiterTheme - The class applied to the delimiter, in case you want to change color or something easily
 * @property {string} id - Id to be added to the wrapper
 * @property {string} linkClass - Class to be added to each link
 * @property {object} schema - A JSON object for the JSON schema for search engines
 * @property {string} theme - Classes to be applied to the wrapper
 */
