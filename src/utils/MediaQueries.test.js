import mq from './MediaQueries';

test('Mq is returned properly', () => {
  expect(mq()).toBe('lg');
});

test('Mq returns right xs', () => {
  Object.defineProperty(window, 'innerWidth', {
    writable: true,
    configurable: true,
    value: 105,
  });

  expect(mq()).toBe('xs');
});

test('Mq returns right sm', () => {
  Object.defineProperty(window, 'innerWidth', {
    writable: true,
    configurable: true,
    value: 600,
  });

  expect(mq()).toBe('sm');
});

test('Mq returns right md', () => {
  Object.defineProperty(window, 'innerWidth', {
    writable: true,
    configurable: true,
    value: 800,
  });

  expect(mq()).toBe('md');
});
