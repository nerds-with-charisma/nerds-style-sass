import validateEmail from './validateEmail';

test('Returns true for valid email', () => {
  expect(validateEmail('briandausman@gmail.com').error).toBe(false);
});

test('Returns false for invalid email', () => {
  expect(validateEmail('briandausmangmail.com').error).toBe(true);
  expect(validateEmail('briandausmangmail.com').message).toBe(
    'The e-mail address you have entered is not valid. Email should be of the form abc@de.com',
  );
});
