import maskPhone from './MaskPhone';

// tests
test('Mask phone returns a value', () => {
  expect(maskPhone(5555555555)).toBe('(555) 555-5555');
});
