import passwordValidator from './passwordValidator';

test('Returns true for valid password with default settings', () => {
  expect(passwordValidator('Testing123').error).toBe(false);
});

test('Returns false for invalid password with default settings', () => {
  expect(passwordValidator('testing').error).toBe(true);
});

// test each param
test('Length validation works correctly', () => {
  // length tests
  expect(passwordValidator('Hi1', 4, true, true, true, '?!').error).toBe(true);
  expect(passwordValidator('Hello1', 4, true, true, '?!').error).toBe(false);

  // has number tests
  expect(passwordValidator('Hello', 4, true).error).toBe(true);
  expect(passwordValidator('Hello1', 4, true).error).toBe(false);

  // capitol number tests
  expect(passwordValidator('hello', 4, true).error).toBe(true);
  expect(passwordValidator('Hello1', 4, true).error).toBe(false);

  // has special chars
  expect(passwordValidator('H@llo1', 4, true, true, true, '@').error).toBe(
    true,
  );
  expect(passwordValidator('Hello1', 4, true, true, '@').error).toBe(false);
});
