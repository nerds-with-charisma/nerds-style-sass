import React from 'react';
import { PropTypes } from 'prop-types';

const Select = ({
  callback,
  error,
  fullWidth,
  id,
  options,
  theme,
  title,
  required,
  optional,
}) => (
  <label
    data-test="component-select"
    className={`nwc--select ${theme}`}
    htmlFor={id}
  >
    <div className="nwc--label-wrap">
      <strong className="font--14">
        <span data-test="component-select-title">{title}</span>
        {required === true && (
          <sup
            data-test="component-select-required"
            className="font--error font--10"
          >
            {' *'}
          </sup>
        )}
        {optional === true && (
          <sup
            data-test="component-select-optional"
            className="font--grey font--10"
          >
            {' (optional)'}
          </sup>
        )}
      </strong>
    </div>
    <select
      data-test="component-select-input"
      id={id}
      className={fullWidth === true ? 'full-width' : ''}
      onChange={(e) => callback(e.target.value)}
    >
      {options.map((item) => (
        <option key={item.value} value={item.value}>
          {item.title}
        </option>
      ))}
    </select>
    {error !== null && (
      <aside
        data-test="component-select-error"
        className="nwc--validation nwc--validation--error font--12"
      >
        {error}
      </aside>
    )}
  </label>
);

Select.defaultProps = {
  fullWidth: false,
  error: null,
  theme: '',
  title: null,
  required: false,
  optional: false,
};

Select.propTypes = {
  callback: PropTypes.func.isRequired,
  error: PropTypes.string,
  fullWidth: PropTypes.bool,
  id: PropTypes.string.isRequired,
  options: PropTypes.array.isRequired,
  theme: PropTypes.string,
  title: PropTypes.string,
  required: PropTypes.bool,
  optional: PropTypes.bool,
};

export default Select;

/**
 * Select input with callbacks
 *
 * @property {func} callback - A function that will run after the input  has changed, it will return the value of the input for you to use in the parent component.
 * @property {string} error - An error message to be shown if there's an issue with the selection.
 * @property {bool} fullWidth - A bool to determin if inline or block.
 * @property {string} id - An id to be added to the input itself, not the wrapping label.
 * @property {array} optoins - An array of object shown when the select is open { title: '', value: '' }
 * @property {string} theme - Classes to be added to the wrapping label.
 * @property {string} title - The label text title of the input.
 * @property {bool} required - Will show required * if true, else nothing.
 * @property {bool} optional - Will show (optional) if true, else nothing.
 */
