import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';

import Select from './Select';

Enzyme.configure({ adapter: new EnzymeAdapter() });

// get a copy of the dom for our component
const setup = (props = {}, state = null) => {
  return shallow(
    <Select
      callback={props.callback}
      error={props.error}
      fullWidth={props.fullWidth}
      id={props.id}
      options={props.options}
      theme={props.theme}
      title={props.title}
      required={props.required}
      optional={props.optional}
    />,
  );
};

const testProps = {
  callback: (v) => console.log(v),
  error: 'Test error',
  fullWidth: true,
  id: 'testId',
  options: [],
  theme: 'test-class',
  title: 'Test',
  required: true,
  optional: false,
};

// find by test attr
const findByTestAttr = (wrapper, val) => {
  return wrapper.find(`[data-test="${val}"]`);
};

// make sure the component renders
test('Select renders without crashing', () => {
  const wrapper = setup(testProps);
  const appComponent = findByTestAttr(wrapper, 'component-select');
  expect(appComponent.length).toBe(1);
});

// callback
describe('Select callback value is returned', () => {
  test('function is called on change', () => {
    const options = [
      {
        title: 'Option 1',
        value: 'option-1',
        active: true,
      },
    ];

    const callback = jest.fn((v) => v);
    const wrapper = shallow(
      <Select id="test" callback={callback} title="test" options={options} />,
    );
    const input = findByTestAttr(wrapper, 'component-select-input');
    const mockEvent = { target: { value: 'Option 1' } };
    input.simulate('change', mockEvent);
    const callbackItem = callback.mock.results[0].value;

    expect(callback).toHaveBeenCalled();
    expect(callbackItem).toBe('Option 1');
  });
});

// error
test('Select error shows', () => {
  const wrapper = setup(testProps);
  const appComponent = findByTestAttr(wrapper, 'component-select-error');
  expect(appComponent.length).toBe(1);
});

// fullWidth
test('Select full width style shows', () => {
  const wrapper = setup(testProps);
  const appComponent = findByTestAttr(wrapper, 'component-select');
  expect(appComponent.find('.full-width').length).toBe(1);
});

// id,
test('Select full width style shows', () => {
  const wrapper = setup(testProps);
  const appComponent = findByTestAttr(wrapper, 'component-select');
  expect(appComponent.find('#testId').length).toBe(1);
});

// theme: 'test-class',
test('Select theme classes are applied', () => {
  const wrapper = setup(testProps);
  expect(wrapper.find('.nwc--select.test-class').length).toBe(1);
});

//   title
test('Select title renders', () => {
  const wrapper = setup(testProps);
  const appComponent = findByTestAttr(wrapper, 'component-select-title');
  expect(appComponent.text()).toBe('Test');
});

// required
test('Select required renders', () => {
  const wrapper = setup(testProps);
  const appComponent = findByTestAttr(wrapper, 'component-select-required');
  expect(appComponent.length).toBe(1);
});

// optional
test('Select optional renders', () => {
  const wrapper = setup({
    callback: (v) => console.log(v),
    error: 'Test error',
    fullWidth: true,
    id: 'testId',
    options: [],
    theme: 'test-class',
    title: 'Test',
    required: false,
    optional: true,
  });
  const appComponent = findByTestAttr(wrapper, 'component-select-optional');
  expect(appComponent.length).toBe(1);
});
