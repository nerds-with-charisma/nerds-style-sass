import React, { useState } from 'react';
import { PropTypes } from 'prop-types';

const Tooltip = ({
  callback,
  content,
  id,
  position,
  theme,
  themeTooltip,
  title,
  width,
}) => {
  const [show, showSetter] = useState(false);
  const [elementWidth, elementWidthSetter] = useState(0);

  const buttonStyle = {
    display: 'inline-block',
    position: 'relative',
    textAlign: 'center',
  };

  const tooltipStyle = {
    position: 'absolute',
    top: position === 'top' ? null : null,
    bottom: position === 'top' ? '100%' : null,
    left:
      position === 'left' ? `-${width}px` : `${elementWidth / 2 - width / 2}px`,
    left: position === 'right' ? '100%' : `${elementWidth / 2 - width / 2}px`,
    marginTop: position === 'left' || position === 'right' ? '-60%' : 0,
  };

  return (
    <span
      data-test="component-tooltip"
      id={id}
      className="nwc--tooltip"
      style={buttonStyle}
    >
      <button
        type="button"
        style={{ cursor: 'pointer' }}
        className={theme}
        onClick={() => callback()}
        onMouseEnter={(e) => {
          elementWidthSetter(e.currentTarget.offsetWidth);
          showSetter(true);
        }}
        onMouseLeave={() => showSetter(false)}
      >
        {title}
      </button>
      {show === true && (
        <div
          className={`${themeTooltip} nwc--tooltip-${position}`}
          style={{ width: width, ...tooltipStyle, zIndex: 9999 }}
        >
          {content}
        </div>
      )}
    </span>
  );
};

Tooltip.defaultProps = {
  callback: null,
  content: 'Hello!',
  id: null,
  theme: null,
  themeTooltip: 'bg--light shadow--md padding--sm text--center font--14',
  title: 'Hover Here',
  width: 300,
};

Tooltip.propTypes = {
  callback: PropTypes.func,
  content: PropTypes.object,
  id: PropTypes.string,
  theme: PropTypes.string,
  themeTooltip: PropTypes.string,
  title: PropTypes.string,
  width: PropTypes.number,
};

export default Tooltip;

/**
 * @property {func} callback - what, if anything should happen when the hover button is clicked
 * @property {object} content - a valid jsx object that you want displayed inside the hover tooltip
 * @property {string} id - id applied to the wrapper
 * @property {string} theme - classes applied to the hover button
 * @property {string} themeTooltip - classes applied to the hover tooltip itself, default uses the nwc sass styles
 * @property {string} title - the title that appears on the button
 * @property {number} width - how wide the tooltip should be, defaults to 300
 */
