import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';

import Tooltip from './Tooltip';

Enzyme.configure({ adapter: new EnzymeAdapter() });

const setup = (props = {}, state = null) => {
  return shallow(
    <Tooltip
      callback={props.callback}
      content={props.content}
      id={props.id}
      position={props.position}
      theme={props.theme}
      themeTooltip={props.themeTooltip}
      title={props.title}
      width={props.width}
    />,
  );
};

const testProps = {
  callback: () => console.log('callback'),
  content: <strong>{"This is all stuff and it's super duper cool"}</strong>,
  id: 'myId',
  position: 'bottom',
  theme: 'test-theme',
  themeTooltip: 'tooltip-classes',
  title: 'Hello',
  width: 800,
};

// find by test attr
const findByTestAttr = (wrapper, val) => {
  return wrapper.find(`[data-test="${val}"]`);
};

const wrapper = setup(testProps);
const mainComponent = findByTestAttr(wrapper, 'component-tooltip');

// Test
test('Tooltip renders without crashing', () => {
  expect(mainComponent.length).toBe(1);
});

// id: 'myId',
test('Tooltip id gets applied', () => {
  expect(mainComponent.find('#myId').length).toBe(1);
});

// theme
test('Tooltip theme gets applied', () => {
  expect(mainComponent.find('button').prop('className')).toBe('test-theme');
});

// title
test('Tooltip title renders', () => {
  expect(mainComponent.find('button').text()).toBe('Hello');
});
