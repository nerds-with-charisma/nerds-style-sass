import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';

import HeaderBar from './HeaderBar';

Enzyme.configure({ adapter: new EnzymeAdapter() });

const setup = (props = {}, state = null) => {
  return shallow(
    <HeaderBar
      fixed={props.fixed}
      center={props.center}
      left={props.left}
      right={props.right}
      backgroundColor={props.backgroundColor}
      container={props.container}
      fontColor={props.fontColor}
      height={props.height}
      mobileOnly={props.mobileOnly}
      theme={props.theme}
    />,
  );
};

const testProps = {
  fixed: true,
  center: <strong>{'Center'}</strong>,
  left: <span>{'Left'}</span>,
  right: <span>{'Right'}</span>,
  backgroundColor: 'red',
  container: 'container',
  fontColor: 'blue',
  height: 100,
  mobileOnly: false,
  theme: 'test-class',
};

const defaultProps = {
  mobileOnly: true,
  center: <strong>{'Center'}</strong>,
  left: <span>{'Left'}</span>,
  right: <span>{'Right'}</span>,
};

const heightProps = {
  fixed: true,
  center: <strong>{'Center'}</strong>,
  left: <span>{'Left'}</span>,
  right: <span>{'Right'}</span>,
  backgroundColor: 'red',
  container: 'container',
  fontColor: 'blue',
  mobileOnly: false,
  theme: 'test-class',
};

// find by test attr
const findByTestAttr = (wrapper, val) => {
  return wrapper.find(`[data-test="${val}"]`);
};

const wrapper = setup(testProps);
const defaultWrapper = setup(defaultProps);
const heightWrapper = setup(heightProps);

const mainComponent = findByTestAttr(wrapper, 'component-header-bar');
const defaultComponent = findByTestAttr(defaultWrapper, 'component-header-bar');
const heightComponent = findByTestAttr(heightWrapper, 'component-header-bar');

const centerComponent = findByTestAttr(wrapper, 'component-header-bar-center');
const letfComponent = findByTestAttr(wrapper, 'component-header-bar-left');
const rightComponent = findByTestAttr(wrapper, 'component-header-bar-right');

// Test
test('HeaderBar renders without crashing', () => {
  expect(mainComponent.length).toBe(1);
});

// fixed
test('HeaderBar gets fixed styles when prop is true', () => {
  expect(mainComponent.prop('style').position).toBe('fixed');
});

// center
test('HeaderBar center prop children render', () => {
  expect(centerComponent.find('strong').text()).toBe('Center');
});

// left
test('HeaderBar left prop children render', () => {
  expect(letfComponent.find('span').text()).toBe('Left');
});

// right
test('HeaderBar right prop children render', () => {
  expect(rightComponent.find('span').text()).toBe('Right');
});

// backgroundColor
test('HeaderBar background color gets set', () => {
  expect(mainComponent.prop('style').backgroundColor).toBe('red');
});

// container
test('HeaderBar container gets applied', () => {
  expect(mainComponent.find('.container').length).toBe(1);
});

// fontColor
test('HeaderBar text color gets set', () => {
  expect(mainComponent.prop('style').color).toBe('blue');
});

// height
test('HeaderBar height prop gets set', () => {
  expect(mainComponent.prop('style').height).toBe(100);
  expect(heightComponent.prop('style').height).toBe(56);
});

// mobileOnly
test("HeaderBar doesn't show up on desktop", () => {
  expect(defaultComponent.length).toBe(0);
});
// theme
test('Hamburger renders without crashing', () => {
  expect(mainComponent.find('.test-class').length).toBe(1);
});
