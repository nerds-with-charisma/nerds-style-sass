import React from 'react';
import { PropTypes } from 'prop-types';

const Hamburger = ({ callback, color, height, showClose }) => {
  const hamburgerStyle = {
    width: 27,
    height: 2,
    backgroundColor: color,
    margin: '0 0 6px 0',
    transition: '0.4s',
  };

  const isClosed1 = {
    transform: 'rotate(-45deg) translate(-6px, 8px)',
  };

  const isClosed2 = { opacity: 0 };

  const isClosed3 = {
    transform: 'rotate(45deg) translate(-3px, -6px)',
  };

  return (
    <div
      data-test="component-hamburger"
      className="hamburger"
      style={{ paddingTop: `${height / 3}px` }}
      onClick={() => callback()}
    >
      <div
        className="bar1"
        style={
          showClose === true
            ? { ...isClosed1, ...hamburgerStyle }
            : hamburgerStyle
        }
      ></div>
      <div
        className="bar2"
        style={
          showClose === true
            ? { ...isClosed2, ...hamburgerStyle }
            : hamburgerStyle
        }
      ></div>
      <div
        className="bar3"
        style={
          showClose === true
            ? { ...isClosed3, ...hamburgerStyle }
            : hamburgerStyle
        }
      ></div>
    </div>
  );
};

Hamburger.defaultProps = {
  height: 56,
  color: '#000',
  showClose: false,
};

Hamburger.propTypes = {
  callback: PropTypes.func.isRequired,
  color: PropTypes.string,
  height: PropTypes.number,
  showClose: PropTypes.bool,
};

export default Hamburger;

/**
 * @property {func} callback - The callback function when the icon is clicked
 * @property {string} color - The color of the icon
 * @property {number} height - How tall the icon should be
 * @property {bool} showClose - Toggle to show the close button or the hamburger, track with your own state
 */
