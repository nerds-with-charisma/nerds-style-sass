import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';

import Hamburger from './Hamburger';

Enzyme.configure({ adapter: new EnzymeAdapter() });

const setup = (props = {}, state = null) => {
  return shallow(
    <Hamburger
      callback={props.callback}
      color={props.color}
      height={props.height}
      showClose={props.showClose}
    />,
  );
};

const testProps = {
  callback: () => null,
  color: '#000',
  showClose: false,
};

const closeProps = {
  callback: () => null,
  color: '#000',
  showClose: true,
};

// find by test attr
const findByTestAttr = (wrapper, val) => {
  return wrapper.find(`[data-test="${val}"]`);
};

const wrapper = setup(testProps);
const wrapperClosed = setup(closeProps);
const mainComponent = findByTestAttr(wrapper, 'component-hamburger');
const closeComponent = findByTestAttr(wrapperClosed, 'component-hamburger');

// Test
test('Hamburger renders without crashing', () => {
  expect(mainComponent.length).toBe(1);
});

// color
test('Hamburger color is set', () => {
  expect(
    mainComponent
      .find('div')
      .last()
      .prop('style').backgroundColor,
  ).toBe('#000');
});

// close shows when it should, icon when it shouldn't
test('Hamburger close renders ok', () => {
  expect(
    closeComponent
      .find('div')
      .last()
      .prop('style').transform,
  ).toBe('rotate(45deg) translate(-3px, -6px)');

  expect(
    mainComponent
      .find('div')
      .last()
      .prop('style').transform,
  ).toBe(undefined);
});
