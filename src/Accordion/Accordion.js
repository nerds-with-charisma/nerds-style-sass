import React, { useState } from 'react';
import { PropTypes } from 'prop-types';

import AccordionItem from './AccordionItem';

const Accordion = ({ delimiter, delimiterClose, id, multiOpen, options }) => {
  const [o, oSetter] = useState(options);

  const togglePanels = (i) => {
    let optionsMute = options;

    if (multiOpen !== true) optionsMute.map(a => a.isOpen = false); // set them all closed
    optionsMute[i].isOpen = !optionsMute[i].isOpen; // toggle the clicked one

    oSetter([...optionsMute]);
  };

  // todo change isToggle so it leave them open on click or collapses them and opens the one u clicked;
  return (
    <section data-test="component-accordion" id={id} className="nwc--accordion">
      {o.map((item, i) => (
        <AccordionItem
          key={item.title}
          delimiter={delimiter}
          delimiterClose={delimiterClose}
          item={item}
          togglePanels={togglePanels} i={i}
        />
      ))}
    </section>
  )
};

Accordion.defaultProps = {
  delimiter: '+',
  delimiterClose: '-',
  id: null,
  multiOpen: true,
};

Accordion.propTypes = {
  delimiter: PropTypes.string,
  delimiterClose: PropTypes.string,
  id: PropTypes.string,
  multiOpen: PropTypes.bool,
  options: PropTypes.array.isRequired,
};

export default Accordion;

/**
* Accordion component that can be single open or multip open based off the mutliOpe prop.
* If false, only 1 panel will be open at a time.
* If true, panels will stay open until their heading is clicked again.
*
* @property {string} delimiter - The indicator on the right of the accordion when closed
* @property {string} delimiterClosed - The indicator on the right of the accordion when opened
* @property {string} id - Id to be applied to the wrapper
* @property {bool} multiOpen - If true, then panels will stay open until you click the heading. If false, all panels will close except the one you click, leaving only 1 open at a time
* @property {array} options - The options for each accordion panel { title: 'test', content: 'hello', isOpen: true }
*/
