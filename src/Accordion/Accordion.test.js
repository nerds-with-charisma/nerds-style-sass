import React from 'react';
import Enzyme, { mount } from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';

import Accordion from './Accordion';
import AccordionItem from './AccordionItem';

Enzyme.configure({ adapter: new EnzymeAdapter() });

const setup = (props = {}, state = null) => {
  return mount(
    <Accordion options={props.options} multiOpen={props.multiOpen} />,
  );
};

const testProps = {
  options: [
    { title: 'test', content: 'hello', isOpen: true },
    { title: 'test2', content: 'bye', isOpen: false },
  ],
  multiOpen: true,
};

// find by test attr
const findByTestAttr = (wrapper, val) => {
  return wrapper.find(`[data-test="${val}"]`);
};

// Tests
test('Accordion renders aight', () => {
  const wrapper = setup(testProps);
  const appComponent = findByTestAttr(wrapper, 'component-accordion');
  expect(appComponent.length).toBe(1);
});

// Options render
test('Accordion panels seem to render', () => {
  const wrapper = setup(testProps);
  const appComponent = findByTestAttr(wrapper, 'component-accordion-item');
  expect(appComponent.length).toBe(2);

  expect(
    appComponent
      .first()
      .find('.nwc--accordion-content')
      .text(),
  ).toBe('hello');

  expect(
    appComponent
      .last()
      .find('.nwc--accordion-title')
      .text(),
  ).toBe('+test2');
});
