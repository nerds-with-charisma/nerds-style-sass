import React from 'react';
import { PropTypes } from 'prop-types';

const AccordionItem = ({
  delimiter,
  delimiterClose,
  i,
  item,
  togglePanels,
}) => {
  return (
    <div data-test="component-accordion-item" className="nwc--accordion-item">
      <div
        className="nwc--accordion-title padding--sm cursor--pointer"
        onClick={() => togglePanels(i)}
      >
        <span
          data-test="component-accordion-delimiter"
          className="nwc--accordion-delimiter float--right font--20"
        >
          {item.isOpen === true ? delimiterClose : delimiter}
        </span>
        {item.title}
      </div>
      {item.isOpen === true && (
        <div className="nwc--accordion-content font--16 padding--sm">
          {item.content}
        </div>
      )}
    </div>
  );
};

AccordionItem.defaultProps = {
  delimiter: '+',
  delimiterClose: '-',
};

AccordionItem.propTypes = {
  delimiter: PropTypes.string,
  delimiterClose: PropTypes.string,
  i: PropTypes.number.isRequired,
  item: PropTypes.object.isRequired,
  togglePanels: PropTypes.func.isRequired,
};

export default AccordionItem;

/**
 * The accordion item that's mapped inside the parent accodrion component
 *
 * @property
 */
