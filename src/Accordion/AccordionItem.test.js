import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';

import AccordionItem from './AccordionItem';

Enzyme.configure({ adapter: new EnzymeAdapter() });

const setup = (props = {}, state = null) => {
  return shallow(
    <AccordionItem
      delimiter={props.delimiter}
      delimiterClose={props.delimiterClose}
      item={props.item}
      togglePanels={props.togglePanels}
      i={props.i}
    />,
  );
};

const testProps = {
  delimiter: '>',
  delimiterClose: '-',
  item: { title: 'test', content: 'hello', isOpen: false },
  togglePanels: () => console.log(),
  i: 0,
};

// find by test attr
const findByTestAttr = (wrapper, val) => {
  return wrapper.find(`[data-test="${val}"]`);
};

// Renders without exploding
test('AccordionItem renders without crashing', () => {
  const wrapper = setup(testProps);
  const appComponent = findByTestAttr(wrapper, 'component-accordion-item');
  expect(appComponent.length).toBe(1);
});

// delimiter
test('AccordionItem delimiter works if provided', () => {
  const wrapper = setup(testProps);
  const appComponent = findByTestAttr(wrapper, 'component-accordion-delimiter');
  expect(appComponent.length).toBe(1);
  expect(appComponent.text()).toBe('>');
});

test('AccordionItem close delimiter shows default +', () => {
  const wrapper = setup({
    item: { title: 'test', content: 'hello', isOpen: false },
    togglePanels: () => console.log(),
    i: 0,
  });
  const appComponent = findByTestAttr(wrapper, 'component-accordion-delimiter');
  expect(appComponent.length).toBe(1);
  expect(appComponent.text()).toBe('+');
});

// delimiterClose
test('AccordionItem close delimiter shows', () => {
  const wrapper = setup({
    delimiter: '>',
    delimiterClose: '-',
    item: { title: 'test', content: 'hello', isOpen: true },
    togglePanels: () => console.log(),
    i: 0,
  });
  const appComponent = findByTestAttr(wrapper, 'component-accordion-delimiter');
  expect(appComponent.length).toBe(1);
  expect(appComponent.text()).toBe('-');
});
